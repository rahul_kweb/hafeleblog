﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="aboutus.aspx.cs" Inherits="aboutus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>About Us | Digital Door Lock Hafele |Keypad Lock</title>
    <meta name="description" content="Presenting the most secured range of Digital Door Locks which offers keyless convenience for easy access and smarter options to manage access to your homes">

    <link rel="canonical" href="https://digital-locks.hafeleindia.co.in/about-us" />

    <meta property="og:url" content="https://digital-locks.hafeleindia.co.in/about-us" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="About Us | Digital Door Lock Hafele |Keypad Lock" />
    <meta property="og:description" content="Presenting the most secured range of Digital Door Locks which offers keyless convenience for easy access and smarter options to manage access to your homes" />
    <meta property="og:image" content="https://digital-locks.hafeleindia.co.in/img/logo.png" />

    <meta name="twitter:card" content="summary" />
    <meta property="og:url" content="https://digital-locks.hafeleindia.co.in/about-us" />
    <meta property="og:title" content="About Us | Digital Door Lock Hafele |Keypad Lock" />
    <meta property="og:description" content="Presenting the most secured range of Digital Door Locks which offers keyless convenience for easy access and smarter options to manage access to your homes" />
    <meta property="og:image" content="https://digital-locks.hafeleindia.co.in/img/logo.png" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="myh1">About Hafele Digital Door Lock</h1>

    <div class="aboutus-container">
        <div class="aboutus-innercontainer">

            <div class="aboutus-contentwrap">
                <h4 class="heading">About <span>us</span></h4>
                <div class="aboutus-bannerwrp">
                    <iframe width="100%" height="400" class="aboutus-videowrap" src="https://www.youtube.com/embed/1QxBWKvGchc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <!--End of the about us banner wrap-->
                <p class="info">One area that has seen a major evolution over the last 10 decades is ‘home security’ as changes in living patterns has demanded more from the traditional key than mere security. People move out of their homes much more – be it for short durations to work or longer durations for holidays; and the home is left to the limited realms of security provided by a simple lock and key. During this time, the home has to be accessible to few key people like the domestic help coming in for household chores or children returning from school or maybe even a guest wanting to use your house while you are away on vacation. At the same time the home has to be secured from break-ins or unwanted access. Today, people want smarter and convenient options to manage the access to their homes - <b>the question is no longer about who stays out of the house but who can be let in.</b> </p>

            </div>
            <!--End of the about us content wrap-->
        </div>
        <!--End of the about us inner container-->
    </div>
    <!--End of the about us container-->

</asp:Content>

