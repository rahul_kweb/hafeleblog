﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MainMaster : System.Web.UI.MasterPage
{
    string Source;
    string size;
    string creative;
    string position;
    string cmp;
    string utm_source;
    string utm_medium;
    string utm_content;
    string utm_campaign;
    string utm_keyword;
    string keyword;
    public string utm_adgroup = "";
    public string utm_ad = "";
    string utm_domain;
    string srcPage = string.Empty;
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["HafeleIconicCMS"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        Source = Request.QueryString["Source"];
        size = Request.QueryString["size"];
        creative = Request.QueryString["creative"];
        position = Request.QueryString["position"];
        cmp = Request.QueryString["cmp"];
        utm_source = Request.QueryString["utm_source"];
        utm_medium = Request.QueryString["utm_medium"];
        utm_content = Request.QueryString["utm_content"];
        utm_campaign = Request.QueryString["utm_campaign"];
        utm_keyword = Request.QueryString["utm_keyword"];
        keyword = Request.QueryString["keyword"];
        utm_adgroup = Request.QueryString["utm_adgroup"];
        utm_ad = Request.QueryString["utm_ad"];
        utm_domain = Request.QueryString["domain_name"];
        srcPage = getPageSource();
    }

    //public void SendEmail(string to, string subject, string bodyText, string bodyHtml, string from, string fromName)
    //{

    //    WebClient client = new WebClient();
    //    NameValueCollection values = new NameValueCollection();
    //    values.Add("username", "b733bd84-a8ff-47f2-9c66-978f44e4bade");
    //    values.Add("api_key", "b733bd84-a8ff-47f2-9c66-978f44e4bade");
    //    values.Add("from", from);
    //    values.Add("from_name", fromName);
    //    values.Add("subject", subject);
    //    if (bodyHtml != null)
    //        values.Add("body_html", bodyHtml);
    //    if (bodyText != null)
    //        values.Add("body_text", bodyText);
    //    values.Add("to", to);
    //    byte[] response = client.UploadValues("https://api.elasticemail.com/mailer/send", values);
    //    //return response;
    //}

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtEmail.Text != "" && txtMobile.Text != "" && txtName.Text != "" && ddlSelectProdutc.SelectedItem.Text != "")
        {

            SqlCommand cmd = new SqlCommand("INSERT [LandingPage_PostLaunch]([Name],[MobileNo],[Email],[Product],[City],source,utm_source,utm_medium," +
                                            "utm_content,utm_campaign,utm_keyword,Device,utm_adgroup,utm_ad,PageSource)values('"
                                            + txtName.Text + "','" + txtMobile.Text + "','" + txtEmail.Text + "','" + ddlSelectProdutc.SelectedItem.Text + "','" + ddlCity.SelectedItem.Text + "','" + Source + "','" + utm_source + "','" + utm_medium + "','" + utm_content + "','" + utm_campaign + "','" + keyword + "','" + hdnDevice.Value + "','" + utm_adgroup + "','" + utm_ad + "','" + srcPage + "')", con);
            cmd.CommandType = CommandType.Text;

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                string Body = "<html><body><br /><b>Hello Hafele</b>,<br />" + "Lead on Hafele Reinvent access<br />First Name: " + txtName.Text + "<br />Email: " + txtEmail.Text + "<br />Phone: " + txtMobile.Text + "<br />Products: " + ddlSelectProdutc.SelectedItem.Text + " <br/> City: " + ddlCity.SelectedItem.Text + " <br/>Sender: Hafele</body></html>";

                //SendEmail("shraddha.gurav@hafeleindia.com;priyanka.rathod@hafeleindia.com;yesha@id8labs.com;customercare@hafeleindia.com", "Hafele Digital Locks", null, Body, "customercare@hafeleindia.com", "Hafele Digital Locks");
                //SendEmail("Priyanka.Rathod@hafeleindia.com;yesha@id8labs.com;Shraddha.Gurav@hafeleindia.com;Ruzario.Fernandes@hafeleindia.com;Customer.Care@hafeleindia.com", "Hafele Digital Locks", null, Body, "customercare@hafeleindia.com", "Hafele Digital Locks");
                //SendEmail("hamid@id8labs.com;malay@id8labs.com", "Hafele Digital Locks", null, Body, "customercare@hafeleindia.com", "Hafele Digital Locks");


                //ScriptManager.RegisterStartupScript(this, GetType(), "success", "alert('Data saved successfully');", true);

                SendEmailViaAmazon(Body, "Hafele Digital Locks", "", "", "Hafele Digital Locks");


                Response.Redirect("~/thank-you.aspx");

                //string function = "SendEmailViaAmazonService('" + Body + "','noreply@things2do.club','No Reply','" + txtEmail.Text + "')";
                //ScriptManager.RegisterStartupScript(this,Page.GetType(), "CallMyFunction", function, true);

               // clear();
            }

            catch (Exception ex)
            {

            }
        }
    }

   

    //protected void btnSubmitD_Click(object sender, EventArgs e)
    //{
    //    if (txtEmailD.Text != "")
    //    {
    //        SqlCommand cmd = new SqlCommand("INSERT [LandingPage_PostLaunchDowloadB]([Email],source,utm_source,utm_medium," +
    //                                        "utm_content,utm_campaign,utm_keyword,Device,utm_adgroup,utm_ad,PageSource)values('" + txtEmailD.Text + "','" + Source + "','" + utm_source + "','" + utm_medium + "','" + utm_content + "','" + utm_campaign + "','" + keyword + "','" + hdnDevice.Value + "','" + utm_adgroup + "','" + utm_ad + "','" + srcPage + "')", con);
    //        cmd.CommandType = CommandType.Text;
    //        try
    //        {
    //            con.Open();
    //            cmd.ExecuteNonQuery();
    //            con.Close();

    //            string Body = "<html><body><br /><b>Hello Hafele</b>,<br />" + "Lead on Hafele Reinvent access<br />Email: " + txtEmailD.Text + "<br />Sender: Hafel</body></html>";
    //            //SendEmail("Priyanka.Rathod @hafeleindia.com;Shraddha.Gurav@hafeleindia.com;Ruzario.Fernandes@hafeleindia.com;Customer.Care @hafeleindia.com", "Hafele Digital Locks", null, Body, "customercare@hafeleindia.com", "Hafele Digital Locks");

    //            //SendEmail("Ruzario.Fernandes@hafeleindia.com;Vrushali.Lad@hafeleindia.com;Pragnesh.Kantharia@hafeleindia.com;Melroy.Gonsalves@hafeleindia.com;Swapnil.Jagtap@hafeleindia.com;priyanka.rathod@hafeleindia.com;monikab@id8labs.com", "Hafele Digital Locks", null, Body, "customercare@hafeleindia.com", "Hafele Digital Locks");                
    //            // SendEmail("Swapnil.Jagtap@hafeleindia.com;priyanka.rathod@hafeleindia.com;monikab@id8labs.com", "Hafele Digital Locks", null, Body, "customercare@hafeleindia.com", "Hafele Digital Locks");
    //            //SendEmail("hamid@ideate.email;sameena@id8labs.com", "Hafele Digital Locks", null, Body, "priyankah@id8labs.com", "Hafele Digital Locks");
    //            // ScriptManager.RegisterStartupScript(this, GetType(), "success", "alert('Data saved successfully');", true);
    //            //Response.Redirect("~/pdf/Set-1.pdf");
    //            Response.Write("<script>window.open('pdf/REinventAccess_catalogue.pdf','_blank');</script>");
    //            //Response.Redirect("index.aspx");
    //            clear();
    //        }
    //        catch (Exception ex)
    //        {

    //        }
    //    }
    //}

    protected void btnSubmitD_Click(object sender, EventArgs e)
    {
        if (txtEmailD.Text != "")
        {
            SqlCommand cmd = new SqlCommand("INSERT [LandingPage_PostLaunchDowloadB]([Email],source,utm_source,utm_medium," +
                                            "utm_content,utm_campaign,utm_keyword,Device,utm_adgroup,utm_ad,PageSource)values('" + txtEmailD.Text + "','" + Source + "','" + utm_source + "','" + utm_medium + "','" + utm_content + "','" + utm_campaign + "','" + keyword + "','" + hdnDevice.Value + "','" + utm_adgroup + "','" + utm_ad + "','" + srcPage + "')", con);
            cmd.CommandType = CommandType.Text;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                string Body = "<html><body><br /><b>Hello Hafele</b>,<br />" + "Request for Broucher on Hafele Reinvent access<br />Email: " + txtEmailD.Text + "<br />Sender: Hafele</body></html>";
                //SendEmail("Priyanka.Rathod @hafeleindia.com;Shraddha.Gurav@hafeleindia.com;Ruzario.Fernandes@hafeleindia.com;Customer.Care @hafeleindia.com", "Hafele Digital Locks", null, Body, "customercare@hafeleindia.com", "Hafele Digital Locks");

                //SendEmail("Ruzario.Fernandes@hafeleindia.com;Vrushali.Lad@hafeleindia.com;Pragnesh.Kantharia@hafeleindia.com;Melroy.Gonsalves@hafeleindia.com;Swapnil.Jagtap@hafeleindia.com;priyanka.rathod@hafeleindia.com;monikab@id8labs.com", "Hafele Digital Locks", null, Body, "customercare@hafeleindia.com", "Hafele Digital Locks");                
                // SendEmail("Swapnil.Jagtap@hafeleindia.com;priyanka.rathod@hafeleindia.com;monikab@id8labs.com", "Hafele Digital Locks", null, Body, "customercare@hafeleindia.com", "Hafele Digital Locks");
                //SendEmail("hamid@ideate.email;sameena@id8labs.com", "Hafele Digital Locks", null, Body, "priyankah@id8labs.com", "Hafele Digital Locks");
                // ScriptManager.RegisterStartupScript(this, GetType(), "success", "alert('Data saved successfully');", true);
                //Response.Redirect("~/pdf/Set-1.pdf");
                
                //Response.Redirect("index.aspx");

                SendEmailViaAmazon(Body, "Hafele Digital Locks", "", "", "Hafele Digital Locks");

                Response.Write("<script>window.open('pdf/REinventAccess_catalogue.pdf','_blank');</script>");

                clear();
            }
            catch (Exception ex)
            {

            }
        }
    }

    protected string getPageSource()
    {
        string srcPage = Request.Url.Segments[1];
        return srcPage;

    }

    private void clear()
    {
        txtName.Text = string.Empty;
        //txtLName.Text = string.Empty;
        txtMobile.Text = string.Empty;
        txtEmail.Text = string.Empty;
        ddlSelectProdutc.SelectedValue = "0";
        ddlCity.SelectedValue = "0";
        txtEmailD.Text = string.Empty;
    }


    private void SendEmailViaAmazon(String EmailContent, String EmailSubject, String toEmail, String attachmentPath, String PageName)
    {
        // Replace sender@example.com with your "From" address. 
        // This address must be verified with Amazon SES.
        const String FROM = "mailer@hafeleindia.co.in";//"no_reply@id8labs.com";no_reply@hafeleindia.co.in
        const String FROMNAME = "Hafele Digital Locks";

        // Replace recipient@example.com with a "To" address. If your account 
        // is still in the sandbox, this address must be verified.
        // String TO = toEmail; //"hardik@id8labs.com";
        String TO = toEmail;

        // Replace smtp_username with your Amazon SES SMTP user name.
        const String SMTP_USERNAME = "AKIAJFPTGFQQFRPAEJ5A";

        // Replace smtp_password with your Amazon SES SMTP user name.
        const String SMTP_PASSWORD = "AlFsOm/WKvHoZwWBVqWYYIxD3FRXH06E7ysj4VC8jteG";

        // (Optional) the name of a configuration set to use for this message.
        // If you comment out this line, you also need to remove or comment out
        // the "X-SES-CONFIGURATION-SET" header below.
        //const String CONFIGSET = "ConfigSet";

        // If you're using Amazon SES in a region other than US West (Oregon), 
        // replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP  
        // endpoint in the appropriate Region.
        const String HOST = "email-smtp.us-east-1.amazonaws.com";

        // The port you will connect to on the Amazon SES SMTP endpoint. We
        // are choosing port 587 because we will use STARTTLS to encrypt
        // the connection.
        const int PORT = 587;

        // The subject line of the email
        String SUBJECT = EmailSubject;
        //"Amazon SES test (SMTP interface accessed using C#)";

        // The body of the email
        String BODY = EmailContent;
        //"<h1>Amazon SES Test</h1>" +
        //"<p>This email was sent through the " +
        //"<a href='https://aws.amazon.com/ses'>Amazon SES</a> SMTP interface " +
        //"using the .NET System.Net.Mail library.</p>";

        // Create and build a new MailMessage object
        MailMessage message = new MailMessage();
        message.IsBodyHtml = true;
        message.From = new MailAddress(FROM, FROMNAME);



        if (attachmentPath != null && attachmentPath.Trim() != "")
        {
            //Attachment att = new Attachment(attachmentPath);

            DirectoryInfo d = new DirectoryInfo(attachmentPath);


            FileInfo[] Files = d.GetFiles("*.xlsx");

            foreach (FileInfo file in Files)
            {
                Attachment att = new Attachment(file.FullName);
                message.Attachments.Add(att);
            }


        }

        message.To.Add("customer.care@hafeleindia.com");
      
        message.To.Add("ruzario.fernandes@hafeleindia.com");
        //message.To.Add("aarohi@id8labs.com");
        message.To.Add("Tushna@id8labs.com");
        message.To.Add("Shraddha.Gurav@hafeleindia.com");
        message.To.Add("Priyanka.Rathod@hafeleindia.com");
		message.To.Add("Rupesh@id8labs.com");
        message.To.Add("mahrukh@id8labs.com");
        message.To.Add("ravina@id8labs.com");

        //message.To.Add("yesha@id8labs.com");
        //message.To.Add("hamid@id8labs.com");       
        // message.To.Add("poojas@id8labs.com");


        //message.To.Add(new MailAddress(TO));

        message.Subject = SUBJECT;

        message.Body = BODY;

        //if (message.Attachments.Count == 0)
        //    message.Body = "No Conversations recorded";

        //var attachment = new Attachment()

        //message.Attachments.Add()
        // Comment or delete the next line if you are not using a configuration set
        //message.Headers.Add("X-SES-CONFIGURATION-SET", CONFIGSET);

        // Create and configure a new SmtpClient
        SmtpClient client =
         new SmtpClient(HOST, PORT);
        // Pass SMTP credentials
        client.Credentials =
         new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
        // Enable SSL encryption
        client.EnableSsl = true;

        // Send the email. 
        try
        {
            //Console.WriteLine("Attempting to send email...");
            client.Send(message);
            //MessageBox.Show("Email sent!");
        }
        catch (Exception ex)
        {
            //Console.WriteLine("The email was not sent.");
            //MessageBox.Show("Error message: " + ex.Message);
        }



    }

    //private void clear1()
    //{
    //    txtEmailD.Text = string.Empty;
    //}
}
