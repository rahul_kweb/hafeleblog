﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="re-size.aspx.cs" Inherits="re_vel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Capacitive Fingerprint Sensor | Re-Size |Hafele Digital Lock</title>
    <meta name="description" content="With capacitive fingerprint sensor and other smart safety enhancers, Hafele's RE-size combines technology and design to offer thorough security for your home.">

    <link rel="canonical" href="https://digital-locks.hafeleindia.co.in/re-size-electronic-door-locks" />

    <meta property="og:url" content="https://digital-locks.hafeleindia.co.in/re-size-electronic-door-locks" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Capacitive Fingerprint Sensor | Re-Size |Hafele Digital Lock" />
    <meta property="og:description" content="With capacitive fingerprint sensor and other smart safety enhancers, Hafele's RE-size combines technology and design to offer thorough security for your home." />
    <meta property="og:image" content="https://digital-locks.hafeleindia.co.in/img/banner_size.jpg" />

    <meta name="twitter:card" content="summary" />
    <meta property="og:url" content="https://digital-locks.hafeleindia.co.in/re-size-electronic-door-locks" />
    <meta property="og:title" content="Capacitive Fingerprint Sensor | Re-Size |Hafele Digital Lock" />
    <meta property="og:description" content="With capacitive fingerprint sensor and other smart safety enhancers, Hafele's RE-size combines technology and design to offer thorough security for your home." />
    <meta property="og:image" content="https://digital-locks.hafeleindia.co.in/img/banner_size.jpg" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="myh1">RE-SIZE - Capacitive Fingerprint Sensor</h1>

    <!-- content sec -->
    <div class="content_wrapper">

        <!-- banner sec -->
        <div class="prodbbanner-img">
            <img src="img/banner_size.jpg" alt="" class="img-responsive" />
        </div>
        <!-- // banner sec -->
        <!-- Pr Detail sec -->
        <section class="PrDetail_sec productindividual-wrap">
            <div class="container">

                <div class="row clear">
                    <div class="col-lg-4 col-sm-4">
                        <div class="prImg">
                            <img src="img/prImg/re-size.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-8 col-sm-8">
                        <div class="PrDeas">
                            <h2>RE-SIZE</h2>
                            <h5>Because your fashionable door needs its ‘right’ match…</h5>
                            <figcaption>Because your fashionable door needs its ‘right’ match… ‘Peace’ for the mind; ‘Win’ for the eye: Häfele’s RE-Size Digital Lock uniquely combines incomparable technology with an aesthetic design, bringing the much desired ornamentation to your door while also securing your home with planned access. Owing to its singular design, RE-Size allows you to combine its existence with any preferred lever handle as per your choice.
                                This brings in a whiff of customization to your approach while designing the security needs for your door- you may not otherwise ﬁnd such ﬂexibility in the market. The exclusivity of Hafele’s RE-Size lies in its potential to jump-start operations through an integrated power bank which can be easily charged through a USB Cable. With 4 different access modes – Finger Print, Key Pad, RFID and Mechanical Key – Häfele’s RE-Size Digital Lock brings you optimum security.

                            </figcaption>


                        </div>
                    </div>
                </div>

                <div class="ro clear">
                    <div class="iconwrpSec">
                        <div class="col-lg-12 row">
                            <div class="subHeading">Mnemonic Guide: <span>RE-Size</span></div>
                        </div>

                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">ACCESS MODES:</h4>
                                            <ul>
                                                <li>
                                                    <img src="img/icon/access/icon_1.jpg" alt="">
                                                    <p>Capacitive Fingerprint (upto 43 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_2.jpg" alt="">
                                                    <p>Key-pad or Password (upto 7 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_3.jpg" alt="">
                                                    <p>RFID (upto 43 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_4.jpg" alt="">
                                                    <p>Mechanical Key (upto 2 keys)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">SMART TECHNOLOGIES: </h4>
                                            <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_1.jpg" alt="">
                                                        <p>Smart Password </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_2.jpg" alt="">
                                                        <p>Smart Voice</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_3.jpg" alt="">
                                                        <p>Smart Etiquettes </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_4.jpg" alt="">
                                                        <p>Smart Security</p>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_5.jpg" alt="">
                                                        <p>Smart Freeze </p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">LOCKING MODES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/locking/icon_3.jpg" alt="">
                                                    <p>Auto Locking </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_2.jpg" alt="">
                                                    <p>Manual Locking</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_1.jpg" alt="">
                                                    <p>Privacy Locking </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_5.jpg" alt="">
                                                    <p>Defense Locking</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">OPERATIONAL FEATURES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/oprational/icon_1.jpg" alt="">
                                                    <p>Panic Exit </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/oprational/icon_3.jpg" alt="">
                                                    <p>Non-handed Operation</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">USER ACCESS RIGHTS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/userAccess/icon_2.jpg" alt="">
                                                    <p>Guest</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_3.jpg" alt="">
                                                    <p>User</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_4.jpg" alt="">
                                                    <p>Admin</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">ALARMS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/alarms/icon_1.jpg" alt="">
                                                    <p>Low Battery </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_4.jpg" alt="">
                                                    <p>Door not Locked</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Battery:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_1.jpg" alt="">
                                                    <p>AA (3,000 cycles - 4 batteries) </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/battery/icon_2.jpg" alt="">
                                                    <p>Jump start (power bank)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">DOOR THICKNESS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_1.jpg" alt="">
                                                    <p>40-60mm</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_2.jpg" alt="">
                                                    <p>60-90mm, 90-120mm*</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-8 col-sm-8">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Finish:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_1.jpg" alt="">
                                                    <p>Matt Black, Polish Chrome + Matt Black</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 clear">
                            <ul class="lockingguide">
                                <li>
                                    <p class="lockingBTN">Lever handle selection guide <span>(to be ordered separately)</span></p>
                                </li>
                            </ul>
                            <ul class="lockingguide bticon">
                                <li>
                                    <div class="turnLeve">
                                        <img src="img/img_2.jpg" alt="">
                                    </div>
                                </li>
                                <li>
                                    <div class="turnLeve">
                                        <img src="img/img_3.jpg" alt="">
                                    </div>
                                </li>
                                <li>
                                    <div class="turnLeve">
                                        <img src="img/img_4.jpg" alt="">
                                    </div>
                                </li>
                                <li>
                                    <div class="turnLeve">
                                        <img src="img/img_5.jpg" alt="">
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- // Pr Detail sec -->


        <%--<div class="prodctdummy-content container">
            <p> </p>
        </div>--%><!--End of the prodct dummy content-->

        <div class="prodctboxes-imgcontainer">
            <ul class="list-inline">
                <li>
                    <div class="individualprod-boxwrap">
                    </div>
                    <!--End of the individual box wrap-->
                </li>
                <li>
                    <div class="individualprod-boxwrap">
                    </div>
                    <!--End of the individual box wrap-->
                </li>
                <li>
                    <div class="individualprod-boxwrap">
                    </div>
                    <!--End of the individual box wrap-->
                </li>

            </ul>
        </div>
        <!--End of the prodct boxes img container-->

        <!-- Pr PrVideo_sec -->
        <section class="PrVideo_sec">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>RE-Size <span>Videos</span></h2>
                        <div class="video_frames yt_videos">
                            <div class="col-sm-3">
                                <div class="videoWrp">
                                    <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/3n1pb5Q9SgE?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <%--<h4>Hafele Digital Locks – User guide – RE-AL</h4>--%>
                                    <p>User guide</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="videoWrp">
                                    <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/PtuYp5zMB7c?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <%-- <h4>Hafele Digital Locks – Installation video – RE-AL</h4>--%>
                                    <p>Installation video</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="videoWrp">
                                    <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/fca0DS8YJXw?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <%--<h4>Hafele Digital Locks – Quick Installation video – RE-AL</h4>--%>
                                    <p>Quick Installation video</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="videoWrp">
                                    <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/FmoscI7XwOs?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <%--<h4>Hafele Digital Locks – Manual – RE-AL</h4>--%>
                                    <p>Manual</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Pr PrVideo_sec -->


        <!--Start of the explore range-->
        <section class="exploreSec">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Explore <span>range</span></h2>
                        <div id="slider3D" class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-size.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Size</h5>
                                        <p>Because your fashionable door needs its ‘right’ match…</p>
                                    </div>
                                    <img src="img/prImgsl/re-size.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-place.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Place</h5>
                                        <p>Because your mechanical lock needs an upgrade…</p>
                                    </div>
                                    <img src="img/prImgsl/re-place.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-bell.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Bell</h5>
                                        <p>Because you need a <span class="strikethrough">99.99</span> “100” percent…</p>
                                    </div>
                                    <img src="img/prImgsl/re-bell.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-veal.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImgsl/pr_1.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-al.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-AL</h5>
                                        <p>Because you need a better security solution…</p>
                                    </div>
                                    <img src="img/prImgsl/re-ai.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-tro.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-TRO</h5>
                                        <p>Because your office cabins also deserve the same digital security as your homes</p>
                                    </div>
                                    <img src="img/prImgsl/re-tro.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-design.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-DESIGN</h5>
                                        <p>Because your main doors deserve the best of both – top notch security and elegant design </p>
                                    </div>
                                    <img src="img/prImgsl/re-design.png" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End of the explore range-->



    </div>
    <!-- // content sec -->
</asp:Content>

