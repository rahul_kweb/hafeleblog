﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="re-tro.aspx.cs" Inherits="re_vel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Smart Locks for Home | Re-Tro |Hafele Digital Lock</title>
    <meta name="description" content="Your office too deserve the same security as your home & Hafele digital lock Re-Tro ensures exactly the same. Simply mount the lock on the glass door & it's done.">

    <link rel="canonical" href="https://digital-locks.hafeleindia.co.in/re-tro-security-door-locks" />

    <meta property="og:url" content="https://digital-locks.hafeleindia.co.in/re-tro-security-door-locks" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Smart Locks for Home | Re-Tro |Hafele Digital Lock" />
    <meta property="og:description" content="Your office too deserve the same security as your home & Hafele digital lock Re-Tro ensures exactly the same. Simply mount the lock on the glass door & it's done." />
    <meta property="og:image" content="https://digital-locks.hafeleindia.co.in/img/banner-retro.jpg" />


    <meta name="twitter:card" content="summary" />
    <meta property="og:url" content="https://digital-locks.hafeleindia.co.in/re-tro-security-door-locks" />
    <meta property="og:title" content="Smart Locks for Home | Re-Tro |Hafele Digital Lock" />
    <meta property="og:description" content="Your office too deserve the same security as your home & Hafele digital lock Re-Tro ensures exactly the same. Simply mount the lock on the glass door & it's done." />
    <meta property="og:image" content="https://digital-locks.hafeleindia.co.in/img/banner-retro.jpg" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="myh1">RE-TRO Digital Security Lock for Glass Door Application</h1>

    <!-- content sec -->
    <div class="content_wrapper">

        <!-- banner sec -->
        <div class="prodbbanner-img">
            <img src="img/banner-retro.jpg" alt="" class="img-responsive" />
        </div>

        <!-- // banner sec -->
        <!-- Pr Detail sec -->
        <section class="PrDetail_sec productindividual-wrap">


            <div class="container">

                <div class="row clear">
                    <div class="col-lg-4 col-sm-4">
                        <div class="prImg">
                            <img src="img/prImg/pr_2.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-8 col-sm-8">
                        <div class="PrDeas">
                            <h2>RE-Tro</h2>
                            <h5>Because your office cabins also deserve the same digital security as your homes</h5>
                            <figcaption>Unquestionable security for Glass Door Applications: After managing your home security needs with its remarkable RE-invent Access Series,Häfele now launches the ideal digital security solution for offices. The
                            new RETRO Digital Lock from Häfele comes in a unique and elegant design ideally suited for glass door applications. The beauty of the RETRO lock is that it demands absolutely no pre-work on the glass door like creating tedious cut-outs or fixing grooves – the lock can be simply
                            mounted onto to an existing glass door through a retro fixing mechanism; easy plug-and-play. RETRO combines two access modes – Keypad and  RFID – making it a highly secure option for office cabins or other such
                            areas that need limited or exclusive access.                            </figcaption>


                        </div>
                    </div>
                </div>

                <div class="ro clear">
                    <div class="iconwrpSec">
                        <div class="col-lg-12 row">
                            <div class="subHeading">Mnemonic Guide: <span>RE-TRO</span></div>
                        </div>

                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">ACCESS MODES:</h4>
                                            <ul>
                                                <li>
                                                    <img src="img/icon/access/icon_2.jpg" alt="">
                                                    <p>Key-pad or Password (upto 3 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_3.jpg" alt="">
                                                    <p>RFID (upto 100 unique accesses)</p>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">SMART TECHNOLOGIES: </h4>
                                            <%--<div class="col-lg-6 col-sm-6 PR_specifcation">--%>
                                            <ul class="clear ">
                                                <li>
                                                    <img src="img/icon/smartTech/icon_1.jpg" alt="">
                                                    <p>Smart Password </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/smartTech/icon_3.jpg" alt="">
                                                    <p>Smart Etiquettes </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/smartTech/icon_2.jpg" alt="">
                                                    <p>Smart Voice</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/smartTech/icon_5.jpg" alt="">
                                                    <p>Smart Freeze </p>
                                                </li>

                                            </ul>
                                            <%--</div>--%>
                                            <%-- <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_4.jpg" alt="">
                                                        <p>Smart Security</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_6.jpg" alt="">
                                                        <p>Smart Night Vision</p>
                                                    </li>
                                                </ul>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">LOCKING MODES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/locking/icon_3.jpg" alt="">
                                                    <p>Auto Locking </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_1.jpg" alt="">
                                                    <p>Privacy Locking</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_2.jpg" alt="">
                                                    <p>Manual Locking</p>
                                                </li>

                                                <li>
                                                    <img src="img/icon/locking/icon_5.jpg" alt="">
                                                    <p>Defense Locking</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">OPERATIONAL FEATURES:</h4>
                                            <ul class="clear">
                                                <%--<li>
                                                    <img src="img/icon/oprational/icon_1.jpg" alt="">
                                                    <p>Panic Exit </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/oprational/icon_2.jpg" alt="">
                                                    <p>Audit Trails<br>
                                                        <div style="font-size: 10px;">(Up to 30,000 records can be stored)</div>
                                                    </p>
                                                </li>--%>
                                                <li>
                                                    <img src="img/icon/oprational/icon_3.jpg" alt="">
                                                    <p>Non-handed Operation</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">USER ACCESS RIGHTS:</h4>
                                            <ul class="clear">
                                                <%--<li>
                                                    <img src="img/icon/userAccess/icon_1.jpg" alt="">
                                                    <p>Super Admin </p>
                                                </li>--%>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_3.jpg" alt="">
                                                    <p>User</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_4.jpg" alt="">
                                                    <p>Admin</p>
                                                </li>

                                                <%--<li>
                                                    <img src="img/icon/userAccess/icon_2.jpg" alt="">
                                                    <p>Guest</p>
                                                </li>--%>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">ALARMS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/alarms/icon_1.jpg" alt="">
                                                    <p>Low Battery </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_2.jpg" alt="">
                                                    <p>Break-in</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_3.jpg" alt="">
                                                    <p>High Temperature</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_4.jpg" alt="">
                                                    <p>Door not Locked</p>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Battery:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_3.jpg" alt="">
                                                    <p>AA (4 batteries) </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/battery/icon_4.jpg" alt="">
                                                    <p>Jump start (9V)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">DOOR THICKNESS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_1.jpg" alt="">
                                                    <p>10-12 mm</p>
                                                </li>
                                                <%--<li>
                                                    <img src="img/icon/doorThickness/icon_2.jpg" alt="">
                                                    <p>65-90mm, 90-100mm*</p>
                                                </li>--%>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Finish:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_1.jpg" alt="">
                                                    <p>Black & Chrome </p>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <%--     <div class="col-lg-12 clear">
                        <ul class="lockingguide">
                            <li>
                                <p class="lockingBTN">Locking Guide</p>
                            </li>
                            <li>
                                <div class="turnLeve">
                                    <img src="img/icon/dt.png" alt="">
                                    <p>Turn lever handle in upward direction after closing the door</p>
                                </div>
                            </li>
                        </ul>
                    </div>--%>
                </div>


            </div>
        </section>



        <!-- // Pr Detail sec -->


        <%--<div class="prodctdummy-content container">
            <p></p>
        </div>--%><!--End of the prodct dummy content-->

        <div class="prodctboxes-imgcontainer">
            <ul class="list-inline">
                <li>
                    <div class="individualprod-boxwrap">
                    </div>
                    <!--End of the individual box wrap-->
                </li>
                <li>
                    <div class="individualprod-boxwrap">
                    </div>
                    <!--End of the individual box wrap-->
                </li>
                <li>
                    <div class="individualprod-boxwrap">
                    </div>
                    <!--End of the individual box wrap-->
                </li>

            </ul>
        </div>
        <!--End of the prodct boxes img container-->


        <!-- Pr PrVideo_sec -->
        <section class="PrVideo_sec" style="display: none">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>RE-Veal <span>Videos</span></h2>
                        <div class="video_frames yt_videos">
                            <div class="col-sm-3">
                                <div class="videoWrp">
                                    <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/kM5Fz9MkR6g?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <%--<h4>Hafele Digital Locks – User guide – RE-Veal</h4>--%>
                                    <p>User guide</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="videoWrp">
                                    <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/96SFjwBRXzs?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <%--<h4>Hafele Digital Locks – Installation video – RE-Veal</h4>--%>
                                    <p>Installation video</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="videoWrp">
                                    <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/54oJkW1qwCM?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <%--<h4>Hafele Digital Locks – Quick Installation – RE-Veal</h4>--%>
                                    <p>Quick Installation video</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="videoWrp">
                                    <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/SZS6CfXjn6k?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <%--<h4>Hafele Digital Locks – Manual – RE-Veal</h4>--%>
                                    <p>Manual</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Pr PrVideo_sec -->

        <!--Start of the explore range-->
        <section class="exploreSec">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Explore <span>range</span></h2>
                        <div id="slider3D" class="owl-carousel owl-theme">
                            
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-veal.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImgsl/pr_1.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-al.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-AL</h5>
                                        <p>Because you need a better security solution…</p>
                                    </div>
                                    <img src="img/prImgsl/re-ai.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-tro.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-TRO</h5>
                                        <p>Because your office cabins also deserve the same digital security as your homes</p>
                                    </div>
                                    <img src="img/prImgsl/re-tro.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-design.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-DESIGN</h5>
                                        <p>Because your main doors deserve the best of both – top notch security and elegant design </p>
                                    </div>
                                    <img src="img/prImgsl/re-design.png" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End of the explore range-->


    </div>
    <!-- // content sec -->

    <script>
        $(document).ready(function () {
            //$('.viewall-btnwrap .viewallbtn').click(function () {
            //    $('.navigation-listing li').removeClass('active');
            //    $('.navigation-listing .productsdesktop').addClass('active');

            //});
        });
    </script>
</asp:Content>

