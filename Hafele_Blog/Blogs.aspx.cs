﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Blogs : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindFeatureBlogs();
            BindFeatureBlogsTop1();
            BindLatestBlogs();
            BindTopTwoPopular();
            BindMustRead();
            BindBottomBlogs();
        }


    }

    public string BindFeatureBlogs()
    {
        StringBuilder strBuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Blogs 'BindFeatureBlogs'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strBuild.Append("<div class='position-relative'>");
                strBuild.Append("<div class='bannerPic'><img src='/Content/uploads/Blogs/" + dr["Image"].ToString() + "' class='img-fluid'></div>");
                strBuild.Append("<div class='bannerText'>");
                strBuild.Append("<a href='/blogdetails/" + dr["Slug"].ToString() + "'>");
                strBuild.Append("<h1 class='animated' data-animation-in='fadeInLeft'>" + dr["Title"].ToString() + "</h1>");
                strBuild.Append("<p class='animated' data-animation-in='fadeInLeft'>" + dr["Description"].ToString() + "</p>");
                strBuild.Append("</a>");
                strBuild.Append("</div>");
                strBuild.Append("</div>");

            }
        }
        ltrFeatureBlogs.Text = strBuild.ToString();

        return ltrFeatureBlogs.Text;
    }


    public string BindFeatureBlogsTop1()
    {
        StringBuilder strBuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Blogs 'BindFeatureBlogsTop1'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strBuild.Append("<div class='blogBox'>");
                strBuild.Append("<img src='/Content/uploads/Blogs/" + dr["Image"].ToString() + "' class='img-fluid mb-4'>");
                strBuild.Append("<div class='blogBrief'>");
                strBuild.Append("<span class='categoryTitle'>" + dr["Category"].ToString() + "</span>   |  <span class='blogDate'>" + dr["PostDate"].ToString() + "</span>");
                strBuild.Append("</div>");
                strBuild.Append("<a href='/blogdetails/" + dr["Slug"].ToString() + "'>");
                strBuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                strBuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                strBuild.Append("</a>");
                strBuild.Append("</div>");

            }
        }
        ltrFeatureBlogsTop1.Text = strBuild.ToString();

        return ltrFeatureBlogsTop1.Text;
    }


    public void BindLatestBlogs()
    {
        StringBuilder strBuildCat = new StringBuilder();
        StringBuilder strBuild = new StringBuilder();
        DataTable dt = new DataTable();




        dt = utility.Display("Exec Proc_Blogs 'BindLatestbyCategory'");

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strBuildCat.Append("<div class='blogCateogry'>" + dr["Category"].ToString() +"</div>");

                DataTable dt1 = new DataTable();

                strBuild.Append("<div>");
                strBuild.Append("<div class=postSlider pb-4>");

                dt1 = utility.Display("Exec Proc_Blogs 'BindLatestBlogs',0,'','"+ dr["Category"].ToString() + "'");
                if (dt1.Rows.Count > 0)
                {
                  
                    foreach (DataRow dr1 in dt1.Rows)
                    {

                        strBuild.Append("<div class='pr-5'>");
                        strBuild.Append("<div class='blogBox'>");
                        strBuild.Append("<a href='/blogdetails/" + dr1["Slug"].ToString() + "'>");
                        strBuild.Append("<div class='blogPic mb-4'><img src='/Content/uploads/Blogs/" + dr1["Image"].ToString() + "'></div>");
                        strBuild.Append("<div class='blogBrief'>");
                        strBuild.Append("<span class='categoryTitle'>" + dr1["Category"].ToString() + "</span>   |  <span class='blogDate'>" + dr1["PostDate"].ToString() + "</span>");
                        strBuild.Append("</div>");
                        strBuild.Append("<h1>" + dr1["Title"].ToString() + "</h1>");
                        strBuild.Append("<p>" + dr1["Description"].ToString() + "</p>");
                        strBuild.Append("</a>");
                        strBuild.Append("</div>");
                        strBuild.Append("</div>");

                    }
             
                }

                strBuild.Append("</div>");
                strBuild.Append("</div>");

            }
        }



        ltrLatestCategory.Text = strBuildCat.ToString();
        ltrLatest.Text = strBuild.ToString();

    }


    public string BindTopTwoPopular()
    {
        StringBuilder strBuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Blogs 'BindTopFivePopularBlogs'");

        int count = 0;
        strBuild.Append("<div class='col-md-8'>");
        strBuild.Append("<h1 class='title mb-4' data-aos='zoom-in'>Populer <span>posts</span></h1>");
        strBuild.Append("<div class='row'>");
        if (dt.Rows.Count > 0)
        {
          
                foreach (DataRow dr in dt.Rows)
                {
                if (count < 2)
                {

                    strBuild.Append("<div class='col-md-6' data-aos='fade-up' data-aos-delay='200'>");
                    strBuild.Append("<div class='blogBox'>");
                    strBuild.Append("<a href='/blogdetails/" + dr["Slug"].ToString() + "'>");
                    strBuild.Append("<div class='blogPic mb-4'><img src='/Content/uploads/Blogs/" + dr["Image"].ToString() + "'></div>");
                    strBuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                    strBuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                    strBuild.Append("</a>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    count++;
                }
          
            } 
            strBuild.Append("</div>");
            strBuild.Append("</div>");

            strBuild.Append("<div class='col-md-4'>");
            strBuild.Append("<h1 class='title mb-4' data-aos='zoom-in'>Post <span>list</span></h1>");
            

            if (count > 1)
            {
                strBuild.Append("<ul class='postList'>");
                dt.Rows.RemoveAt(1);
                dt.Rows.RemoveAt(0);
                foreach (DataRow dr in dt.Rows)
                {
                    strBuild.Append("<li data-aos='fade-up' data-aos-delay='200'>");
                    strBuild.Append("<a href='/blogdetails/" + dr["Slug"].ToString() + "'>");
                    strBuild.Append("<div class='blogBrief'>");
                    strBuild.Append("<span class='categoryTitle'>" + dr["Category"].ToString() + "</span>   |  <span class='blogDate'>" + dr["PostDate"].ToString() + "</span>");
                    strBuild.Append("</div>");
                    strBuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                    strBuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                    strBuild.Append("</a>");
                    strBuild.Append("</li>");
                }
                strBuild.Append("</ul>");

            }
         
            strBuild.Append("</div>");
        }

        else
        {
            strBuild.Append("</div>");
            strBuild.Append("</div>");

            strBuild.Append("<div class='col-md-4'>");
            strBuild.Append("<h1 class='title mb-4' data-aos='zoom-in'>Post <span>list</span></h1>");
            strBuild.Append("<ul class='postList'>");

            strBuild.Append("</ul>");
            strBuild.Append("</div>");

        }
        ltrTopTwoPopular.Text = strBuild.ToString();

        return ltrTopTwoPopular.Text;
    }



    public string BindMustRead()
    {
        StringBuilder strBuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Blogs 'BindMustRead'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {

                strBuild.Append("<div>");
                strBuild.Append("<a href='/blogdetails/" + dr["Slug"].ToString() + "' class='articleBox d-flex h-100'>");
                strBuild.Append("<div class='align-self-center text-center w-100'>");
                strBuild.Append("<div class='blogBrief'>");
                strBuild.Append("<span class='categoryTitle'>" + dr["Category"].ToString() + "</span>   |  <span class='blogDate'>" + dr["PostDate"].ToString() + "</span>");
                strBuild.Append("</div>");
                strBuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                strBuild.Append("</div>");
                strBuild.Append("</a>");
                strBuild.Append("</div>");

            }
        }
        ltrMustRead.Text = strBuild.ToString();

        return ltrMustRead.Text;
    }


    public string BindBottomBlogs()
    {
        StringBuilder strBuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Blogs 'BindBottomBlogs'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {

                strBuild.Append("<div class='col-md-6 col-lg-4  mb-4 mb-md-5'  data-aos='fade-up' data-aos-delay='400'>");
                strBuild.Append("<div class='position-relative bottomBlogBox'>");
                strBuild.Append("<div class='blogPic mb-4'><img src='/Content/uploads/Blogs/" + dr["Image"].ToString() + "'></div>");
                strBuild.Append("<div class='bottomBlogText'>");
                strBuild.Append("<a href='/blogdetails/" + dr["Slug"].ToString() + "'>");
                strBuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                strBuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                strBuild.Append("</a>");
                strBuild.Append("</div>");
                strBuild.Append("</div>");
                strBuild.Append("</div>");

            }
        }
        ltrBottomBlogs.Text = strBuild.ToString();

        return ltrBottomBlogs.Text;
    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string Email = txtEmail.Text.ToString().Trim();
        if (!CheckEmail(txtEmail.Text.ToString().Trim()))
        {
            using (SqlCommand cmd = new SqlCommand("Proc_BlogSubscribe"))
            {

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Email", Email);

                if (utility.Execute(cmd))
                {

                    txtEmail.Text = "";
                }


            }
        }
      
    }

    public  bool CheckEmail(string useroremail)
    {
        bool status;

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["HafeleIconicCMS"].ConnectionString);

        using (SqlCommand cmd = new SqlCommand("Proc_BlogSubscribe", con))
            {
            con.Open();
             cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "CheckEmailExist");
            cmd.Parameters.AddWithValue("@Email", useroremail.Trim());
            SqlDataReader dr = cmd.ExecuteReader();
           
            if (dr.HasRows)
            {
                lblStatus.Text = "Email Already Exist";
                status = true;
            }
            else
            {

                txtEmail.Text = "";
                status = false;
            }

        }
        con.Close();

        return status;
    }
}