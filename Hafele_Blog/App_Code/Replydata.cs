﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Replydata
/// </summary>
public class Replydata
{
    public Replydata()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int CommentId { get; set; }
    public string Reply { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
}