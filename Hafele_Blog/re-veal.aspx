﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="re-veal.aspx.cs" Inherits="re_vel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Digital Lock for Main Door | RE-Veal | Hafele Digital Lock</title>
    <meta name="description" content="RE-veal is India's first face recognition Hafele Digital Lock with night vision technology & intruder capture feature offering the highest form of security.">

    <link rel="canonical" href="https://digital-locks.hafeleindia.co.in/re-veal-digital-door-lock" />

    <meta property="og:url" content="https://digital-locks.hafeleindia.co.in/re-veal-digital-door-lock" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Digital Lock for Main Door | RE-Veal | Hafele Digital Lock" />
    <meta property="og:description" content="RE-veal is India's first face recognition Hafele Digital Lock with night vision technology & intruder capture feature offering the highest form of security." />
    <meta property="og:image" content="https://digital-locks.hafeleindia.co.in/img/banner_revel.jpg" />

    <meta name="twitter:card" content="summary" />
    <meta property="og:url" content="https://digital-locks.hafeleindia.co.in/re-veal-digital-door-lock" />
    <meta property="og:title" content="Digital Lock for Main Door | RE-Veal | Hafele Digital Lock" />
    <meta property="og:description" content="RE-veal is India's first face recognition Hafele Digital Lock with night vision technology & intruder capture feature offering the highest form of security." />
    <meta property="og:image" content="https://digital-locks.hafeleindia.co.in/img/prImg/pr_1.png" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="myh1">RE-VEAL - Face Recognition Digital Door Lock</h1>

    <!-- content sec -->
    <div class="content_wrapper">

        <!-- banner sec -->
        <div class="prodbbanner-img">
            <img src="img/banner_revel.jpg" alt="" class="img-responsive" />
        </div>

        <!-- // banner sec -->
        <!-- Pr Detail sec -->
        <section class="PrDetail_sec productindividual-wrap">


            <div class="container">

                <div class="row clear">
                    <div class="col-lg-4 col-sm-4">
                        <div class="prImg">
                            <img src="img/prImg/pr_1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-8 col-sm-8">
                        <div class="PrDeas">
                            <h2>RE-Veal</h2>
                            <h5>Because you need the highest form of security…</h5>
                            <figcaption>For the Judicious-jivers who would go the extra mile to choose the best and most holistic solution available; a profound lot who leave nothing to chance. Smile for Security: Häfele’s RE-Veal Digital Lock carefully scrutinizes every individual need that you may have from your home security system and presents itself as the ideal answer for all those needs. This fully-loaded face-recognition lock comes with the highest standards of technology that reads over 170 points on the user’s face – this means that it is highly sensitive to even the flinch of a nerve on your face and will only allow access if you exactly emulate the expression that was pre-set as your access recognition.
                                With 5 dirent access modes – Face Recognition, Finger Print, Key Pad, RFID and Mechanical Key – Häfele’s RE-Veal Digital Lock is the authority in home security and access planning. For double assurance, you can combine two access modes as per your choice, making it impossible for any unwanted break-ins. And should someone dare try a break-in, our integrated intruder-capture feature can show you an image of the person who made this futile attempt. Häfele’s RE-Veal Digital Lock comes with lithium batteries that are tested to last for 8000 cycles (or approximately one year) providing you with a hassle-free experience. The most singular feature that sets it apart from others is RE-Veal’s night-vision that recognizes a registered face even in pitch darkness. So it’s time to leave your security worries behind, flash your best smile at this miracle machine and let yourself in…
                            </figcaption>


                        </div>
                    </div>
                </div>

                <div class="ro clear">
                    <div class="iconwrpSec">
                        <div class="col-lg-12 row">
                            <div class="subHeading">Mnemonic Guide: <span>RE-Veal</span></div>
                        </div>

                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">ACCESS MODES:</h4>
                                            <ul>
                                                <li>
                                                    <img src="img/icon/access/icon_1.jpg" alt="">
                                                    <p>Silk id Fingerprint (upto 100 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_2.jpg" alt="">
                                                    <p>Key-pad or Password (upto 100 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_3.jpg" alt="">
                                                    <p>RFID (upto 100 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_4.jpg" alt="">
                                                    <p>Mechanical Key (4 user keys,1 construction key)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_5.jpg" alt="">
                                                    <p>Face Recognition (upto 100 unique accesses)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">SMART TECHNOLOGIES: </h4>
                                            <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_1.jpg" alt="">
                                                        <p>Smart Password </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_3.jpg" alt="">
                                                        <p>Smart Etiquettes </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_5.jpg" alt="">
                                                        <p>Smart Freeze </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_7.jpg" alt="">
                                                        <p>Smart Intruder (upto 10 photos)</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_2.jpg" alt="">
                                                        <p>Smart Voice</p>
                                                    </li>



                                                </ul>
                                            </div>
                                            <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_4.jpg" alt="">
                                                        <p>Smart Security</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_6.jpg" alt="">
                                                        <p>Smart Night Vision</p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">LOCKING MODES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/locking/icon_1.jpg" alt="">
                                                    <p>Privacy Locking</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_2.jpg" alt="">
                                                    <p>Manual Locking</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">OPERATIONAL FEATURES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/oprational/icon_1.jpg" alt="">
                                                    <p>Panic Exit </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/oprational/icon_2.jpg" alt="">
                                                    <p>
                                                        Audit Trails<br>
                                                        <div style="font-size: 10px;">(Up to 30,000 records can be stored)</div>
                                                    </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/oprational/icon_3.jpg" alt="">
                                                    <p>Non-handed Operation</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">USER ACCESS RIGHTS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/userAccess/icon_1.jpg" alt="">
                                                    <p>Super Admin </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_4.jpg" alt="">
                                                    <p>Admin</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_3.jpg" alt="">
                                                    <p>User (upto 100 user)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_2.jpg" alt="">
                                                    <p>Guest</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">ALARMS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/alarms/icon_1.jpg" alt="">
                                                    <p>Low Battery </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_2.jpg" alt="">
                                                    <p>Break-in</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_3.jpg" alt="">
                                                    <p>High Temperature</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Battery:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_1.jpg" alt="">
                                                    <p>Lithium (8,000 cycles, onced full charged)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/battery/icon_2.jpg" alt="">
                                                    <p>Jump start (9V)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">DOOR THICKNESS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_1.jpg" alt="">
                                                    <p>40-50mm, 50-65mm</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_2.jpg" alt="">
                                                    <p>65-90mm, 90-100mm*</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 clear">
                        <ul class="lockingguide">
                            <li>
                                <p class="lockingBTN">Locking Guide</p>
                            </li>
                            <li>
                                <div class="turnLeve">
                                    <img src="img/icon/dt.png" alt="">
                                    <p>Turn lever handle in upward direction after closing the door</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>


            </div>
        </section>



        <!-- // Pr Detail sec -->


        <%--<div class="prodctdummy-content container">
            <p></p>
        </div>--%><!--End of the prodct dummy content-->

        <div class="prodctboxes-imgcontainer">
            <ul class="list-inline">
                <li>
                    <div class="individualprod-boxwrap">
                    </div>
                    <!--End of the individual box wrap-->
                </li>
                <li>
                    <div class="individualprod-boxwrap">
                    </div>
                    <!--End of the individual box wrap-->
                </li>
                <li>
                    <div class="individualprod-boxwrap">
                    </div>
                    <!--End of the individual box wrap-->
                </li>

            </ul>
        </div>
        <!--End of the prodct boxes img container-->


        <!-- Pr PrVideo_sec -->
        <section class="PrVideo_sec">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>RE-Veal <span>Videos</span></h2>
                        <div class="video_frames yt_videos">
                            <div class="col-sm-3">
                                <div class="videoWrp">
                                    <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/kM5Fz9MkR6g?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <%--<h4>Hafele Digital Locks – User guide – RE-Veal</h4>--%>
                                    <p>User guide</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="videoWrp">
                                    <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/96SFjwBRXzs?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <%--<h4>Hafele Digital Locks – Installation video – RE-Veal</h4>--%>
                                    <p>Installation video</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="videoWrp">
                                    <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/54oJkW1qwCM?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <%--<h4>Hafele Digital Locks – Quick Installation – RE-Veal</h4>--%>
                                    <p>Quick Installation video</p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="videoWrp">
                                    <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/SZS6CfXjn6k?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <%--<h4>Hafele Digital Locks – Manual – RE-Veal</h4>--%>
                                    <p>Manual</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Pr PrVideo_sec -->
        <!--Start of the explore range-->
        <section class="exploreSec">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Explore <span>range</span></h2>
                        <div id="slider3D" class="owl-carousel owl-theme">
                            
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-veal.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImgsl/pr_1.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-al.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-AL</h5>
                                        <p>Because you need a better security solution…</p>
                                    </div>
                                    <img src="img/prImgsl/re-ai.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-tro.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-TRO</h5>
                                        <p>Because your office cabins also deserve the same digital security as your homes</p>
                                    </div>
                                    <img src="img/prImgsl/re-tro.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-design.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-DESIGN</h5>
                                        <p>Because your main doors deserve the best of both – top notch security and elegant design </p>
                                    </div>
                                    <img src="img/prImgsl/re-design.png" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End of the explore range-->



    </div>
    <!-- // content sec -->

    <script>
        $(document).ready(function () {
            //$('.viewall-btnwrap .viewallbtn').click(function () {
            //    $('.navigation-listing li').removeClass('active');
            //    $('.navigation-listing .productsdesktop').addClass('active');

            //});
        });
    </script>
</asp:Content>

