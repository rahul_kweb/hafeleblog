﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="re-place.aspx.cs" Inherits="re_place" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Hafele</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="img/nmims.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/style.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- content sec -->
    <div class="content_wrapper">

        <!-- banner sec -->
        <section class="bannerForm_Sec">
            <div class="">
                <div class="mobileLogo">
                <a href="index.aspx">    <img src="img/logo.png" alt=""></a>
                </div>
                <div class="bannerIn">
                    <div id="bannerBig" class="owl-carousel owl-theme">
                        <div class="item">
                            <img src="img/banner_place.jpg" alt="">
                        </div>
                    </div>
                    <div class="banner_logo">
                        <img src="img/logo_1.png" alt=""></div>
                </div>
            </div>

        </section>
        <!-- // banner sec -->
        <!-- Pr Detail sec -->
        <section class="PrDetail_sec">
            <div class="container">

                <div class="row clear">
                    <div class="col-lg-5 col-sm-5">
                        <div class="prImg">
                            <img src="img/prImg/re-place.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-7 col-sm-7">
                        <div class="PrDeas">
                            <h2>RE-Place</h2>
                            <h5>Because your mechanical lock needs an upgrade…</h5>
                            <figcaption>For the Trend-transformers who are steered by novelty and always look for new ways to enhance their lifestyles…</figcaption>
                            <p><span>Form to Function:</span> Häfele’s RE-Place Digital Lock is designed to bring a seamless transition from traditional mechanical locks to sophisticated digital security systems. As the name suggests, this lock will replace the limitations of the traditional lock-and-key with the innumerous possibilities of a tech-savvy digital lock- and literally overnight. With 3 different access modes – Key Pad, RFID and Mechanical Key – Häfele’s RE-Place Digital Lock addresses your principal security needs; and the possibility of combining two access modes neutralizes any chances of a break-in. Its strong and sturdy tri-bolt mortise lock body makes it a robust choice that’s built to last. The smart touch-pad on the external lock body adds the much needed sophistication in design and operation.</p>
                        </div>
                    </div>
                </div>

                <div class="ro clear">
                    <div class="iconwrpSec">
                        <div class="col-lg-12 row">
                            <div class="subHeading">Mnemonic Guide: <span>RE-Place</span></div>
                        </div>

                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">ACCESS MODES</h4>
                                            <ul>
                                                <li>
                                                    <img src="img/icon/access/icon_2.jpg" alt="">
                                                    <p>Key-pad or Password (upto 4 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_3.jpg" alt="">
                                                    <p>RFID (upto 100 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_4.jpg" alt="">
                                                    <p>Mechanical Key (upto 2 keys)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_6.jpg" alt="">
                                                    <p>Remote Control Module (can be purchased separately)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">SMART TECHNOLOGIES: </h4>
                                            <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_1.jpg" alt="">
                                                        <p>Smart Password </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_8.jpg" alt="">
                                                        <p>Smart Video Phone</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_3.jpg" alt="">
                                                        <p>Smart Etiquettes </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_4.jpg" alt="">
                                                        <p>Smart Security</p>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_5.jpg" alt="">
                                                        <p>Smart Freeze </p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">LOCKING MODES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/locking/icon_3.jpg" alt="">
                                                    <p>Auto Locking </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_2.jpg" alt="">
                                                    <p>Manual Locking</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_1.jpg" alt="">
                                                    <p>Privacy Locking </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">OPERATIONAL FEATURES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/oprational/icon_1.jpg" alt="">
                                                    <p>Panic Exit </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/oprational/icon_3.jpg" alt="">
                                                    <p>Non-handed Operation</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">USER ACCESS RIGHTS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/userAccess/icon_3.jpg" alt="">
                                                    <p>User</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_4.jpg" alt="">
                                                    <p>Admin</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">ALARMS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/alarms/icon_1.jpg" alt="">
                                                    <p>Low Battery </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_4.jpg" alt="">
                                                    <p>Door not Locked</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_2.jpg" alt="">
                                                    <p>Break-in</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_3.jpg" alt="">
                                                    <p>High Temperature</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Battery:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_1.jpg" alt="">
                                                    <p>AA (3,000 cycles) </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/battery/icon_2.jpg" alt="">
                                                    <p>Jump start (9V)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">DOOR THICKNESS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_1.jpg" alt="">
                                                    <p>40-50mm</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_2.jpg" alt="">
                                                    <p>50-110mm* (special order)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
        </section>
        <!-- // Pr Detail sec -->
         <!-- Pr PrVideo_sec -->
			<section class="PrVideo_sec">
				<div class="container-fluid">
				    <div class="row">
                        <div class="col-lg-12">
                           <h2>RE-Place <span>Videos</span></h2> 
                            <div class="video_frames yt_videos">
                                <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe class="video_groups"  width="100%" height="315" src="https://www.youtube.com/embed/ozojH6PFSEo?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p>User guide</p>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe class="video_groups"  width="100%" height="315" src="https://www.youtube.com/embed/cfZuXPrwXvU?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p>Installation video</p>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe class="video_groups"  width="100%" height="315" src="https://www.youtube.com/embed/njMWB2rQoBU?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p>Quick Installation video</p>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe class="video_groups"  width="100%" height="315" src="https://www.youtube.com/embed/TXS0T_ILVFU?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p>Manual</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</section>
			<!-- // Pr PrVideo_sec -->	
        <!-- explore range -->
         <section class="exploreSec">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Explore <span>range</span></h2>
                        <div id="slider3D" class="owl-carousel owl-theme">
                           <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-size.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Size</h5>
                                        <p>Because your fashionable door needs its ‘right’ match…</p>
                                    </div>
                                    <img src="img/prImgsl/re-size.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-place.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Place</h5>
                                        <p>Because your mechanical lock needs an upgrade…</p>
                                    </div>
                                    <img src="img/prImgsl/re-place.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-bell.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Bell</h5>
                                        <p>Because you need a <span class="strikethrough">99.99</span> “100” percent…</p>
                                    </div>
                                    <img src="img/prImgsl/re-bell.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-vel.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImgsl/pr_1.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-ai.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-AL</h5>
                                        <p>Because you need a better security solution…</p>
                                    </div>
                                    <img src="img/prImgsl/re-ai.png" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // explore range -->
    </div>
    <!-- // content sec -->
</asp:Content>

