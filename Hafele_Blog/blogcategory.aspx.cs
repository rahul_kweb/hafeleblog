﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class blogcategory : System.Web.UI.Page
{
    Utility utility = new Utility();
    string CategoryName = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.RouteData.Values["CategoryName"] != null)
        {
            Session["PreviousPage"] = "CategoryName";

             CategoryName = Page.RouteData.Values["CategoryName"].ToString();

            CategoryName = CategoryName.Replace("-", " ");

            if (!IsPostBack)
            {
                BindBlogsByCategory(CategoryName);
                BindTopTwoCategoryBlogByPopular();
                BindCategoryBlogsTags();
                BindCategoryRelatedBogs();
            }



        }
    }

    public void BindBlogsByCategory(string Category)
    {
        StringBuilder strbuildCat = new StringBuilder();
        StringBuilder strbuild = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("EXEC Proc_Blogs 'BindBlogsByCategoryPage',0,'','"+Category+"'");


        strbuildCat.Append("<h2 class='subHead mb-4'>"+ Category + "</h2>");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strbuild.Append("<div class='col-sm-6 mb-4 line-content' data-aos='fade-up' >");
                strbuild.Append("<div class='blogBox'>");
                strbuild.Append("<a href='/blogdetails/" + dr["slug"].ToString() + "'>");
                strbuild.Append("<div class='blogPic mb-4'><img src='/Content/uploads/Blogs/" + dr["Image"].ToString() + "' class=''></div>");
                strbuild.Append("<div class='blogBrief'>");
                strbuild.Append("<span class='categoryTitle'>" + dr["Category"].ToString() + "</span> | <span class='blogDate'>" + dr["PostDate"].ToString() + "</span>");
                strbuild.Append("</div>");
                strbuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                strbuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                strbuild.Append("</a>");
                strbuild.Append("</div>");
                strbuild.Append("</div>");

            }
        }

        ltrCategory.Text = strbuildCat.ToString();
        ltrBlogCategory.Text = strbuild.ToString();
    }


    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        StringBuilder strbuild = new StringBuilder();
        if (ddlMonth.SelectedValue != "" && ddlYear.SelectedValue != "")
        {
            ltrBlogCategory.Text = "";


            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_Blogs 'BindBlogsCategoryByMonthAndYear',0,'','" + CategoryName + "','','','','','','',"+ddlMonth.SelectedItem.Text+","+ddlYear.SelectedItem.Text+"");

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    strbuild.Append("<div class='col-sm-6 mb-4' data-aos='fade-up' >");
                    strbuild.Append("<div class='blogBox'>");
                    strbuild.Append("<a href='/blogdetails/" + dr["slug"].ToString() + "'>");
                    strbuild.Append("<div class='blogPic mb-4'><img src='/Content/uploads/Blogs/" + dr["Image"].ToString() + "' class=''></div>");
                    strbuild.Append("<div class='blogBrief'>");
                    strbuild.Append("<span class='categoryTitle'>" + dr["Category"].ToString() + "</span> | <span class='blogDate'>" + dr["PostDate"].ToString() + "</span>");
                    strbuild.Append("</div>");
                    strbuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                    strbuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                    strbuild.Append("</a>");
                    strbuild.Append("</div>");
                    strbuild.Append("</div>");

                }
            }
         
            else
            {
                strbuild.Append("<div class='col-sm-6 mb-4' data-aos='fade-up' >");
                strbuild.Append("<div class='blogBox'>");
                strbuild.Append("<h1>No Data Found.</h1>");
                strbuild.Append("</div>");
                strbuild.Append("</div>");
            }
            ltrBlogCategory.Text = strbuild.ToString();

        }

        else if (ddlMonth.SelectedValue != "" && ddlYear.SelectedValue == "")
        {
            ltrBlogCategory.Text = "";


            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_Blogs 'BindBlogsCategoryByMonth',0,'','" + CategoryName + "','','','','','',''," + ddlMonth.SelectedItem.Text + "");

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    strbuild.Append("<div class='col-sm-6 mb-4' data-aos='fade-up' >");
                    strbuild.Append("<div class='blogBox'>");
                    strbuild.Append("<a href='/blogdetails/" + dr["slug"].ToString() + "'>");
                    strbuild.Append("<div class='blogPic mb-4'><img src='/Content/uploads/Blogs/" + dr["Image"].ToString() + "' class=''></div>");
                    strbuild.Append("<div class='blogBrief'>");
                    strbuild.Append("<span class='categoryTitle'>" + dr["Category"].ToString() + "</span> | <span class='blogDate'>" + dr["PostDate"].ToString() + "</span>");
                    strbuild.Append("</div>");
                    strbuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                    strbuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                    strbuild.Append("</a>");
                    strbuild.Append("</div>");
                    strbuild.Append("</div>");

                }
            }

            else
            {
                strbuild.Append("<div class='col-sm-6 mb-4' data-aos='fade-up' >");
                strbuild.Append("<div class='blogBox'>");
                strbuild.Append("<h1>No Data Found.</h1>");
                strbuild.Append("</div>");
                strbuild.Append("</div>");
            }
            ltrBlogCategory.Text = strbuild.ToString();
        }
        else
        {
            strbuild.Append("<div class='col-sm-6 mb-4' data-aos='fade-up' >");
            strbuild.Append("<div class='blogBox'>");
            strbuild.Append("<h1>No Data Found.</h1>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");
        }
        ltrBlogCategory.Text = strbuild.ToString();

    }

    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        StringBuilder strbuild = new StringBuilder();
        if (ddlMonth.SelectedValue != "" && ddlYear.SelectedValue != "")
        {
            ltrBlogCategory.Text = "";


            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_Blogs 'BindBlogsCategoryByMonthAndYear',0,'','" + CategoryName + "','','','','','',''," + ddlMonth.SelectedItem.Text + "," + ddlYear.SelectedItem.Text + "");

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    strbuild.Append("<div class='col-sm-6 mb-4' data-aos='fade-up' >");
                    strbuild.Append("<div class='blogBox'>");
                    strbuild.Append("<a href='/blogdetails/" + dr["slug"].ToString() + "'>");
                    strbuild.Append("<div class='blogPic mb-4'><img src='/Content/uploads/Blogs/" + dr["Image"].ToString() + "' class=''></div>");
                    strbuild.Append("<div class='blogBrief'>");
                    strbuild.Append("<span class='categoryTitle'>" + dr["Category"].ToString() + "</span> | <span class='blogDate'>" + dr["PostDate"].ToString() + "</span>");
                    strbuild.Append("</div>");
                    strbuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                    strbuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                    strbuild.Append("</a>");
                    strbuild.Append("</div>");
                    strbuild.Append("</div>");

                }
            }

            else
            {
                strbuild.Append("<div class='col-sm-6 mb-4' data-aos='fade-up' >");
                strbuild.Append("<div class='blogBox'>");
                strbuild.Append("<h1>No Data Found.</h1>");
                strbuild.Append("</div>");
                strbuild.Append("</div>");
            }
            ltrBlogCategory.Text = strbuild.ToString();

        }

        else if (ddlMonth.SelectedValue == "" && ddlYear.SelectedValue != "")
        {
            ltrBlogCategory.Text = "";


            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_Blogs 'BindBlogsCategoryByYear',0,'','" + CategoryName + "','','','','','','','',"+ ddlYear.SelectedValue + "");

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    strbuild.Append("<div class='col-sm-6 mb-4' data-aos='fade-up' >");
                    strbuild.Append("<div class='blogBox'>");
                    strbuild.Append("<a href='/blogdetails/" + dr["slug"].ToString() + "'>");
                    strbuild.Append("<div class='blogPic mb-4'><img src='/Content/uploads/Blogs/" + dr["Image"].ToString() + "' class=''></div>");
                    strbuild.Append("<div class='blogBrief'>");
                    strbuild.Append("<span class='categoryTitle'>" + dr["Category"].ToString() + "</span> | <span class='blogDate'>" + dr["PostDate"].ToString() + "</span>");
                    strbuild.Append("</div>");
                    strbuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                    strbuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                    strbuild.Append("</a>");
                    strbuild.Append("</div>");
                    strbuild.Append("</div>");

                }
            }

            else
            {
                strbuild.Append("<div class='col-sm-6 mb-4' data-aos='fade-up' >");
                strbuild.Append("<div class='blogBox'>");
                strbuild.Append("<h1>No Data Found.</h1>");
                strbuild.Append("</div>");
                strbuild.Append("</div>");
            }
            ltrBlogCategory.Text = strbuild.ToString();
        }
        else
        {
            strbuild.Append("<div class='col-sm-6 mb-4' data-aos='fade-up' >");
            strbuild.Append("<div class='blogBox'>");
            strbuild.Append("<h1>No Data Found.</h1>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");
        }
        ltrBlogCategory.Text = strbuild.ToString();
    }


    public string BindCategoryRelatedBogs()
    {

        StringBuilder strbuild = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("EXEC Proc_Blogs 'BindCategoryRelatedPostByCategory',0,'','" + CategoryName + "'");

        int count = 0;
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {

                if (count < 4)
                {
                    strbuild.Append("<li data-aos='fade-up' data-aos-delay='200'>");
                    strbuild.Append("<a href='/blogdetails/" + dr["slug"].ToString() + "'>");
                    strbuild.Append("<div class='blogBrief'>");
                    strbuild.Append("<span class='categoryTitle'>" + dr["Category"].ToString() + "</span>  |  <span class='blogDate'>" + dr["PostDate"].ToString() + "</span>");
                    strbuild.Append("</div>");
                    strbuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                    strbuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                    strbuild.Append("</a>");
                    strbuild.Append("</li>");
                    count++;
                }

            }
        }


        ltrCategoryRelatedBogs.Text = strbuild.ToString();

        return ltrCategoryRelatedBogs.Text;
    }


    public string BindTopTwoCategoryBlogByPopular()
    {

        StringBuilder strbuild = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("EXEC Proc_Blogs 'BindTopTwoCategoryBlogByPopular',0,'','"+ CategoryName + "'");


        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {

                strbuild.Append("<div class='col-md-6 col-lg-12 mb-4' data-aos='fade-up'>");
                strbuild.Append("<div class='blogBox dashedBorder'>");
                strbuild.Append("<a href='/blogdetails/" + dr["slug"].ToString() + "'>");
                strbuild.Append("<div class='blogPic mb-4'><img src='/Content/uploads/Blogs/" + dr["Image"].ToString() + "' class=''></div>");
                strbuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                strbuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                strbuild.Append("</a>");
                strbuild.Append("</div>");
                strbuild.Append("</div>");

            }
        }


        ltrCategoryBlogByPopular.Text = strbuild.ToString();

        return ltrCategoryBlogByPopular.Text;
    }

    public string BindCategoryBlogsTags()
    {

        StringBuilder strbuild = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("EXEC Proc_Blogs 'BindCategoryBlogsTags',0,'','" + CategoryName + "'");


        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {

                foreach (string Tags in dr["Tags"].ToString().Split(','))
                {
                    if (Tags != "")
                    {
                        strbuild.Append("<div class='col'><a href='/blogdetails/" + dr["slug"].ToString() + "' class='tags'>"+ Tags + "</a></div>");
                    }


                }
        


            }
        }

        ltrCateegoryTags.Text = strbuild.ToString();

        return ltrCateegoryTags.Text;
    }

}