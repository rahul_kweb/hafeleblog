﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="video.aspx.cs" Inherits="video" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Video of Hafele Digital Lock | RFID Lock | Fingerprint Door Lock</title>
    <meta name="description" content="Know more about Hafele's smart range of digital locks through a video tour. Right from the user guide to installation manual, we have you covered.">

    <link rel="canonical" href="https://digital-locks.hafeleindia.co.in/video" />

    <meta property="og:url" content="https://digital-locks.hafeleindia.co.in/video" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Video of Hafele Digital Lock | RFID Lock | Fingerprint Door Lock" />
    <meta property="og:description" content="Know more about Hafele's smart range of digital locks through a video tour. Right from the user guide to installation manual, we have you covered." />
    <meta property="og:image" content="https://digital-locks.hafeleindia.co.in/img/logo.png" />

    <meta name="twitter:card" content="summary" />
    <meta property="og:url" content="https://digital-locks.hafeleindia.co.in/video" />
    <meta property="og:title" content="Video of Hafele Digital Lock | RFID Lock | Fingerprint Door Lock" />
    <meta property="og:description" content="Know more about Hafele's smart range of digital locks through a video tour. Right from the user guide to installation manual, we have you covered." />
    <meta property="og:image" content="https://digital-locks.hafeleindia.co.in/img/logo.png" />


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<style>
		a.fancyLink {position: relative;display: block;}
		img.img-responsive.playButton{ position:absolute; left:50%; top:50%; margin-left:-25px; margin-top:-13px;}
	</style>
    <h1 class="myh1">Videos for Hafele Digital Door Lock</h1>

    <!--VIDEO CONTAINER-->
    <div class="video-container">
        <div class="container videocontainer-innerwrap">
            <div class="row">
                <div class="col-md-12">
                    <div class="video-body">

                        <!--VIDEO HEADER-->
                        <div class="video-header">
                            <h4 class="heading">Watch <span>videos</span></h4>
                            <%--<p class="info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>--%>
                        </div>
                        <!--video-header-->

                        <div class="video-tab-box">

                            <div class="video-tab-link">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#re-veal">RE-VEAL</a></li>
                                    <li><a data-toggle="tab" href="#re-al">RE-AL</a></li>
                                </ul>
                            </div>

                            <div class="tab-content video-box">
								<div id="re-veal" class="tab-pane fade in active">
									<div class="col-md-12 no-padding">
										<div class="row">
											<div class="col-md-3 col-xs-6 col-sm-6">
												<div class="video-card">
													<div class="video-frame">
														<!-- <iframe width="100%" height="290" src="https://www.youtube.com/embed/kM5Fz9MkR6g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
														<a data-fancybox="" class="fancyLink" href="https://www.youtube.com/watch?v=kM5Fz9MkR6g">
															<img class="img-responsive" src="https://img.youtube.com/vi/kM5Fz9MkR6g/hqdefault.jpg" />
															<img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
														</a>
													</div>
													<div class="video-details">
														<%--<h4>Hafele Digital Locks – User guide – RE-VEAL</h4>--%>
														<p>User guide</p>
													</div>
												</div>
												<!--video-card-->
											</div>
											<!--col-md-4-->
											<div class="col-md-3 col-xs-6 col-sm-6">
												<div class="video-card">
													<div class="video-frame">
														<!-- <iframe width="100%" height="290" src="https://www.youtube.com/embed/96SFjwBRXzs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
														<a data-fancybox="" class="fancyLink" href="https://www.youtube.com/watch?v=96SFjwBRXzs">
															<img class="img-responsive" src="https://img.youtube.com/vi/96SFjwBRXzs/hqdefault.jpg" />
															<img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
														</a>
													</div>
													<div class="video-details">
														<%--<h4>Hafele Digital Locks – Installation video – RE-VEAL</h4>--%>
														<p>Installation video</p>
													</div>
												</div>
												<!--video-card-->
											</div>
											<div class="col-md-3 col-xs-6 col-sm-6">
												<div class="video-card">
													<div class="video-frame">
														<!-- <iframe width="100%" height="290" src="https://www.youtube.com/embed/54oJkW1qwCM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
														<a data-fancybox="" class="fancyLink" href="https://www.youtube.com/watch?v=54oJkW1qwCM">
															<img class="img-responsive" src="https://img.youtube.com/vi/54oJkW1qwCM/hqdefault.jpg" />
															<img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
														</a>
													</div>
													<div class="video-details">
														<%--<h4>Hafele Digital Locks – Quick Installation video – RE-VEAL</h4>--%>
														<p>Quick Installation video</p>
													</div>
												</div>
												<!--video-card-->
											</div>
											<div class="col-md-3 col-xs-6 col-sm-6">
												<div class="video-card">
													<div class="video-frame">
														<!-- <iframe width="100%" height="290" src="https://www.youtube.com/embed/SZS6CfXjn6k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
														<a data-fancybox="" class="fancyLink" href="https://www.youtube.com/watch?v=SZS6CfXjn6k">
															<img class="img-responsive" src="https://img.youtube.com/vi/SZS6CfXjn6k/hqdefault.jpg" />
															<img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
														</a>
													</div>
													<div class="video-details">
														<%--<h4>Hafele Digital Locks – Manual – RE-VEAL</h4>--%>
														<p>Manual</p>
													</div>
												</div>
												<!--video-card-->
											</div>
											<!--col-md-4-->
										</div>
									</div>
									<!--col-md-12-->	
								</div>
								<div id="re-al" class="tab-pane fade">
									<div class="col-md-12 no-padding">
										<div class="row">
											<div class="col-md-3 col-xs-6 col-sm-6">
												<div class="video-card">
													<div class="video-frame">
														<!-- <iframe width="100%" height="290" src="https://www.youtube.com/embed/c3_HNfRBqFo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
														<a data-fancybox="" class="fancyLink" href="https://www.youtube.com/watch?v=c3_HNfRBqFo">
															<img class="img-responsive" src="https://img.youtube.com/vi/c3_HNfRBqFo/hqdefault.jpg" />
															<img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
														</a>
													</div>
													<div class="video-details">
														<%--<h4>Hafele Digital Locks – User guide – RE-AL</h4>--%>
														<p>User guide</p>
													</div>
												</div>
												<!--video-card-->
											</div>
											<!--col-md-4-->
											<div class="col-md-3 col-xs-6 col-sm-6">
												<div class="video-card">
													<div class="video-frame">
														<!-- <iframe width="100%" height="290" src="https://www.youtube.com/embed/ZRevgQnak_Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
														<a data-fancybox="" class="fancyLink" href="https://www.youtube.com/watch?v=ZRevgQnak_Y">
															<img class="img-responsive" src="https://img.youtube.com/vi/ZRevgQnak_Y/hqdefault.jpg" />
															<img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
														</a>
													</div>
													<div class="video-details">
														<p>Installation video</p>
													</div>
													<%--<h4>Hafele Digital Locks – Installation video – RE-AL</h4>--%>
												</div>
												<!--video-card-->
											</div>
											<div class="col-md-3 col-xs-6 col-sm-6">
												<div class="video-card">
													<div class="video-frame">
														<!-- <iframe width="100%" height="290" src="https://www.youtube.com/embed/yfsvlKhxbwU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
														<a data-fancybox="" class="fancyLink" href="https://www.youtube.com/watch?v=yfsvlKhxbwU">
															<img class="img-responsive" src="https://img.youtube.com/vi/yfsvlKhxbwU/hqdefault.jpg" />
															<img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
														</a>
													</div>
													<div class="video-details">
														<%--<h4>Hafele Digital Locks – Quick Installation video – RE-AL</h4>--%>
														<p>Quick Installation video</p>
													</div>
												</div>
												<!--video-card-->
											</div>
											<div class="col-md-3 col-xs-6 col-sm-6">
												<div class="video-card">
													<div class="video-frame">
														<!-- <iframe width="100%" height="290" src="https://www.youtube.com/embed/xD9rAiZw-8I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
														<a data-fancybox="" class="fancyLink" href="https://www.youtube.com/watch?v=xD9rAiZw-8I">
															<img class="img-responsive" src="https://img.youtube.com/vi/xD9rAiZw-8I/hqdefault.jpg" />
															<img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
														</a>
													</div>
													<div class="video-details">
														<%--<h4>Hafele Digital Locks – Manual – RE-AL</h4>--%>
														<p>Manual</p>
													</div>
												</div>
												<!--video-card-->
											</div>
											<!--col-md-4-->
										</div>
									</div>
									<!--col-md-12-->	
								</div>
							</div>
                        </div>
                        <!--video-tab-box-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--video-container-->



</asp:Content>

