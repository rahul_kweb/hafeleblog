﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="blogcategory.aspx.cs" Inherits="blogcategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta charset="utf-8">
    <title>Hafele - Blogs</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/fav-icon.png" type="image/x-icon">
    <link rel="stylesheet" href="/Content/Blogs/css/bootstrap.min.css">
    <link rel="stylesheet" href="/Content/Blogs/css/mystyle.css">
    <link rel="stylesheet" type="text/css" href="/Content/Blogs/css/slick.css">
    <link rel="stylesheet" type="text/css" href="/Content/Blogs/css/aos.css">
    <link rel="stylesheet" href="/Content/Blogs/css/font-awesome.min.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content_wrapper">
        <section class="mainContent">


            <div class="container mb-5 mt-5  px-lg-0 ">
                <div class="row  pb-5">
                    <div class="col-lg-8">
                        <asp:Literal ID="ltrCategory" runat="server"></asp:Literal>
                    <%--    <h2 class=" subHead mb-4">Category 01</h2>--%>

                        <div class="row mb-4 justify-content-end">
                            <div class="col-auto sleactCategory ">
                                <div class="select">
                                    <%--<select name="slct" id="slct">
                        <option selected disabled>Month</option>
                        <option value="01">January</option>
                        <option value="02">February</option>
                        <option value="03">March</option>
                        <option value="04">April</option>
                        <option value="05">May</option>
                        <option value="06">June</option>
                        <option value="07">July</option>
                        <option value="08">August</option>
                        <option value="09">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                      </select>--%>
                                    <asp:DropDownList ID="ddlMonth" runat="server" AutoPostBack="True"  OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged">
                                        <asp:ListItem Value="">Month</asp:ListItem>
                                        <asp:ListItem>January </asp:ListItem>
                                        <asp:ListItem>February</asp:ListItem>
                                        <asp:ListItem>March</asp:ListItem>
                                        <asp:ListItem>April</asp:ListItem>
                                        <asp:ListItem>May</asp:ListItem>
                                        <asp:ListItem>June</asp:ListItem>
                                         <asp:ListItem>August</asp:ListItem>
                                         <asp:ListItem>September</asp:ListItem>
                                         <asp:ListItem>October</asp:ListItem>
                                         <asp:ListItem>November</asp:ListItem>
                                         <asp:ListItem>December</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-auto sleactCategory">
                                <div class="select">
                                <%--    <select name="slct" id="slct">
                                        <option selected disabled>Year</option>
                                        <option value="01">2021</option>
                                        <option value="02">2020</option>
                                        <option value="03">2019</option>
                                        <option value="04">2018</option>
                                        <option value="05">2017</option>
                                        <option value="06">2016</option>
                                        <option value="07">2015</option>
                                    </select>--%>

                                      <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                        <asp:ListItem Value="">Year</asp:ListItem>
                                        <asp:ListItem>2021</asp:ListItem>
                                        <asp:ListItem>2020</asp:ListItem>
                                        <asp:ListItem>2019</asp:ListItem>
                                        <asp:ListItem>2018</asp:ListItem>
                                        <asp:ListItem>2017</asp:ListItem>
                                        <asp:ListItem>2016</asp:ListItem>
                                        <asp:ListItem>2015</asp:ListItem>                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>


                        <div class="row categoryWrap">
                            <asp:Literal ID="ltrBlogCategory" runat="server"></asp:Literal>
                            <%--<div class="col-sm-6 mb-4" data-aos="fade-up" >
            		<div class="blogBox">
                        <a href="#">
                        <div class="blogPic mb-4"><img src="/Content/Blogs/images/post/1.jpg" class=""></div>
                       	
                        <div class="blogBrief">
                            <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                        </div>
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
                        <p>Consectetur adipiscing elitseding</p>
                        </a>
                    </div>
        		</div>
                
                <div class="col-sm-6 mb-4" data-aos="fade-up" >
            		<div class="blogBox">
                        <a href="#">
                        <div class="blogPic mb-4"><img src="/Content/Blogs/images/post/2.jpg" class=""></div>
                       	
                        <div class="blogBrief">
                            <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                        </div>
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
                        <p>Consectetur adipiscing elitseding</p>
                        </a>
                    </div>
        		</div>
                
                <div class="col-sm-6 mb-4" data-aos="fade-up" >
            		<div class="blogBox">
                        <a href="#">
                        <div class="blogPic mb-4"><img src="/Content/Blogs/images/post/3.jpg" class=""></div>
                       	
                        <div class="blogBrief">
                            <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                        </div>
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
                        <p>Consectetur adipiscing elitseding</p>
                        </a>
                    </div>
        		</div>
                
                <div class="col-sm-6 mb-4" data-aos="fade-up" >
            		<div class="blogBox">
                        <a href="#">
                        <div class="blogPic mb-4"><img src="/Content/Blogs/images/post/4.jpg" class=""></div>
                       	
                        <div class="blogBrief">
                            <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                        </div>
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
                        <p>Consectetur adipiscing elitseding</p>
                        </a>
                    </div>
        		</div>
                
                <div class="col-sm-6 mb-4" data-aos="fade-up" >
            		<div class="blogBox">
                        <a href="#">
                        <div class="blogPic mb-4"><img src="/Content/Blogs/images/post/1.jpg" class=""></div>
                       	
                        <div class="blogBrief">
                            <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                        </div>
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
                        <p>Consectetur adipiscing elitseding</p>
                        </a>
                    </div>
        		</div>
                
                <div class="col-sm-6 mb-4" data-aos="fade-up" >
            		<div class="blogBox">
                        <a href="#">
                        <div class="blogPic mb-4"><img src="/Content/Blogs/images/post/2.jpg" class=""></div>
                       	
                        <div class="blogBrief">
                            <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                        </div>
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
                        <p>Consectetur adipiscing elitseding</p>
                        </a>
                    </div>
        		</div>
                            --%>
                        </div>

                        <div class="d-inline-block pagination text-center mb-5">

                            <ul id="pagin"></ul>
                    <%-- <a href="#" class="prev"></a>
                            <a href="#" class="active">01</a>
                            <a href="#">02</a>
                            <a href="#">03</a>
                            <a href="#" class="next"></a>
                            --%>
                        </div>

                    </div>

                    <div class="col-lg-4">
                        <h1 class="title mb-4" data-aos="zoom-in">Related <span>post</span></h1>

                        <ul class="postList mb-5">
                            <asp:Literal ID="ltrCategoryRelatedBogs" runat="server"></asp:Literal>
                            <%--<li data-aos="fade-up" data-aos-delay="200">
                                <a href="#">
                                    <div class="blogBrief">
                                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                    </div>
                                    <h1>Lorem ipsum dummy Hafele Blog </h1>
                                    <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </li>

                            <li data-aos="fade-up" data-aos-delay="300">
                                <a href="#">
                                    <div class="blogBrief">
                                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                    </div>
                                    <h1>Lorem ipsum dummy Hafele Blog </h1>
                                    <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </li>

                            <li data-aos="fade-up" data-aos-delay="400">
                                <a href="#">
                                    <div class="blogBrief">
                                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                    </div>
                                    <h1>Lorem ipsum dummy Hafele Blog </h1>
                                    <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </li>
                            <li data-aos="fade-up" data-aos-delay="400">
                                <a href="#">
                                    <div class="blogBrief">
                                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                    </div>
                                    <h1>Lorem ipsum dummy Hafele Blog </h1>
                                    <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </li>--%>
                        </ul>

                        <h1 class="title mb-4" data-aos="zoom-in">Populer <span>post</span></h1>

                        <div class="row mb-3">
                            <asp:Literal ID="ltrCategoryBlogByPopular" runat="server"></asp:Literal>
                   <%--         <div class="col-md-6 col-lg-12 mb-4 " data-aos="fade-up">
                                <div class="blogBox dashedBorder ">
                                    <a href="#">
                                        <div class="blogPic mb-4">
                                            <img src="/Content/Blogs/images/post/2.jpg" class=""></div>

                                        <h1>Lorem ipsum dummy Hafele Blog </h1>
                                        <p>Consectetur adipiscing elitseding</p>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-12 mb-4 " data-aos="fade-up">
                                <div class="blogBox dashedBorder ">
                                    <a href="#">
                                        <div class="blogPic mb-4">
                                            <img src="/Content/Blogs/images/post/2.jpg" class=""></div>

                                        <h1>Lorem ipsum dummy Hafele Blog </h1>
                                        <p>Consectetur adipiscing elitseding</p>
                                    </a>
                                </div>
                            </div>--%>
                        </div>


                        <h1 class="title mb-4" data-aos="zoom-in">#Tags <span>posts</span></h1>
                        <div class="row row-cols-2 row-cols-sm-4 row-cols-lg-3">
                            <asp:Literal ID="ltrCateegoryTags" runat="server"></asp:Literal>
                          <%--  <div class="col"><a href="#" class="tags">Category 01</a></div>
                            <div class="col"><a href="#" class="tags">Category 02</a></div>
                            <div class="col"><a href="#" class="tags">Category 03</a></div>
                            <div class="col"><a href="#" class="tags">Category 04</a></div>
                            <div class="col"><a href="#" class="tags">Category 05</a></div>
                            <div class="col"><a href="#" class="tags">Category 06</a></div>
                            <div class="col"><a href="#" class="tags">Category 07</a></div>
                            <div class="col"><a href="#" class="tags">Category 08</a></div>--%>
                        </div>

                    </div>
                </div>
            </div>



        </section>
    </div>
    <style>
        .current {
  color: green;
}

#pagin li {
  display: inline-block;
}

/*.prev {
  cursor: pointer;
}

.next {
  cursor: pointer;
}*/

.last{
  cursor:pointer;
  margin-left:5px;
}

.first{
  cursor:pointer;
  margin-right:5px;
}
    </style>

    <script src="/Content/Blogs/js/jquery-3.5.1.min.js"></script>
    <script src="/Content/Blogs/js/bootstrap.js"></script>
    <script src="/Content/Blogs/js/aos.js"></script>
    <script src="/Content/Blogs/js/common.js"></script>
    <script src="/Content/Blogs/js/slick.min.js"></script>

    <script>
        //Pagination
        pageSize = 4;
        incremSlide = 1;
        startPage = 0;
        numberPage = 0;

        var pageCount = $(".line-content").length / pageSize;
        var totalSlidepPage = Math.floor(pageCount / incremSlide);

        for (var i = 0 ; i < pageCount; i++) {
            $("#pagin").append('<li><a href="#">' + (i + 1) + '</a></li> ');
            if (i > pageSize) {
                $("#pagin li").eq(i).hide();
            }
        }

        //var prev = $("<li/>").addClass("prev").html("Prev").click(function () {
        //    startPage -= 5;
        //    incremSlide -= 5;
        //    numberPage--;
        //    slide();
        //});

        //prev.hide();

        //var next = $("<li/>").addClass("next").html("Next").click(function () {
        //    startPage += 5;
        //    incremSlide += 5;
        //    numberPage++;
        //    slide();
        //});

        //$("#pagin").prepend(prev).append(next);

        $("#pagin li").first().find("a").addClass("current");

        slide = function (sens) {
            $("#pagin li").hide();

            for (t = startPage; t < incremSlide; t++) {
                $("#pagin li").eq(t + 1).show();
            }
            //if (startPage == 0) {
            //    next.show();
            //    prev.hide();
            //} else if (numberPage == totalSlidepPage) {
            //    next.hide();
            //    prev.show();
            //} else {
            //    next.show();
            //    prev.show();
            //}


        }

        showPage = function (page) {
            $(".line-content").hide();
            $(".line-content").each(function (n) {
                if (n >= pageSize * (page - 1) && n < pageSize * page)
                    $(this).show();
            });
        }

        showPage(1);
        $("#pagin li a").eq(0).addClass("current");

        $("#pagin li a").click(function () {
            $("#pagin li a").removeClass("current");
            $(this).addClass("current");
            showPage(parseInt($(this).text()));
        });
    </script>



</asp:Content>

