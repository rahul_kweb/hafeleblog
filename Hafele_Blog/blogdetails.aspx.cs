﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class blogdetails : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.RouteData.Values["PageName"] != null)
        {
            Session["PreviousPage"] = "PreviousPage";

            string PageName = Page.RouteData.Values["PageName"].ToString();
            if (!IsPostBack)
            {
                Bindblogdetails(PageName);
                BindPopularBlog();
                BindBottomBlogs();
                BindTagsByTitle(PageName);
                BindComment();
            }
         


        }
      
    }

    public void Bindblogdetails(string PageName)
    {
        try
        {
            StringBuilder strbuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_Blogs 'BindBlogDetails',0,'','','','','','','','"+ PageName + "'");


            if (dt.Rows.Count > 0)
            {
                foreach(DataRow dr in dt.Rows)
                { 
                strbuild.Append("<div class='blogPic mb-5' data-aos='fade-up'><img src='/Content/uploads/Blogs/" + dr["Image"].ToString() + "' class='d-block w-100'></div>");
                strbuild.Append("<div class='blogDate'>"+ dr["PostDate"].ToString() + "</div>");
                strbuild.Append("<h1 class='title mb-3'>"+ dr["Title"].ToString() + "</h1>");
                strbuild.Append("<div class='blogTags mb-4'>");
                strbuild.Append("<img src='/Content/Blogs/images/tag.png'>");
                strbuild.Append("<a href='#'>" + dr["Tags"].ToString() + "</a>");
                    DataTable dtComment = new DataTable();
                    dtComment = utility.Display("EXEC Proc_BlogsComments 'BindComment',0,'" + dr["Id"].ToString() + "'");
                    strbuild.Append("<span>"+ dtComment.Rows.Count + " Comments</span>");
                strbuild.Append("<div class='clearfix'></div>");
                strbuild.Append("</div>");
                strbuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                strbuild.Append("<div class='row'>");
                strbuild.Append("<div class='col-md-8'>");
                strbuild.Append("<div class='row row-cols-2 row-cols-sm-4 row-cols-lg-4'>");

                    foreach (string category in dr["Category"].ToString().Split(','))
                    {
                        if (category != "")
                        {

                           string categoryName = category.Replace(" ", "-");
                            strbuild.Append("<div class='col'><a href='/blogcategory/" + categoryName + "' class='tags'>" + category + "</a></div>");
                        }


                    }
            
                strbuild.Append("</div>");
                strbuild.Append("</div>");
                strbuild.Append("<div class='col-md-4'>");
                strbuild.Append("<div class='d-inline-block mr-3'>Share Now</div>");
                strbuild.Append("<div class='d-inline-block shareMedia'>");
                strbuild.Append("<a href='http://www.facebook.com/sharer/sharer.php?u=http://hafele.kwebmakerdigital.com/blogdetails/" + PageName + "' target='_blank'><i class='fa fa-facebook' aria-hidden='true'></i></a>");
                strbuild.Append("<a href='https://twitter.com/share?url=http://hafele.kwebmakerdigital.com/blogdetails/" + PageName + "' target='_blank'><i class='fa fa-twitter' aria-hidden='true'></i></a>");
                //strbuild.Append("<a href='#'><i class='fa fa-instagram' aria-hidden='true'></i></a>");
                strbuild.Append("<a href='whatsapp://send?text=http://hafele.kwebmakerdigital.com/blogdetails/" + PageName + "'><i class='fa fa-whatsapp' aria-hidden='true'></i></a>");
                strbuild.Append("</div>");
                strbuild.Append("</div>");
                strbuild.Append("</div>");

                    BlogDetailsRelatedBlog(dr["Category"].ToString());

                    // Blog Id for Comment
                    hdnBlogId.Value = dr["Id"].ToString();
                }
            }
            ltrdetails.Text = strbuild.ToString();
        }
        catch (Exception Ex)
        {
            this.Title = Ex.Message;
        }
    }

    public void BlogDetailsRelatedBlog(string Blogcategory)
    {
        int count = 0;
        StringBuilder strbuild = new StringBuilder();
        foreach (string category in Blogcategory.ToString().Split(','))
        {
         
            if (category != "")
            {
                if (count < 4)
                {
            
                    DataTable dt = new DataTable();
                dt = utility.Display("EXEC Proc_Blogs 'BindDetailblogRelatedbycategory',0,'','" + category +"'");

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (count < 4)
                        {
                            strbuild.Append("<li data-aos='fade-up' data-aos-delay='200'>");
                            strbuild.Append("<a href='/blogdetails/" + dr["slug"].ToString() + "'>");
                            strbuild.Append("<div class='blogBrief'>");
                            strbuild.Append("<span class='categoryTitle'>"+ category + "</span>  |  <span class='blogDate'>"+dr["PostDate"].ToString()+"</span>");
                            strbuild.Append("</div>");
                            strbuild.Append("<h1>"+dr["Title"].ToString()+"</h1>");
                            strbuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                            strbuild.Append("</a>");
                            strbuild.Append("</li>");
                            count++;
                        }
                  
                    }
                   
                }
             

            }

            }
        }

        ltrDetailsRelatedBlog.Text = strbuild.ToString();

    }

    public string BindPopularBlog()
    {
     
            StringBuilder strbuild = new StringBuilder();

                DataTable dt = new DataTable();
                dt = utility.Display("EXEC Proc_Blogs 'BindThreePopularBlogsForBolgDetails'");


        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {

                strbuild.Append("<div class='col-md-6 col-lg-12 mb-4' data-aos='fade-up'>");
                strbuild.Append("<div class='blogBox dashedBorder'>");
                strbuild.Append("<a href='http://localhost:2994//blogdetails/" + dr["slug"].ToString() + "'>");
                strbuild.Append("<div class='blogPic mb-4'><img src='/Content/uploads/Blogs/" + dr["Image"].ToString() + "' class=''></div>");
                strbuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                strbuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                strbuild.Append("</a>");
                strbuild.Append("</div>");
                strbuild.Append("</div>");

            }
        }


        ltrPopularBlogDetails.Text = strbuild.ToString();

        return ltrPopularBlogDetails.Text;
    }

    public string BindBottomBlogs()
    {

        StringBuilder strbuild = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("EXEC Proc_Blogs 'BindTopFourBlogDetailBottomBlogs'");


        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {

                strbuild.Append("<div>");
                strbuild.Append("<a href='/blogdetails/" + dr["slug"].ToString() + "' class='articleBox d-flex h-100'>");
                strbuild.Append("<div class='align-self-center text-center w-100'>");
                strbuild.Append("<div class='blogBrief'>");
                strbuild.Append("<span class='categoryTitle'>"+dr["Category"].ToString()+ "</span> | <span class='blogDate'>"+dr["PostDate"].ToString()+"</span>");
                strbuild.Append("</div>");
                strbuild.Append("<h1>"+dr["Title"].ToString()+"</h1>");
                strbuild.Append("</div>");
                strbuild.Append("</a>");
                strbuild.Append("</div>");

            }
        }


        ltrBlogDetailBottomBlogs.Text = strbuild.ToString();

        return ltrBlogDetailBottomBlogs.Text;
    }

    public string BindTagsByTitle(string PageName)
    {
        StringBuilder strbuild = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("EXEC Proc_Blogs 'BindTagsByTitle',0,'','','','','','','','" + PageName + "'");



        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                foreach (string Tags in dr["Tags"].ToString().Split(','))
                {
                    if (Tags != "")
                    {
                        //strbuild.Append("<div class='col'><a href='#' class='tags'>"+Tags+"</a></div>");
                        strbuild.Append("<div class='col'><a href='' class='tags'>" + Tags + "</a></div>");
                    }


                }

               
               
            }
        }



        ltrTagsByTitle.Text = strbuild.ToString();

        return ltrTagsByTitle.Text;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_BlogsComments"))
        {

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "add");
            cmd.Parameters.AddWithValue("@BlogId", int.Parse(hdnBlogId.Value.ToString().Trim()));
            cmd.Parameters.AddWithValue("@Name", txtName.Text.ToString().Trim());
            cmd.Parameters.AddWithValue("@Email", txtEmail.Text.ToString().Trim());
            cmd.Parameters.AddWithValue("@Comment", txtComment.Text.ToString().Trim());

            if (utility.Execute(cmd))
            {
                txtComment.Text = string.Empty;
                txtName.Text= string.Empty;
                txtEmail.Text = string.Empty;


                //string PageName = Page.RouteData.Values["PageName"].ToString();
                //Response.Redirect("/blogdetails/" + PageName + "");
                //Display success message.
                //string message = "Your details have been saved successfully.";
                //string script = "window.onload = function(){ alert('";
                //script += message;
                //script += "')};";
                //ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);

                string PageName = Page.RouteData.Values["PageName"].ToString();
                //string script = "window.onload = setTimeout(function(){alert('Your details have been saved successfully.'); window.location = '/blogdetails/" + PageName + "'; }, 5000);";

                string script = "window.onload = function(){alert('Your Comment have been saved successfully.'); window.location = '/blogdetails/" + PageName + "'; };";

                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);







            }


        }
    }

    public void BindComment()
    {
        StringBuilder strbuild = new StringBuilder();

        StringBuilder strbuildCommentCount = new StringBuilder();

        StringBuilder strbuildreply = new StringBuilder();


        DataTable dt = new DataTable();
        dt = utility.Display("EXEC Proc_BlogsComments 'BindComment',0,'"+hdnBlogId.Value+"'");

        strbuildCommentCount.Append("<div class='commentCount'>"+ dt.Rows.Count + " Comments</div>");

        hdnBlogCommentsCount.Value= dt.Rows.Count.ToString();
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {

                strbuild.Append("<li class='row justify-content-center position-relative'>");
                strbuild.Append("<div class='col-5 col-sm-2'>");
                strbuild.Append("<div class='commentPic'><img src = '/Content/Blogs/images/commentPic.jpg' class='w-100 d-block'></div>");
                strbuild.Append("</div>");
                strbuild.Append("<div class='col-sm-10'>");
                strbuild.Append("<div class='comment'>");
                strbuild.Append("<div class='commentBy '>"+dr["Name"].ToString()+"<span>:</span></div>");
                strbuild.Append("<p>" + dr["Comment"].ToString() + "</p>");
                strbuild.Append("<div class='commentDate'>" + dr["Commentdate"].ToString() + "</div>");
                strbuild.Append("</div>");

                DataTable dtReply = new DataTable();
                dtReply = utility.Display("EXEC Proc_CommentReply 'BindReply',0,'" + int.Parse(dr["CommentId"].ToString()) + "'");

                if (dtReply.Rows.Count > 0)
                {
                    foreach (DataRow drReply in dtReply.Rows)
                    {
                        strbuild.Append("<div class='comment  pl-4'>");
                        strbuild.Append("<div class='commentBy redColor'>" + drReply["Name"].ToString() + "<span>:</span></div>");
                        strbuild.Append("<p>" + drReply["Comment"].ToString() + "</p>");
                        strbuild.Append("<div class='commentDate'>" + drReply["Commentdate"].ToString() + "</div>");
                        strbuild.Append("</div>");
                       
                    }
                }
                strbuild.Append("</div>");

                strbuild.Append("<button class='replBtn  d-inline-block' onclick='myFunction()' type='button'>Reply</button>");

            

                strbuild.Append("<div class=' col-12 commentReply'>");
                strbuild.Append("<div class='replybox'>");
                strbuild.Append("<div class='row form-group mb-0'>");
                strbuild.Append("<input type='hidden' class='hiddenReply' id='hdn" + int.Parse(dr["CommentId"].ToString()) + "' name='comment-id' value='" + int.Parse(dr["CommentId"].ToString()) + "'>");
                strbuild.Append("<div class='col-12'><textarea class='form-control rounded-0 mb-3' placeholder='Your Reply' id='txtReply" + int.Parse(dr["CommentId"].ToString()) + "'></textarea></div>");
                //strbuild.Append("<div class='col-12'> <span id='ErrorCommentReply' class='ErrorValidation'>Reply is required.</span></div>");
                strbuild.Append("<div class='col-sm-5'><input type = 'text' class='form-control rounded-0 mb-3' placeholder='Name' id='txtName" + int.Parse(dr["CommentId"].ToString()) + "'></div>");
                strbuild.Append("<div class='col-sm-5'><input type = 'email' class='form-control rounded-0 mb-3' placeholder='Email' id='txtEmail" + int.Parse(dr["CommentId"].ToString()) + "'></div>");
                strbuild.Append("<div class='col-sm-2'><button class='form-control rounded-0' id='btn" + int.Parse(dr["CommentId"].ToString()) + "' onclick=\"validationReply()\" type='button'><i class='fa fa-paper-plane' aria-hidden='true'></i></button></div>");
                //strbuild.Append("<div class='col-sm-6'> <span id='ErrorNameReply' class='ErrorValidation'>Name is required.</span></div>");
                //strbuild.Append("<div class='col-sm-6'> <span id='ErrorEmailReply1' class='ErrorValidation'>Email is required.</span></div>");
                //strbuild.Append("<div class='col-sm-6'> <span id='ErrorEmailReply2' class='ErrorValidation'>Invalid email address.</span></div>");

                strbuild.Append("</div>");
                strbuild.Append("</div>");
                strbuild.Append("</div>");
                strbuild.Append("</li>");
                
               

            }
        }


        ltrCommentCount.Text = strbuildCommentCount.ToString();
        ltrCommentReply.Text = strbuild.ToString();

    }

    [WebMethod]
    public static string AddReply(Replydata obj)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CommentReply"))
        {
            Utility utility = new Utility();

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "add");
            cmd.Parameters.AddWithValue("@CommentId", int.Parse(obj.CommentId.ToString()));
            cmd.Parameters.AddWithValue("@Name", obj.Name.ToString().Trim());
            cmd.Parameters.AddWithValue("@Email", obj.Email.ToString().Trim());
            cmd.Parameters.AddWithValue("@Comment", obj.Reply.ToString().Trim());

            if (utility.Execute(cmd))
            {

                //static string PageName = Page.RouteData.Values["PageName"].ToString();
                //string script = "window.onload = setTimeout(function(){alert('Your details have been saved successfully.'); window.location = '/blogdetails/" + PageName + "'; }, 5000);";

                //string script = "window.onload = function(){alert('Your details have been saved successfully.'); window.location = location.reload();};";

                //Page objpage = new Page();
                //objpage.ClientScript.RegisterStartupScript(objpage.GetType(), "SuccessMessage", script, true);

                //Page objpage = new Page();
                //string PageName = objpage.Page.RouteData.Values["PageName"].ToString();
                //objpage.Response.Redirect("/blogdetails/" + PageName + "");

                return "Your reply have been saved successfully.";

            }
            else
            {
                return "Please try after some time.";
            }


        }

    }
}