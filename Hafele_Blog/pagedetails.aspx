﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="pagedetails.aspx.cs" Inherits="pagedetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Hafele</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="img/nmims.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/style.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- content sec -->
    <div class="content_wrapper">

        <!-- banner sec -->
        <section class="bannerForm_Sec">
            <div class="">
                <div class="mobileLogo">
                    <img src="img/logo.png" alt="">
                </div>
                <div class="bannerIn">
                    <div id="bannerBig" class="owl-carousel owl-theme">
                        <div class="item">
                            <img src="img/banner.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="img/banner.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="img/banner.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="img/banner.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="img/banner.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="img/banner.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- // banner sec -->
        <!-- Pr Detail sec -->
        <section class="PrDetail_sec">
            <div class="container">

                <div class="row clear">
                    <div class="col-lg-5 col-sm-5">
                        <div class="prImg">
                            <img src="img/prImg/img_1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-7 col-sm-7">
                        <div class="PrDeas">
                            <h2>RE-Veal</h2>
                            <h5>Because you need the highest form of security…</h5>
                            <figcaption>For the Judicious-jivers who would go the extra mile to choose the best and most holistic solution available; a profound lot who leave nothing to chance</figcaption>
                            <p>
                                <span>Smile for Security:</span> Häfele’s RE-Veal Digital Lock carefully scrutinizes every individual need that you may have from your home security system and presents itself as the ideal answer for all those needs. This fully-loaded face-recognition lock comes with the highest standards of technology that reads over 170 points on the user’s face – this means that it is highly sensitive to even the flinch of a nerve on your face and will only allow access if you exactly emulate the expression that
was pre-set as your access recognition. With 5 dirent access modes – Face Recognition, Finger Print, Key Pad, RFID and Mechanical Key – Häfele’s RE-Veal Digital Lock is the authority in home security and access planning. For double assurance, you can combine two access modes as per your choice, making it impossible for any unwanted break-ins. And should someone dare try a break-in, our integrated intruder-capture feature can show you an image of the person who made this futile attempt. Häfele’s RE-Veal Digital Lock comes with lithium batteries that are tested to last for 8000 cycles (or approximately one year) providing you with a hassle-free experience. The most singular feature that sets it apart from others is RE-Veal’s night-vision that recognizes a registered face even in pitch darkness. So it’s time to leave your security worries behind, flash your best smile at this miracle machine and let yourself in…
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row clear">
                    <div class="iconwrpSec">
                        <div class="col-lg-12">
                            <div class="subHeading">Mnemonic Guide: <span>RE-Veal</span></div>
                        </div>
                        <div class="col-lg-6 col-sm-6">

                            <div class="iconWrp">
                                <div class="PR_specifcation">
                                    <h4 class="icon_head">ACCESS MODES</h4>
                                    <ul>
                                        <li>
                                            <img src="img/icon/access/icon_1.jpg" alt="">
                                            <p>Silk id Fingerprint (upto 100 unique accesses)</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/access/icon_2.jpg" alt="">
                                            <p>Key-pad or Password (upto 100 unique accesses)</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/access/icon_3.jpg" alt="">
                                            <p>RFID (upto 100 unique accesses)</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/access/icon_4.jpg" alt="">
                                            <p>Mechanical Key (4 user keys,1 construction key)</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/access/icon_5.jpg" alt="">
                                            <p>Face Recognition (upto 100 unique accesses)</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="iconWrp">
                                <div class="PR_specifcation boxIN">
                                    <h4 class="icon_head">LOCKING MODES:</h4>
                                    <ul class="clear">
                                        <li>
                                            <img src="img/icon/locking/icon_1.jpg" alt="">
                                            <p>Privacy Locking</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/locking/icon_2.jpg" alt="">
                                            <p>Manual Locking</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="iconWrp">
                                <div class="PR_specifcation boxIN">
                                    <h4 class="icon_head">USER ACCESS RIGHTS:</h4>
                                    <ul class="clear">
                                        <li>
                                            <img src="img/icon/userAccess/icon_1.jpg" alt="">
                                            <p>Super Admin </p>
                                        </li>
                                        <li>
                                            <img src="img/icon/userAccess/icon_2.jpg" alt="">
                                            <p>Admin</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/userAccess/icon_3.jpg" alt="">
                                            <p>User (upto 100 user)</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/userAccess/icon_4.jpg" alt="">
                                            <p>Guest</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="iconWrp">
                                <div class="PR_specifcation boxIN">
                                    <h4 class="icon_head">Battery:</h4>
                                    <ul class="clear">
                                        <li>
                                            <img src="img/icon/userAccess/icon_1.jpg" alt="">
                                            <p>Lithium (8,000 cycles, onced full charged)</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/userAccess/icon_2.jpg" alt="">
                                            <p>Jump start (9V)</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6 col-sm-6">

                            <div class="iconWrp">
                                <div class="PR_specifcation boxIN">
                                    <h4 class="icon_head">SMART TECHNOLOGIES: </h4>
                                    <ul class="clear">
                                        <li>
                                            <img src="img/icon/smartTech/icon_1.jpg" alt="">
                                            <p>Smart Password </p>
                                        </li>
                                        <li>
                                            <img src="img/icon/smartTech/icon_2.jpg" alt="">
                                            <p>Smart Voice</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/smartTech/icon_3.jpg" alt="">
                                            <p>Smart Etiquettes </p>
                                        </li>
                                        <li>
                                            <img src="img/icon/smartTech/icon_4.jpg" alt="">
                                            <p>Smart Security</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/smartTech/icon_5.jpg" alt="">
                                            <p>Smart Freeze </p>
                                        </li>
                                        <li>
                                            <img src="img/icon/smartTech/icon_6.jpg" alt="">
                                            <p>Smart Night Vision</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/smartTech/icon_7.jpg" alt="">
                                            <p>Smart Intruder (upto 10 photos)</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="iconWrp">
                                <div class="PR_specifcation boxIN">
                                    <h4 class="icon_head">OPERATIONAL FEATURES:</h4>
                                    <ul class="clear">
                                        <li>
                                            <img src="img/icon/smartTech/icon_1.jpg" alt="">
                                            <p>Panic Exit </p>
                                        </li>
                                        <li>
                                            <img src="img/icon/smartTech/icon_2.jpg" alt="">
                                            <p>Audit Trails(Up to 30,000 records can be stored)</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/smartTech/icon_3.jpg" alt="">
                                            <p>Non-handed Operation</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="iconWrp">
                                <div class="PR_specifcation boxIN">
                                    <h4 class="icon_head">ALARMS:</h4>
                                    <ul class="clear">
                                        <li>
                                            <img src="img/icon/alarms/icon_1.jpg" alt="">
                                            <p>Low Battery </p>
                                        </li>
                                        <li>
                                            <img src="img/icon/alarms/icon_2.jpg" alt="">
                                            <p>Break-in</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/alarms/icon_3.jpg" alt="">
                                            <p>High Temperature</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="iconWrp">
                                <div class="PR_specifcation boxIN">
                                    <h4 class="icon_head">DOOR THICKNESS:</h4>
                                    <ul class="clear">
                                        <li>
                                            <img src="img/icon/doorThickness/icon_1.jpg" alt="">
                                            <p>40-50mm, 50-65mm</p>
                                        </li>
                                        <li>
                                            <img src="img/icon/doorThickness/icon_2.jpg" alt="">
                                            <p>50-100mm (special order)</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="col-lg-12 clear">
                        <ul class="lockingguide">
                            <li><a href="#" class="lockingBTN">Locking Guide</a></li>
                            <li>
                                <div class="turnLeve">
                                    <img src="img/icon/dt.png" alt="">
                                    <p>Turn Lever handle in upward direction after closing the door</p>
                                </div>
                            </li>
                        </ul>
                    </div>


                </div>

            </div>
        </section>
        <!-- // Pr Detail sec -->
        <!-- explore range -->
        <section class="exploreSec">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Explore <span>range</span></h2>
                        <div id="slider3D" class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="slider3Dbox">
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImg/img_1.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImg/img_2.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImg/img_3.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImg/img_1.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImg/img_2.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImg/img_3.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // explore range -->
    </div>
    <!-- // content sec -->

</asp:Content>

