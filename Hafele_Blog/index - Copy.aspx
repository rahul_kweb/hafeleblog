﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Hafele Digital Locks | Smart Lock by Hafele | Digital Security</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Check out the amazing digital locks by Hafele with amazing features like smart password, smart voice, smart security and smart visitor image capture">
    <link rel="shortcut icon" type="image/x-icon" href="img/nmims.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/style.css">
    <style>

        .popup {
            width: 1000px;
        }

        .modal-body {
            overflow: hidden;
        }

       @media only screen and (max-width: 480px){
            .popup {
            width: auto;
            }
            .popup .col-sm-4{margin-bottom:10px;}
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- content sec -->
    <div class="content_wrapper">

        <!-- banner sec -->
        <section class="bannerForm_Sec">
            <div class="">
                <div class="mobileLogo">
                    <img src="img/logo.png" alt="">
                </div>
                <div class="bannerIn">
                    <div id="bannerBig" class="owl-carousel owl-theme">
                        <div class="item">
                            <img src="img/banner.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="img/banner_Bell.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="img/banner_place.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="img/banner_revel.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="img/banner_size.jpg" alt="">
                        </div>
                    </div>
                    <div class="banner_logo">
                        <img src="img/logo_1.png" alt="">
                    </div>
                </div>
            </div>

        </section>
        <!-- // banner sec -->
        <!-- feature sec -->
        <section class="featureSec clear">
            <div class="feaure_Txt">
                <h2>Back to<span> the Future…</span></h2>
                <p class="featureDeas">One area that has seen a major evolution over the last 10 decades is ‘home security’. In ancient times people kept their doors open and the intangible ‘neighbourly trust’ was enough security for their homes. But with migration of people from one land to another, suspicious strangers gradually replaced friendly neighbours, and tangible security replaced neighbourly trust. People wanted to keep strangers out of their homes and they did this with the help of a key...</p>
                <%--<a href="#" class="watchLink" data-toggle="modal" data-target="#VideoModal">Watch product video</a>--%>
            </div>
            <img src="img/feature.jpg" alt="">
        </section>
        
        <!-- //feature sec -->

        <!-- video sec -->
        <section class="AmazingFeatureSec clear">
            <div class="yt_videos">
                
                <h3>Watch  <span>Videos</span></h3>
                <div class="col-sm-4">
                        <iframe class="video_groups" width="100%" height="300" src="https://www.youtube.com/embed/FIU8PD3RQCI?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="col-sm-4">
                        <iframe class="video_groups" width="100%" height="300" src="https://www.youtube.com/embed/d0nEYPyDy18?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    </div>
                    <div class="col-sm-4">
                        <iframe class="video_groups" width="100%" height="300" src="https://www.youtube.com/embed/xphOo-NS54E?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
            </div>
            
        </section>
        <!-- //video sec -->

        <!-- amazing feature sec -->
        <section class="AmazingFeatureSec">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Amazing <span>features</span></h3>
                        <div id="navigation" class="owl-carousel">
                            <div class="projectitem">
                                <div class="iconBoxWRP">
                                    <div class="iconBox">
                                        <img src="img/icon/SmartPassword.png" class="default">
                                        <img src="img/icon/SmartPassword1.png" class="deHover" />
                                    </div>
                                    <h5>Smart Password</h5>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="iconBoxWRP">
                                    <div class="iconBox">
                                        <img src="img/icon/SmartVoice.png" class="default">
                                        <img src="img/icon/SmartVoice1.png" class="deHover" />
                                    </div>
                                    <h5>Smart Voice</h5>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="iconBoxWRP">
                                    <div class="iconBox">
                                        <img src="img/icon/SmartEtiquettes.png" class="default">
                                        <img src="img/icon/SmartEtiquettes1.png" class="deHover" />
                                    </div>
                                    <h5>Smart Etiquettes</h5>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="iconBoxWRP">
                                    <div class="iconBox">
                                        <img src="img/icon/SmartSecurity.png" class="default">
                                        <img src="img/icon/SmartSecurity1.png" class="deHover" />
                                    </div>
                                    <h5>Smart Security</h5>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="iconBoxWRP">
                                    <div class="iconBox">
                                        <img src="img/icon/SmartVisitorImageCapture.png" class="default">
                                        <img src="img/icon/SmartVisitorImageCapture1.png" class="deHover" />
                                    </div>
                                    <h5>Smart Visitor Image Capture</h5>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="iconBoxWRP">
                                    <div class="iconBox">
                                        <img src="img/icon/SmartFreeze.png" class="default">
                                        <img src="img/icon/SmartFreeze1.png" class="deHover" />
                                    </div>
                                    <h5>Smart Freeze</h5>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="iconBoxWRP">
                                    <div class="iconBox">
                                        <img src="img/icon/SmartIntruderCapture.png" class="default">
                                        <img src="img/icon/SmartIntruderCapture1.png" class="deHover" />
                                    </div>
                                    <h5>Smart Intruder Capture</h5>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="iconBoxWRP">
                                    <div class="iconBox">
                                        <img src="img/icon/SmartNightVision.png" class="default">
                                        <img src="img/icon/SmartNightVision1.png" class="deHover" />
                                    </div>
                                    <h5>Smart Night Vision</h5>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="iconBoxWRP">
                                    <div class="iconBox">
                                        <img src="img/icon/SmartVideoPhone.png" class="default">
                                        <img src="img/icon/SmartVideoPhone1.png" class="deHover" />
                                    </div>
                                    <h5>Smart Video Phone</h5>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="iconBoxWRP">
                                    <div class="iconBox">
                                        <img src="img/icon/SmartStorage.png" class="default">
                                        <img src="img/icon/SmartStorage1.png" class="deHover" />
                                    </div>
                                    <h5>Smart Storage</h5>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="iconBoxWRP">
                                    <div class="iconBox">
                                        <img src="img/icon/SmartMotionSensor.png" class="default">
                                        <img src="img/icon/SmartMotionSensor1.png" class="deHover" />
                                    </div>
                                    <h5>Smart Motion Sensor</h5>
                                </div>
                            </div>

                        </div>

                        <div id="slider" class="owl-carousel">

                            <div class="projectitem">
                                <div class="tbaDeasIN">
                                    <h4>Smart Password: </h4>
                                    <p>The SMART PASSWORD technology allows you to hide your numeric password between random numbers. Let’s assume that your password is 12345678; the Smart Password function allows you to hide this password before, after or in between other random digits, for example: 9876123456784901. The password can be set up to 8 or 12 digits while the random cushioning numbers can be included up to 30 digits. The Smart Password function is helpful when you don’t want to reveal your password to a person standing next to you while you are accessing your home.</p>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="tbaDeasIN">
                                    <h4>Smart Voice: </h4>
                                    <p>Every Digital Lock from Häfele talks to you during the instances at which you engage with the lock. These interactive engagements could include step-by-step voice guidance by the lock while adding a user, setting a password or enabling a function/mode; or simple voice notiﬁcations from the lock about various operative modes or incorrect usage of the lock.</p>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="tbaDeasIN">
                                    <h4>Smart Etiquettes: </h4>
                                    <p>Since every digital lock from Häfele talks to you, it is pertinent to have the right etiquettes to adjust the volume of the lock voice. This technology allows you to seamlessly adjust the voice volume or put it on mute, especially when you are entering your home in the wee hours of morning ensuring that your neighbours are not jolted out of their sleep by your talking lock.</p>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="tbaDeasIN">
                                    <h4>Smart Security: </h4>
                                    <p>With the different access modes available in the Häfele Digital Locks, you can enable additional security by combining two access authentications; for example, in case of a new house-help who is yet to earn your trust, it could be unsafe to just give her a numeric password access as you could be in danger of her sharing this password with an ally. In such situations adding a face-recognition access as the second authentication can ensure that the person entering your home is not an unwanted stranger.</p>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="tbaDeasIN">
                                    <h4>Smart Visitor Image Capture: </h4>
                                    <p>Hafele’s RE-Bell Video Doorbell allows you to know who had visited your home even when you were not around to receive them. Every visitor’s image and video call can be viewed live if you are around to receive it and is also recorded for you to see in your mobile App later.  In fact, even if you are in a no network zone or your phone is on ﬂight mode and during that time if a guest visits you, the bell clicks and saves the image of the guest on the server which can then be viewed in the app later. You can now be rest assured that no guest coming to your house goes unnoticed.</p>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="tbaDeasIN">
                                    <h4>Smart Freeze: </h4>
                                    <p>The Häfele Digital Locks have a narrow tolerance for incorrect entries of access by way of a wrong password or unfamiliar ﬁngerprint – 5 to 10 wrong entries result in a lock-freeze for up to 5 minutes. This feature reduces the possibilities of unwanted break-ins.</p>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="tbaDeasIN">
                                    <h4>Smart Intruder Capture:</h4>
                                    <p>RE-Veal, the face recognition lock by Häfele, not only enables the highest level of security for your home but also tells you if someone tried to disrupt this security. Its Smart Intruder Capture technology records the face of any unregistered user who has tried to break into your home. The lock can pull out up to 10 such images in case you want to do an instant check.</p>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="tbaDeasIN">
                                    <h4>Smart Night Vision:</h4>
                                    <p>A feature unique to Hafele’s RE-Veal Face Recognition Lock and Hafele’s RE-Bell Video door Bell, Smart Night Vision uses infrared technology to recognize a visitor’s face even in pitch darkness. This feature comes most handy when you or your guests are trying to access your home in the dense hours of the night when the overall lights in your building corridors are dimmed out or completely turned off.</p>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="tbaDeasIN">
                                    <h4>Smart Video Phone: </h4>
                                    <p>Hafele’s RE-AL and RE-Place digital locks can be paired to an existing Video Door Phone using a simple radio frequency unit. Once connected, your Video Door Phone and Digital Lock work seamlessly together as one gadget allowing you the beneﬁts of interacting with a visitor at your doorstep while also enabling you to unlock the door to him through the key button on your video door phone; another feather of convenience to manage your door security system from a remote location. And in case you don’t have a Video Door Phone, you could consider Hafele’s RE-Bell video doorbell which acts individually as a video door phone and cohesively as a complete security solution when clubbed with any of our digital locks!</p>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="tbaDeasIN">
                                    <h4>Smart Storage: </h4>
                                    <p>Häfele’s RE-Bell Video Doorbell comes with the added advantage of recoding video footage and that to for 24 hours continuously! This Smart Storage feature allows you to keep a historic perspective on who entered your home; not just through time logs but through actual videos. The only requirement is that your App-enabled phone (which is paired to the lock) should have an SD card as the videos recorded automatically get saved in the storage memory of an SD card.</p>
                                </div>
                            </div>
                            <div class="projectitem">
                                <div class="tbaDeasIN">
                                    <h4>Smart Motion Sensor: </h4>
                                    <p>Häfele’s RE-Bell Video Doorbell comes with an intuitive smart motion sensor feature which when activated senses the presence of a person or any other moving object within 3 meters of the camera lens. Such a realization is then sent to you as a notiﬁcation on your App–enabled phone alerting you that someone or something was hovering around your main door; an ideal option to enable increased security for your home.</p>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- // amazing feature sec -->
        <!-- explore range -->
        <section class="exploreSec">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Explore <span>range</span></h2>
                        <div id="slider3D" class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-size.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Size</h5>
                                        <p>Because your fashionable door needs its ‘right’ match…</p>
                                    </div>
                                    <img src="img/prImgsl/re-size.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-place.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Place</h5>
                                        <p>Because your mechanical lock needs an upgrade…</p>
                                    </div>
                                    <img src="img/prImgsl/re-place.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-bell.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Bell</h5>
                                        <p>Because you need a <span class="strikethrough">99.99</span> “100” percent…</p>
                                    </div>
                                    <img src="img/prImgsl/re-bell.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-vel.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImgsl/pr_1.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-ai.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-AL</h5>
                                        <p>Because you need a better security solution…</p>
                                    </div>
                                    <img src="img/prImgsl/re-ai.png" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // explore range -->
    </div>
    <!-- // content sec -->

    <!-- Modal -->
    <div id="VideoModal" class="modal fade" role="dialog">
        <div class="modal-dialog popup">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Watch product video</h4>
                </div>
                <div class="modal-body">

                   <%-- <iframe width="100%" height="300" src="https://www.youtube.com/embed/FIU8PD3RQCI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--%>

                    <div class="col-sm-4">
                        <iframe width="100%" height="300" src="https://www.youtube.com/embed/FIU8PD3RQCI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="col-sm-4">
                        <iframe width="100%" height="300" src="https://www.youtube.com/embed/d0nEYPyDy18" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    </div>
                    <div class="col-sm-4">
                        <iframe width="100%" height="300" src="https://www.youtube.com/embed/xphOo-NS54E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

        </div>
    </div>

</asp:Content>

