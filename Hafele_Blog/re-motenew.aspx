﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="re-motenew.aspx.cs" Inherits="re_vel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Hafele</title>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- content sec -->
    <div class="content_wrapper">

        <!-- banner sec -->
            <div class="prodbbanner-img">
                <img src="img/banner-remote.jpg" alt="" class="img-responsive" />
            </div>

        <!-- // banner sec -->
        <!-- Pr Detail sec -->
        <section class="PrDetail_sec productindividual-wrap">
          

            <div class="container">

                <div class="row clear">
                    <div class="col-lg-4 col-sm-4">
                        <div class="prImg">
                            <img src="img/prImg/pr_4.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-8 col-sm-8">
                        <div class="PrDeas">
                            <h2>RE-Mote</h2>
                            <h5>Because you need a smarter way of managing home access; anytime, anywhere</h5>
                            <figcaption>Smarter Security with Remote Access: Its time your digital door security system gets used to your busy lifestyle. With Häfele’s latest
                            Digital Door Lock – REMOTE – this is now a happy possibility. True to  its name, this lock allows you to manage your home access and
                            monitor security remotely, anytime anywhere. The beauty of Häfele’s REMOTE Digital lock is that it works on an offline system with no
                            integration to the web; thereby securing all your data and passwords from getting hacked. REMOTE works on the Bluetooth Technology
                            and can be managed with the user-friendly ‘Häfele Access’ mobile application. You can set multiple access possibilities for yourself as
                            well as your visitors through different password configurations. 

                            </figcaption>
                            
                          
                        </div>
                    </div>
                </div>

                		    	<div class="ro clear">
                    <div class="iconwrpSec">
                        <div class="col-lg-12 row">
                            <div class="subHeading">Mnemonic Guide: <span>RE-MOTE</span></div>
                        </div>

                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">ACCESS MODES:</h4>
                                            <ul class="clear">
                                              <li><img src="img/icon/access/icon_2.jpg" alt=""><p>Key-pad or Password </p></li>
                                    <li><img src="img/icon/access/icon_3.jpg" alt=""><p>RFID (upto 100 unique accesses)</p></li>
                                       <li><img src="img/icon/smartTech/icon_4.jpg" alt=""><p>Mechanical Key (upto 2 keys)</p></li>
                                       <li><img src="img/icon/smartTech/icon_6.jpg" alt=""><p>Bluetooth Enabled Key (Mobile App Access)</p></li>
                                    
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">SMART TECHNOLOGIES: </h4>
                                            <%--<div class="col-lg-6 col-sm-6 PR_specifcation">--%>
                                                <ul class="clear ">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_1.jpg" alt="">
                                                        <p>Smart Password </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_3.jpg" alt="">
                                                        <p>Smart Etiquettes </p>
                                                    </li>
                                                     <li>
                                                        <img src="img/icon/smartTech/icon_2.jpg" alt="">
                                                        <p>Smart Voice</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_5.jpg" alt="">
                                                        <p>Smart Freeze </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_8.jpg" alt="">
                                                        <p>Smart Video Phone**</p>
                                                    </li>

                                                </ul>
                                            <%--</div>--%>
                                           <%-- <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_4.jpg" alt="">
                                                        <p>Smart Security</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_6.jpg" alt="">
                                                        <p>Smart Night Vision</p>
                                                    </li>
                                                </ul>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">LOCKING MODES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/locking/icon_3.jpg" alt="">
                                                    <p>Auto Locking </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_1.jpg" alt="">
                                                    <p>Privacy Locking</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_2.jpg" alt="">
                                                    <p>Manual Locking</p>
                                                </li>

                                                <li>
                                                    <img src="img/icon/locking/icon_5.jpg" alt="">
                                                    <p>Defense Locking</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">OPERATIONAL FEATURES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/oprational/icon_1.jpg" alt="">
                                                    <p>Panic Exit </p>
                                                </li>
                                         
                                                <li>
                                                    <img src="img/icon/oprational/icon_3.jpg" alt="">
                                                    <p>Non-handed Operation</p>
                                                </li>
                                                       <li>
                                                    <img src="img/icon/oprational/icon_7.jpg" alt="">
                                                    <p>Audit TrailsOffline Lock 
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">USER ACCESS RIGHTS:</h4>
                                            <ul class="clear">
                                                <%--<li>
                                                    <img src="img/icon/userAccess/icon_1.jpg" alt="">
                                                    <p>Super Admin </p>
                                                </li>--%>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_3.jpg" alt="">
                                                    <p>User</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_4.jpg" alt="">
                                                    <p>Admin</p>
                                                </li>
                                                
                                                <li>
                                                    <img src="img/icon/userAccess/icon_2.jpg" alt="">
                                                    <p>Guest</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">ALARMS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/alarms/icon_1.jpg" alt="">
                                                    <p>Low Battery </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_2.jpg" alt="">
                                                    <p>Break-in</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_3.jpg" alt="">
                                                    <p>High Temperature</p>
                                                </li>
                                                    <li>
                                                        <img src="img/icon/alarms/icon_4.jpg" alt="">
                                                        <p>Door not Locked</p>
                                                    </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Battery:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_3.jpg" alt="">
                                                    <p>AA (8 batteries) </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/battery/icon_2.jpg" alt="">
                                                    <p>Jump start (9V)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">DOOR THICKNESS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_1.jpg" alt="">
                                                    <p>40-90mm</p>
                                                </li>
                                                <%--<li>
                                                    <img src="img/icon/doorThickness/icon_2.jpg" alt="">
                                                    <p>65-90mm, 90-100mm*</p>
                                                </li>--%>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--<div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Finish:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_1.jpg" alt="">
                                                    <p>Black & Chrome </p>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>--%>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Types of password:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/password/icon-1.png" alt="">
                                                    <p>OTP Password  </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/password/icon-2.png" alt="">
                                                    <p>Bluetooth Key Sharing </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/password/icon-3.png" alt="">
                                                    <p>Period Password </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/password/icon-4.png" alt="">
                                                    <p>Schedule Password </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/password/icon-5.png" alt="">
                                                    <p>Permanent Password </p>
                                                </li>

                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>


                    </div>

               <%--     <div class="col-lg-12 clear">
                        <ul class="lockingguide">
                            <li>
                                <p class="lockingBTN">Locking Guide</p>
                            </li>
                            <li>
                                <div class="turnLeve">
                                    <img src="img/icon/dt.png" alt="">
                                    <p>Turn lever handle in upward direction after closing the door</p>
                                </div>
                            </li>
                        </ul>
                    </div>--%>

                </div>
                

            </div>
        </section>



        <!-- // Pr Detail sec -->

      
        <%--<div class="prodctdummy-content container">
            <p></p>
        </div>--%><!--End of the prodct dummy content-->

        <div class="prodctboxes-imgcontainer">
            <ul class="list-inline">
                <li>
                    <div class="individualprod-boxwrap">

                    </div><!--End of the individual box wrap-->
                </li>
                <li>
                    <div class="individualprod-boxwrap">

                    </div><!--End of the individual box wrap-->
                </li>
                <li>
                    <div class="individualprod-boxwrap">

                    </div><!--End of the individual box wrap-->
                </li>

            </ul>
        </div><!--End of the prodct boxes img container-->

        
         <!-- Pr PrVideo_sec -->
			<section class="PrVideo_sec" style="display:none">
				<div class="container-fluid">
				    <div class="row">
                        <div class="col-lg-12">
                           <h2>RE-Veal <span>Videos</span></h2> 
                            <div class="video_frames yt_videos">
                                <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe class="video_groups"  width="100%" height="315" src="https://www.youtube.com/embed/kM5Fz9MkR6g?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <%--<h4>Hafele Digital Locks – User guide – RE-Veal</h4>--%>
                                        <p>User guide</p>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe class="video_groups"  width="100%" height="315" src="https://www.youtube.com/embed/96SFjwBRXzs?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <%--<h4>Hafele Digital Locks – Installation video – RE-Veal</h4>--%>
                                        <p>Installation video</p>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe class="video_groups"  width="100%" height="315" src="https://www.youtube.com/embed/54oJkW1qwCM?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <%--<h4>Hafele Digital Locks – Quick Installation – RE-Veal</h4>--%>
                                        <p>Quick Installation video</p>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe class="video_groups"  width="100%" height="315" src="https://www.youtube.com/embed/SZS6CfXjn6k?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                         <%--<h4>Hafele Digital Locks – Manual – RE-Veal</h4>--%>
                                        <p>Manual</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</section>
			<!-- // Pr PrVideo_sec -->	

         <!--Start of the explore range-->
       <section class="exploreSec">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Explore <span>range</span></h2>
                        <div id="slider3D" class="owl-carousel owl-theme">
                           <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-sizenew.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Size</h5>
                                        <p>Because your fashionable door needs its ‘right’ match…</p>
                                    </div>
                                    <img src="img/prImgsl/re-size.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-placenew.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Place</h5>
                                        <p>Because your mechanical lock needs an upgrade…</p>
                                    </div>
                                    <img src="img/prImgsl/re-place.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-bellnew.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Bell</h5>
                                        <p>Because you need a <span class="strikethrough">99.99</span> “100” percent…</p>
                                    </div>
                                    <img src="img/prImgsl/re-bell.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-velnew.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImgsl/pr_1.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-ainew.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-AL</h5>
                                        <p>Because you need a better security solution…</p>
                                    </div>
                                    <img src="img/prImgsl/re-ai.png" alt="">
                                </div>
                            </div>
                        <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-tronew.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-TRO</h5>
                                        <p>Because your office cabins also deserve the same digital security as your homes</p>
                                    </div>
                                    <img src="img/prImgsl/re-tro.png" alt="">
                                </div>
                            </div>
                         <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-designnew.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-DESIGN</h5>
                                        <p>Because your main doors deserve the best of both – top notch security and elegant design </p>
                                    </div>
                                    <img src="img/prImgsl/re-design.png" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End of the explore range-->


    </div>
    <!-- // content sec -->

    <script>
        $(document).ready(function () {
            //$('.viewall-btnwrap .viewallbtn').click(function () {
            //    $('.navigation-listing li').removeClass('active');
            //    $('.navigation-listing .productsdesktop').addClass('active');

            //});
        });
    </script>
</asp:Content>

