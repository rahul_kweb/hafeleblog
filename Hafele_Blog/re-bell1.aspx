﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="re-bell.aspx.cs" Inherits="re_bell" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Hafele</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="img/nmims.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/style.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- content sec -->
    <div class="content_wrapper">

        <!-- banner sec -->
        <section class="bannerForm_Sec">
            <div class="">
                <div class="mobileLogo">
                   <a href="index.aspx"> <img src="img/logo.png" alt=""></a>
                </div>
                <div class="bannerIn">
                    <div id="bannerBig" class="owl-carousel owl-theme">
                        <div class="item">
                            <img src="img/banner_Bell.jpg" alt="">
                        </div>
                    </div>
                    <div class="banner_logo">
                        <img src="img/logo_1.png" alt=""></div>
                </div>
            </div>

        </section>
        <!-- // banner sec -->
        <!-- Pr Detail sec -->
        <section class="PrDetail_sec">
            <div class="container">

                <div class="row clear">
                    <div class="col-lg-5 col-sm-5">
                        <div class="prImg">
                            <img src="img/prImg/re-bell.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-7 col-sm-7">
                        <div class="PrDeas">
                            <h2>RE-Bell</h2>
                            <h5>Because you need a <span>99.99</span> “100” percent…</h5>
                            <figcaption>For the Feature-fanatics who have an eye for detail and are always looking to upgrade to latest technologies for better features and enhancements…</figcaption>
                            <p>Häfele’s RE-Bell video doorbell completes our offering for holistic home security with an added touch of smartness. This WiFi enabled solution can be integrated into your smartphone through a feature-packed App that allows you to remotely manage all functions at your fingertips. This means that you could be lazing in your bed or sitting at your office desk or shopping at your favourite mall and yet you can see or even talk to the person standing at your doorstep. This takes care of security even from remote areas while also ensuring that you never miss a chance to know who tried to reach you while you were away. You can also capture intruder images to keep a special check on strangers or unwanted visitors; and this is possible even in pitch darkness thanks to RE-Bell’s integrated night-vision function. The highly sensitive motion sensor immediately recognizes the presence of a person and sends you an alert on the mobile app informing you that someone is awaiting access to your home. You can also download a log of recorded video clippings from the RE-Bell through the mobile app – giving you a historical perspective on who tried to reach you and when. The RE-Bell presents the highest standards of technology through its High Deﬁnition (HD) video quality and 165 degree rotational camera.</p>
                        </div>
                    </div>
                </div>

                <div class="ro clear">
                    <div class="iconwrpSec">
                        <div class="col-lg-12 row">
                            <div class="subHeading">Mnemonic Guide: <span>RE-Bell</span></div>
                        </div>

                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation clear">
                                            <h4 class="icon_head">SMART TECHNOLOGIES: </h4>
                                            <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_7.jpg" alt="">
                                                        <p>Smart Video Recording </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_9.jpg" alt="">
                                                        <p>Smart Motion Sensor </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_8.jpg" alt="">
                                                        <p>Smart Visual Speach
                                                            <br>
                                                            (1280 X 960 @ 10fps) </p>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_6.jpg" alt="">
                                                        <p>Smart Night Vision (2m)</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_10.jpg" alt="">
                                                        <p>Smart Storage (micro SD card)</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_11.jpg" alt="">
                                                        <p>Smart Visitor Image Capture</p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">OPERATIONAL FEATURES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/oprational/icon_4.jpg" alt="">
                                                    <p>Water Resistant (IP65) </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/oprational/icon_5.jpg" alt="">
                                                    <p>IOS 7 & ANDROID 4.0 and above</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/oprational/icon_6.jpg" alt="">
                                                    <p>3x Zoom</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">POWER:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/power/icon_1.jpg" alt="">
                                                    <p>12 Volt – DC Driver</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/power/icon_2.jpg" alt="">
                                                    <p>WiFi Enabled</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>
        <!-- // Pr Detail sec -->

         <!-- Pr PrVideo_sec -->
			<section class="PrVideo_sec">
				<div class="container-fluid">
				    <div class="row">
                        <div class="col-lg-12">
                           <h2>RE-Bell <span>Videos</span></h2> 
                            <div class="video_frames yt_videos">
                               <%-- <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/ozojH6PFSEo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p>User guide – RE-Size</p>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/ozojH6PFSEo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p>Installation video RE-Size</p>
                                    </div>
                                </div>--%>
                                <div class="col-sm-6">
                                    <div class="videoWrp">
                                        <iframe class="video_groups"  width="100%" height="315" src="https://www.youtube.com/embed/qyvXLMicswQ?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p>Quick Installation video</p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="videoWrp">
                                        <iframe class="video_groups"  width="100%" height="315" src="https://www.youtube.com/embed/2teQg9v2F-U?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p>Manual</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</section>
			<!-- // Pr PrVideo_sec -->	

        <!-- explore range -->
        <section class="exploreSec">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Explore <span>range</span></h2>
                        <div id="slider3D" class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-size.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Size</h5>
                                        <p>Because your fashionable door needs its ‘right’ match…</p>
                                    </div>
                                    <img src="img/prImgsl/re-size.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-place.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Place</h5>
                                        <p>Because your mechanical lock needs an upgrade…</p>
                                    </div>
                                    <img src="img/prImgsl/re-place.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-bell.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Bell</h5>
                                        <p>Because you need a <span class="strikethrough">99.99</span> “100” percent…</p>
                                    </div>
                                    <img src="img/prImgsl/re-bell.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-vel.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImgsl/pr_1.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-ai.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-AL</h5>
                                        <p>Because you need a better security solution…</p>
                                    </div>
                                    <img src="img/prImgsl/re-ai.png" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // explore range -->
    </div>
    <!-- // content sec -->
</asp:Content>

