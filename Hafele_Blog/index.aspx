﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Hafele Digital Door Lock | Digital Door Lock India</title>
    <meta name="description" content="Reinvent access with Hafele's Digital Door Lock range of smart locks with numerous features and all the latest technology to provide a seamless keyless entry.">

    <link rel="canonical" href="https://digital-locks.hafeleindia.co.in/" />

    <meta property="og:url" content="https://digital-locks.hafeleindia.co.in/" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Hafele Digital Door Lock | Digital Door Lock India" />
    <meta property="og:description" content="Reinvent access with Hafele's Digital Door Lock range of smart locks with numerous features and all the latest technology to provide a seamless keyless entry." />
    <meta property="og:image" content="https://digital-locks.hafeleindia.co.in/img/banner.jpg" />

    <meta name="twitter:card" content="summary" />
    <meta property="og:url" content="https://digital-locks.hafeleindia.co.in/" />
    <meta property="og:title" content="Hafele Digital Door Lock | Digital Door Lock India" />
    <meta property="og:description" content="Reinvent access with Hafele's Digital Door Lock range of smart locks with numerous features and all the latest technology to provide a seamless keyless entry." />
    <meta property="og:image" content="https://digital-locks.hafeleindia.co.in/img/banner.jpg" />
    <style>
        .modal-body {
            overflow: hidden;
        }

        @media only screen and (max-width: 480px) {
            .popup {
                width: auto;
            }

                .popup .col-sm-4 {
                    margin-bottom: 10px;
                }
        }

        a.fancyLink {position: relative;display: block;}
        img.img-responsive.playButton{ position:absolute; left:50%; top:50%; margin-left:-25px; margin-top:-13px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="myh1">Hafele Digital Door Lock</h1>
    <!--Start of the main wrap-->
    <div class="main-wrapper">
        <!-- content sec -->
        <div class="content_wrapper">
            <!-- banner sec -->
            <section class="bannerForm_Sec">
                <div class="">
                    <%-- <div class="mobileLogo">
					<img loading="lazy" src="img/logo.png" alt="">
					</div>--%>
                    <div class="bannerIn">
                        <div id="bannerBig" class="owl-carousel owl-theme homepage-owlcarsouel">
                            <div class="item">
                                <picture>
                                    <source loading="lazy" srcset="img/product-banner.webp" type="image/webp" />
                                    <source loading="lazy" srcset="img/product-banner.jpeg" type="image/jpeg" />
                                    <img loading="lazy" src="img/product-banner.jpeg" alt="Hafele Digital Lock">
                                </picture>
                            </div>
                            <div class="item">
                                <picture>
                                    <source loading="lazy" srcset="img/banner_revel.webp" type="image/webp" />
                                    <source loading="lazy" srcset="img/banner_revel.jpg" type="image/jpeg" />
                                    <img loading="lazy" src="img/banner_revel.jpg" alt="RE-VEAL Biometric Keypad Door Lock">
                                </picture>
                            </div>
                            <div class="item">
                                <picture>
                                    <source loading="lazy" srcset="img/banner-retro.webp" type="image/webp" />
                                    <source loading="lazy" srcset="img/banner-retro.jpg" type="image/jpeg" />
                                    <img loading="lazy" src="img/banner-retro.jpg" alt="RE-TRO Indoor Keypad Door Lock">
                                </picture>
                            </div>
                            <div class="item">
                                <picture>
                                    <source loading="lazy" srcset="img/banner_redesign.webp" type="image/webp" />
                                    <source loading="lazy" srcset="img/banner_redesign.jpg" type="image/jpeg" />
                                    <img loading="lazy" src="img/banner_redesign.jpg" alt="RE-DESIGN Best Keypad Door Lock">
                                </picture>
                            </div>
                            <div class="item">
                                <picture>
                                    <source loading="lazy" srcset="img/banner-remote.webp" type="image/webp" />
                                    <source loading="lazy" srcset="img/banner-remote.jpg" type="image/jpeg" />
                                    <img loading="lazy" src="img/banner-remote.jpg" alt="RE-MOTE Hafele Digital Door Lock">
                                </picture>
                            </div>
                        </div>
                        <div class="banner_logo">
                            <picture>
                                <img loading="lazy" src="img/logo_1.png" alt="">
                            </picture>
                        </div>
                    </div>
            </section>
            <!-- // banner sec -->
            <!--Start of the auto slider banner-->
            <div class="container mt-30">
                <div class="row">
                    <div class="col-md-12">
                        <div class="home-security d-flex">
                            <div class="home-security-banner border-shadow">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <picture>
                                                <source loading="lazy" srcset="img/banner-1.webp" type="image/webp">
                                                <source loading="lazy" srcset="img/banner-1.jpg" type="image/jpeg">
                                                <img loading="lazy" src="img/banner-1.jpg" alt="" class="img-responsive">
                                            </picture>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--home-security-banner-->
                            <div class="home-security-content">
                                <div id="myCarouseltext" class="carousel slide" data-ride="carousel">
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div class="home-security-card">
                                                <p class="info">
                                                    Häfele introduces a new perspective to home security with its integrated range of Digital Home Security Solutions; allowing you to control multi-dimensional aspects of home access through a single locking device, as per your specific lifestyle and at the leisure of your convenience.
                                                </p>
                                                <h4 class="highlight-text">So it’s time to move to a smarter way of life -it’s time to “REinventAccess”!</h4>
                                            </div>
                                            <!--home-security-card-->
                                        </div>
                                        <%--						    <div class="item">
			<div class="home-security-card">
			<p class="info">
			Häfele introduces a new perspective to home security with its integrated range of Digital Home Security Solutions; allowing you to control multi-dimensional aspects of home access through a single locking device, as per your specific lifestyle and at the leisure of your convenience.
			</p>
			<h4 class="highlight-text">So it’s time to move to a smarter way of life -it’s time to “REinventAccess”!</h4>
			</div>
			</div>
			
			<div class="item">
			<div class="home-security-card">
			<p class="info">
			Häfele introduces a new perspective to home security with its integrated range of Digital Home Security Solutions; allowing you to control multi-dimensional aspects of home access through a single locking device, as per your specific lifestyle and at the leisure of your convenience.
			</p>
			<h4 class="highlight-text">So it’s time to move to a smarter way of life -it’s time to “REinventAccess”!</h4>
			</div>
			</div>--%>
                                    </div>
                                </div>
                            </div>
                            <!--home-security-content-->
                        </div>
                    </div>
                </div>
            </div>
            <!--End of the auto slider banner-->
            <!--Start of the explore range-->
            <div class="explorerange-wrap">
                <div class="explorerange-innerwrap redbottom-border">
                    <h3>Explore <span>range</span></h3>
                    <div class="explorerange-wrap">
                        <div class="explorerange-innergrid xborder-shadow hidden-sm hidden-xs">
                            <div class="list-inline explorerange-gridlisting">
                                <div class="individual-explorerange reveal-wrap">
                                    <div class="individualexplore-innerlist">
                                        <a href="re-veal.aspx" data-toggle="tooltip" title="<img src='img/productimg/reveal-hover.jpg' />" data-placement="right">
                                            <div class="exploreimg-div">
                                                <img loading="lazy" src="img/productimg/exploreimg-1.png" alt="" class="img-responsive" />
                                                <div class="exploreimg-hiddenarrow">
                                                    <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                </div>
                                                <!--End of the explore img hidden arrow-->
                                            </div>
                                            <!--End f the explore img-div-->
                                        </a>
                                        <div class="explorerange-content">
                                            <h4>RE-veal</h4>
                                            <p>Silk id Fingerprint</p>
                                        </div>
                                        <!--End of the explore range content-->
                                    </div>
                                    <!--End if the individual explore inner list-->
                                </div>
                                <!--End  of the individual explore range-->
                                <div class="individual-explorerange real-wrap">
                                    <div class="individualexplore-innerlist ">
                                        <a href="re-al.aspx" data-toggle="tooltip" title="<img src='img/productimg/real-hover.jpg' />" data-placement="right">
                                            <div class="exploreimg-div">
                                                <img loading="lazy" src="img/productimg/exploreimg-2.png" alt="" class="img-responsive" />
                                                <div class="exploreimg-hiddenarrow">
                                                    <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                </div>
                                                <!--End of the explore img hidden arrow-->
                                            </div>
                                            <!--End f the explore img-div-->
                                        </a>
                                        <div class="explorerange-content">
                                            <h4>RE-al</h4>
                                            <p>Smart Video Phone</p>
                                        </div>
                                        <!--End of the explore range content-->
                                    </div>
                                    <!--End if the individual explore inner list-->
                                </div>
                                <!--End  of the individual explore range-->
                                <div class="individual-explorerange retro-wrap">
                                    <div class="individualexplore-innerlist ">
                                        <a href="re-tro.aspx" data-toggle="tooltip" title="<img src='img/productimg/retro-hover.jpg' />" data-placement="left">
                                            <div class="exploreimg-div">
                                                <img loading="lazy" src="img/productimg/exploreimg-6.png" alt="" class="img-responsive" />
                                                <div class="exploreimg-hiddenarrow">
                                                    <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                </div>
                                                <!--End of the explore img hidden arrow-->
                                            </div>
                                            <!--End f the explore img-div-->
                                        </a>
                                        <div class="explorerange-content">
                                            <h4>RE-tro</h4>
                                            <p>Auto Locking</p>
                                        </div>
                                        <!--End of the explore range content-->
                                    </div>
                                    <!--End if the individual explore inner list-->
                                </div>
                                <!--End  of the individual explore range-->
                                <div class="individual-explorerange redesign-wrap">
                                    <div class="individualexplore-innerlist ">
                                        <a href="re-design.aspx" data-toggle="tooltip" title="<img src='img/productimg/redesign-hover.jpg' />" data-placement="right">
                                            <div class="exploreimg-div">
                                                <img loading="lazy" src="img/productimg/exploreimg-7.png" alt="" class="img-responsive" />
                                                <div class="exploreimg-hiddenarrow">
                                                    <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                </div>
                                                <!--End of the explore img hidden arrow-->
                                            </div>
                                            <!--End f the explore img-div-->
                                        </a>
                                        <div class="explorerange-content">
                                            <h4>RE-design</h4>
                                            <p>Auto Locking</p>
                                        </div>
                                        <!--End of the explore range content-->
                                    </div>
                                    <!--End if the individual explore inner list-->
                                </div>
                                <!--End  of the individual explore range-->
                                <div class="individual-explorerange remote-wrap">
                                    <div class="individualexplore-innerlist ">
                                        <a href="re-mote.aspx" data-toggle="tooltip" title="<img src='img/productimg/remote-hover.jpg' />" data-placement="right">
                                            <div class="exploreimg-div">
                                                <img loading="lazy" src="img/productimg/exploreimg-8.png" alt="" class="img-responsive" />
                                                <div class="exploreimg-hiddenarrow">
                                                    <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                </div>
                                                <!--End of the explore img hidden arrow-->
                                            </div>
                                            <!--End f the explore img-div-->
                                        </a>
                                        <div class="explorerange-content">
                                            <h4>RE-mote</h4>
                                            <p>Smart Etiquettes</p>
                                        </div>
                                        <!--End of the explore range content-->
                                    </div>
                                    <!--End if the individual explore inner list-->
                                </div>
                                <!--End  of the individual explore range-->
                            </div>
                        </div>
                        <!--End of the explore range inner grid-->
                        <div class="mobexplore-rangewrapper hidden-md hidden-lg">
                            <div class="mobexplore-innerrange">
                                <div class="owl-carousel owl-theme mobexplore-rangecarsouel">
                                    <div class="item row">
                                        <div class="individual-explorerange reveal-wrap col-xs-12">
                                            <div class="individualexplore-innerlist">
                                                <a href="re-veal.aspx">
                                                    <div class="exploreimg-div">
                                                        <img loading="lazy" src="img/productimg/exploreimg-1.png" alt="" class="img-responsive" />
                                                        <div class="exploreimg-hiddenarrow">
                                                            <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                        </div>
                                                        <!--End of the explore img hidden arrow-->
                                                    </div>
                                                    <!--End f the explore img-div-->
                                                </a>
                                                <div class="explorerange-content">
                                                    <h4>RE-veal</h4>
                                                    <p>Silk id Fingerprint</p>
                                                </div>
                                                <!--End of the explore range content-->
                                            </div>
                                            <!--End if the individual explore inner list-->
                                        </div>
                                        <!--End  of the individual explore range-->
                                    </div>
                                    <!--End of the item-->

                                    <div class="item row">
                                        <div class="individual-explorerange real-wrap col-xs-12">
                                            <div class="individualexplore-innerlist ">
                                                <a href="re-al.aspx">
                                                    <div class="exploreimg-div">
                                                        <img loading="lazy" src="img/productimg/exploreimg-2.png" alt="" class="img-responsive" />
                                                        <div class="exploreimg-hiddenarrow">
                                                            <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                        </div>
                                                        <!--End of the explore img hidden arrow-->
                                                    </div>
                                                    <!--End f the explore img-div-->
                                                </a>
                                                <div class="explorerange-content">
                                                    <h4>RE-al</h4>
                                                    <p>Smart Video Phone</p>
                                                </div>
                                                <!--End of the explore range content-->
                                            </div>
                                            <!--End if the individual explore inner list-->
                                        </div>
                                        <!--End  of the individual explore range-->
                                    </div>
                                    <!--End of the item-->

                                    <div class="row item">
                                        <div class="individual-explorerange retro-wrap col-xs-12">
                                            <div class="individualexplore-innerlist ">
                                                <a href="re-tro.aspx">
                                                    <div class="exploreimg-div">
                                                        <img loading="lazy" src="img/productimg/exploreimg-6.png" alt="" class="img-responsive" />
                                                        <div class="exploreimg-hiddenarrow">
                                                            <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                        </div>
                                                        <!--End of the explore img hidden arrow-->
                                                    </div>
                                                    <!--End f the explore img-div-->
                                                </a>
                                                <div class="explorerange-content">
                                                    <h4>RE-tro</h4>
                                                    <p>Auto Locking</p>
                                                </div>
                                                <!--End of the explore range content-->
                                            </div>
                                            <!--End if the individual explore inner list-->
                                        </div>
                                        <!--End  of the individual explore range-->
                                    </div>
                                    <!--End of the item-->

                                    <div class="row item">
                                        <div class="individual-explorerange redesign-wrap col-xs-12">
                                            <div class="individualexplore-innerlist ">
                                                <a href="re-design.aspx">
                                                    <div class="exploreimg-div">
                                                        <img loading="lazy" src="img/productimg/exploreimg-7.png" alt="" class="img-responsive" />
                                                        <div class="exploreimg-hiddenarrow">
                                                            <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                        </div>
                                                        <!--End of the explore img hidden arrow-->
                                                    </div>
                                                    <!--End f the explore img-div-->
                                                </a>
                                                <div class="explorerange-content">
                                                    <h4>RE-design</h4>
                                                    <p>Auto Locking</p>
                                                </div>
                                                <!--End of the explore range content-->
                                            </div>
                                            <!--End if the individual explore inner list-->
                                        </div>
                                        <!--End  of the individual explore range-->
                                    </div>
                                    <!--End of the item-->

                                    <div class="row item">
                                        <div class="individual-explorerange remote-wrap col-xs-12">
                                            <div class="individualexplore-innerlist ">
                                                <a href="re-mote.aspx">
                                                    <div class="exploreimg-div">
                                                        <img loading="lazy" src="img/productimg/exploreimg-8.png" alt="" class="img-responsive" />
                                                        <div class="exploreimg-hiddenarrow">
                                                            <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                        </div>
                                                        <!--End of the explore img hidden arrow-->
                                                    </div>
                                                    <!--End f the explore img-div-->
                                                </a>
                                                <div class="explorerange-content">
                                                    <h4>RE-mote</h4>
                                                    <p>Smart Etiquettes</p>
                                                </div>
                                                <!--End of the explore range content-->
                                            </div>
                                            <!--End if the individual explore inner list-->
                                        </div>
                                        <!--End  of the individual explore range-->
                                    </div>
                                    <!--End of the item-->
                                    
                                </div>
                                <!--End of he mob owl explore carsouel-->
                            </div>
                            <!--End if the mobexplore inner range-->
                        </div>
                        <!--End of the mob explore range wrapper-->
                    </div>
                    <!--End of the explore range grid wrap-->
                </div>
                <!--End of the explore range inner wrap-->
            </div>
            <!--End of the explore range wrap-->
            <!--End of the explore range-->
            <!-- amazing feature sec -->
            <section class="AmazingFeatureSec">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class=" redbottom-border">
                                <h3>Amazing <span>features</span></h3>
                            </div>
                            <div id="navigation" class="owl-carousel  amazingfeature-wrap">
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartPassword.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartPassword1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartVoice.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartVoice1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartEtiquettes.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartEtiquettes1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartSecurity.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartSecurity1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartVisitorImageCapture.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartVisitorImageCapture1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartFreeze.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartFreeze1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartIntruderCapture.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartIntruderCapture1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartNightVision.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartNightVision1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartVideoPhone.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartVideoPhone1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartStorage.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartStorage1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartMotionSensor.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartMotionSensor1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="slider" class="owl-carousel">
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Password: </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>The SMART PASSWORD technology allows you to hide your numeric password between random numbers. Let’s assume that your password is 12345678; the Smart Password function allows you to hide this password before, after or in between other random digits, for example: 9876123456784901. The password can be set up to 8 or 12 digits while the random cushioning numbers can be included up to 30 digits. The Smart Password function is helpful when you don’t want to reveal your password to a person standing next to you while you are accessing your home.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Voice: </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>Every Digital Lock from Häfele talks to you during the instances at which you engage with the lock. These interactive engagements could include step-by-step voice guidance by the lock while adding a user, setting a password or enabling a function/mode; or simple voice notiﬁcations from the lock about various operative modes or incorrect usage of the lock.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Etiquettes: </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>Since every digital lock from Häfele talks to you, it is pertinent to have the right etiquettes to adjust the volume of the lock voice. This technology allows you to seamlessly adjust the voice volume or put it on mute, especially when you are entering your home in the wee hours of morning ensuring that your neighbours are not jolted out of their sleep by your talking lock.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Security: </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>With the different access modes available in the Häfele Digital Locks, you can enable additional security by combining two access authentications; for example, in case of a new house-help who is yet to earn your trust, it could be unsafe to just give her a numeric password access as you could be in danger of her sharing this password with an ally. In such situations adding a face-recognition access as the second authentication can ensure that the person entering your home is not an unwanted stranger.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Visitor Image Capture: </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>Hafele’s RE-Bell Video Doorbell allows you to know who had visited your home even when you were not around to receive them. Every visitor’s image and video call can be viewed live if you are around to receive it and is also recorded for you to see in your mobile App later.  In fact, even if you are in a no network zone or your phone is on ﬂight mode and during that time if a guest visits you, the bell clicks and saves the image of the guest on the server which can then be viewed in the app later. You can now be rest assured that no guest coming to your house goes unnoticed.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Freeze: </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>The Häfele Digital Locks have a narrow tolerance for incorrect entries of access by way of a wrong password or unfamiliar ﬁngerprint – 5 to 10 wrong entries result in a lock-freeze for up to 5 minutes. This feature reduces the possibilities of unwanted break-ins.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Intruder Capture:</h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>RE-Veal, the face recognition lock by Häfele, not only enables the highest level of security for your home but also tells you if someone tried to disrupt this security. Its Smart Intruder Capture technology records the face of any unregistered user who has tried to break into your home. The lock can pull out up to 10 such images in case you want to do an instant check.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Night Vision:</h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>A feature unique to Hafele’s RE-Veal Face Recognition Lock and Hafele’s RE-Bell Video door Bell, Smart Night Vision uses infrared technology to recognize a visitor’s face even in pitch darkness. This feature comes most handy when you or your guests are trying to access your home in the dense hours of the night when the overall lights in your building corridors are dimmed out or completely turned off.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Video Phone:  </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>Hafele’s RE-AL and RE-Place digital locks can be paired to an existing Video Door Phone using a simple radio frequency unit. Once connected, your Video Door Phone and Digital Lock work seamlessly together as one gadget allowing you the beneﬁts of interacting with a visitor at your doorstep while also enabling you to unlock the door to him through the key button on your video door phone; another feather of convenience to manage your door security system from a remote location. And in case you don’t have a Video Door Phone, you could consider Hafele’s RE-Bell video doorbell which acts individually as a video door phone and cohesively as a complete security solution when clubbed with any of our digital locks!</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Storage:  </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>Häfele’s RE-Bell Video Doorbell comes with the added advantage of recoding video footage and that to for 24 hours continuously! This Smart Storage feature allows you to keep a historic perspective on who entered your home; not just through time logs but through actual videos. The only requirement is that your App-enabled phone (which is paired to the lock) should have an SD card as the videos recorded automatically get saved in the storage memory of an SD card.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Motion Sensor:  </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>Häfele’s RE-Bell Video Doorbell comes with an intuitive smart motion sensor feature which when activated senses the presence of a person or any other moving object within 3 meters of the camera lens. Such a realization is then sent to you as a notiﬁcation on your App–enabled phone alerting you that someone or something was hovering around your main door; an ideal option to enable increased security for your home.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- // amazing feature sec -->
            <!-- video sec -->
            <section class="AmazingFeatureSec clear videosfeature-wrap">
                <div class="yt_videos container">
                    <div class=" redbottom-border">
                        <h3>Watch  <span>Videos</span></h3>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-6">
                            <a data-fancybox="" href="https://www.youtube.com/watch?v=FIU8PD3RQCI" class="fancyLink">
                                <img loading="lazy" class="img-responsive" src="https://img.youtube.com/vi/FIU8PD3RQCI/hqdefault.jpg" />
                                <img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
                            </a>
                        <h4>No reason to smile today?</h4>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <a data-fancybox="" href="https://www.youtube.com/watch?v=d0nEYPyDy18" class="fancyLink">
                                <img loading="lazy" class="img-responsive" src="https://img.youtube.com/vi/d0nEYPyDy18/hqdefault.jpg" />
                                <img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
                            </a>
                        <h4>Invited for a Theme Party?</h4>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <a data-fancybox="" href="https://www.youtube.com/watch?v=xphOo-NS54E" class="fancyLink">
                                <img loading="lazy" class="img-responsive" src="https://img.youtube.com/vi/xphOo-NS54E/hqdefault.jpg" />
                                <img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
                            </a>
                        <h4>Cant find your keys in the dark?</h4>
                        </div>
                        <div class="col-sm-12 all-vdo-up">
                        <a href="video.aspx" class="all-vdo">View All Videos</a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- //video sec -->
        </div>
        <!-- // content sec -->
        <!--Start of the slider wrap-->
    </div>
    <!--End of the main wrapper-->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "Hafele Appliances",
  "url": "https://hafeleappliances.com/",
  "sameAs": [
  "https://www.facebook.com/hafeleindia/",
  "https://twitter.com/hafeleindia",
  "https://in.linkedin.com/company/hafele-india",
  "https://www.youtube.com/user/HaefeleIN1",
  "https://www.instagram.com/hafele_india/"
  ]
 }
    </script>
     <script type="application/ld+json">
      {
      "@context": "http://schema.org/",
      "@type": "Organization",
      "url": "http://digital-locks.hafeleindia.co.in/",
      "logo": "https://digital-locks.hafeleindia.co.in/img/logo.png"
      }
    </script>
</asp:Content>
