﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="productfeature.aspx.cs" Inherits="productfeature" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <title>Best Keypad Door Lock | Product feature |Hafele Digital Lock</title>
<meta name="description" content="Know more about how Hafele's new series of best keypad door lock is the best for you when it comes to digital locks through a list of features and user guide.">
   
<link rel="canonical" href="https://digital-locks.hafeleindia.co.in/product-feature">

<meta property="og:url"               content="https://digital-locks.hafeleindia.co.in/product-feature" />												
<meta property="og:type"              content="article" />												
<meta property="og:title"             content="Best Keypad Door Lock | Product feature |Hafele Digital Lock"/>												
<meta property="og:description"       content="Know more about how Hafele's new series of best keypad door lock is the best for you when it comes to digital locks through a list of features and user guide."/>												
<meta property="og:image"             content= "https://digital-locks.hafeleindia.co.in/img/logo.png"/>		

<meta name="twitter:card" content="summary" />														
<meta property="og:url" content="https://digital-locks.hafeleindia.co.in/product-feature" />														
<meta property="og:title" content="Best Keypad Door Lock | Product feature |Hafele Digital Lock"/>														
<meta property="og:description" content="Know more about how Hafele's new series of best keypad door lock is the best for you when it comes to digital locks through a list of features and user guide." />														
<meta property="og:image" content="https://digital-locks.hafeleindia.co.in/img/logo.png" />	

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<h1 class="myh1">Product Features for Hafele Digital Door Lock</h1>

<!--VIDEO CONTAINER-->
<div class="prodfeature-container">
	<div class="container prodfeature-innerwrap">
		<div class="row">
			<div class="col-md-12">
				<div class="video-body">

					<!--VIDEO HEADER-->
					<div class="video-header">
						<h4 class="heading">Product <span>features</span></h4>
						<%--<p class="info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>--%>
					</div><!--video-header-->

					<div class="video-tab-box">
						
						<div class="video-tab-link">
							<ul class="nav nav-tabs">
                              <li class="active"><a data-toggle="tab" href="#re-veal">RE-VEAL</a></li>
                                <li><a data-toggle="tab" href="#re-al">RE-AL</a></li>
							  <li><a data-toggle="tab" href="#re-design">RE-DESIGN</a></li>
							  <li><a data-toggle="tab" href="#re-tro">RE-TRO</a></li>
							  <li><a data-toggle="tab" href="#re-mote">RE-MOTE</a></li>
							  
							 
							</ul>
						</div>

						<div class="tab-content video-box prodfeature-box">
                          <div id="re-veal" class="tab-pane fade in active">
						    <div class="col-md-12 no-padding">
						    	<div class="ro clear">
                    <div class="iconwrpSec">
                        <div class="col-lg-12 row">
                            <div class="subHeading">Mnemonic Guide: <span>RE-Veal</span></div>
                        </div>

                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">ACCESS MODES:</h4>
                                            <ul>
                                                <li>
                                                    <img src="img/icon/access/icon_1.jpg" alt="">
                                                    <p>Silk id Fingerprint (upto 100 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_2.jpg" alt="">
                                                    <p>Key-pad or Password (upto 100 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_3.jpg" alt="">
                                                    <p>RFID (upto 100 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_4.jpg" alt="">
                                                    <p>Mechanical Key (4 user keys,1 construction key)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_5.jpg" alt="">
                                                    <p>Face Recognition (upto 100 unique accesses)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">SMART TECHNOLOGIES: </h4>
                                            <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_1.jpg" alt="">
                                                        <p>Smart Password </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_3.jpg" alt="">
                                                        <p>Smart Etiquettes </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_5.jpg" alt="">
                                                        <p>Smart Freeze </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_7.jpg" alt="">
                                                        <p>Smart Intruder (upto 10 photos)</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_2.jpg" alt="">
                                                        <p>Smart Voice</p>
                                                    </li>



                                                </ul>
                                            </div>
                                            <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_4.jpg" alt="">
                                                        <p>Smart Security</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_6.jpg" alt="">
                                                        <p>Smart Night Vision</p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">LOCKING MODES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/locking/icon_1.jpg" alt="">
                                                    <p>Privacy Locking</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_2.jpg" alt="">
                                                    <p>Manual Locking</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">OPERATIONAL FEATURES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/oprational/icon_1.jpg" alt="">
                                                    <p>Panic Exit </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/oprational/icon_2.jpg" alt="">
                                                    <p>Audit Trails<br>
                                                        <div style="font-size: 10px;">(Up to 30,000 records can be stored)</div>
                                                    </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/oprational/icon_3.jpg" alt="">
                                                    <p>Non-handed Operation</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">USER ACCESS RIGHTS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/userAccess/icon_1.jpg" alt="">
                                                    <p>Super Admin </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_4.jpg" alt="">
                                                    <p>Admin</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_3.jpg" alt="">
                                                    <p>User (upto 100 user)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_2.jpg" alt="">
                                                    <p>Guest</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">ALARMS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/alarms/icon_1.jpg" alt="">
                                                    <p>Low Battery </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_2.jpg" alt="">
                                                    <p>Break-in</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_3.jpg" alt="">
                                                    <p>High Temperature</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Battery:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_1.jpg" alt="">
                                                    <p>Lithium (8,000 cycles, onced full charged)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/battery/icon_2.jpg" alt="">
                                                    <p>Jump start (9V)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">DOOR THICKNESS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_1.jpg" alt="">
                                                    <p>40-50mm, 50-65mm</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_2.jpg" alt="">
                                                    <p>65-90mm, 90-100mm*</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 clear">
                        <ul class="lockingguide">
                            <li>
                                <p class="lockingBTN">Locking Guide</p>
                            </li>
                            <li>
                                <div class="turnLeve">
                                    <img src="img/icon/dt.png" alt="">
                                    <p>Turn lever handle in upward direction after closing the door</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>
						    </div><!--col-md-12-->	
						  </div>
                          <div id="re-al" class="tab-pane fade">
						    <div class="col-md-12 no-padding">
						    	<div class="ro clear">
                                    <div class="iconwrpSec">
                        <div class="col-lg-12 row">
                            <div class="subHeading">Mnemonic Guide: <span>RE-AL</span></div>
                        </div>

                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">ACCESS MODES:</h4>
                                            <ul>
                                                <li>
                                                    <img src="img/icon/access/icon_1.jpg" alt="">
                                                    <p>Capacitive Fingerprint (upto 100 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_2.jpg" alt="">
                                                    <p>Key-pad or Password (1 master access, upto 3 unique user accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_3.jpg" alt="">
                                                    <p>RFID (upto 100 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_4.jpg" alt="">
                                                    <p>Mechanical Key (upto 2 keys)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_6.jpg" alt="">
                                                    <p>Remote Control Module (can be purchased separately)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">SMART TECHNOLOGIES: </h4>
                                            <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_1.jpg" alt="">
                                                        <p>Smart Password </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_2.jpg" alt="">
                                                        <p>Smart Voice</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_3.jpg" alt="">
                                                        <p>Smart Etiquettes </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_4.jpg" alt="">
                                                        <p>Smart Security</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_5.jpg" alt="">
                                                        <p>Smart Freeze </p>
                                                    </li>

                                                </ul>
                                            </div>
                                            <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_8.jpg" alt="">
                                                        <p>Smart Video Phone</p>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">LOCKING MODES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/locking/icon_3.jpg" alt="">
                                                    <p>Auto Locking </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_2.jpg" alt="">
                                                    <p>Manual Locking</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_4.jpg" alt="">
                                                    <p>Privacy Locking </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_5.jpg" alt="">
                                                    <p>Defense Locking</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">OPERATIONAL FEATURES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/oprational/icon_1.jpg" alt="">
                                                    <p>Panic Exit </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/oprational/icon_3.jpg" alt="">
                                                    <p>Non-handed Operation</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">USER ACCESS RIGHTS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/userAccess/icon_3.jpg" alt="">
                                                    <p>User</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_4.jpg" alt="">
                                                    <p>Admin</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">ALARMS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/alarms/icon_1.jpg" alt="">
                                                    <p>Low Battery </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_4.jpg" alt="">
                                                    <p>Door not Locked</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_3.jpg" alt="">
                                                    <p>High Temperature</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_2.jpg" alt="">
                                                    <p>Break-in</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Battery:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_1.jpg" alt="">
                                                    <p>AA (3,000 cycles - 8 batteries) </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/battery/icon_2.jpg" alt="">
                                                    <p>Jump start (9V)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">DOOR THICKNESS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_1.jpg" alt="">
                                                    <p>40-50mm</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_2.jpg" alt="">
                                                    <p>50-110mm*</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                                     <%-- <div class="col-lg-12 clear">
                        <ul class="lockingguide">
                            <li>
                                <p class="lockingBTN">Locking Guide</p>
                            </li>
                            <li>
                                <div class="turnLeve">
                                    <img src="img/icon/dt.png" alt="">
                                    <p>Turn lever handle in upward direction after closing the door</p>
                                </div>
                            </li>
                        </ul>
                    </div>--%>

                                </div>
						    </div><!--col-md-12-->	
						  </div>
						  <div id="re-design" class="tab-pane fade">
						    <div class="col-md-12 no-padding">
						    	<div class="ro clear">
                                    <div class="iconwrpSec">
                        <div class="col-lg-12 row">
                            <div class="subHeading">Mnemonic Guide: <span>RE-Design</span></div>
                        </div>

                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">ACCESS MODES:</h4>
                                            <ul>
                                                <li><img src="img/icon/smartTech/icon_7.jpg" alt=""><p>Capacitive Fingerprint (upto 100 unique accesses)</p></li>
									<li><img src="img/icon/access/icon_2.jpg" alt=""><p>Key-pad or Password (1 master access, upto 20 unique user accesses) </p></li>
                                    <li><img src="img/icon/access/icon_3.jpg" alt=""><p>RFID (upto 100 unique accesses)</p></li>
                                       
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">SMART TECHNOLOGIES: </h4>
                                            <%--<div class="col-lg-6 col-sm-6 PR_specifcation">--%>
                                                <ul class="clear ">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_1.jpg" alt="">
                                                        <p>Smart Password </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_3.jpg" alt="">
                                                        <p>Smart Etiquettes </p>
                                                    </li>
                                                     <li>
                                                        <img src="img/icon/smartTech/icon_2.jpg" alt="">
                                                        <p>Smart Voice</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_5.jpg" alt="">
                                                        <p>Smart Freeze </p>
                                                    </li>

                                                </ul>
                                            <%--</div>--%>
                                           <%-- <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_4.jpg" alt="">
                                                        <p>Smart Security</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_6.jpg" alt="">
                                                        <p>Smart Night Vision</p>
                                                    </li>
                                                </ul>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">LOCKING MODES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/locking/icon_3.jpg" alt="">
                                                    <p>Auto Locking </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_1.jpg" alt="">
                                                    <p>Privacy Locking</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_2.jpg" alt="">
                                                    <p>Manual Locking</p>
                                                </li>

                                                <li>
                                                    <img src="img/icon/locking/icon_5.jpg" alt="">
                                                    <p>Defense Locking</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">OPERATIONAL FEATURES:</h4>
                                            <ul class="clear">
                                                <%--<li>
                                                    <img src="img/icon/oprational/icon_1.jpg" alt="">
                                                    <p>Panic Exit </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/oprational/icon_2.jpg" alt="">
                                                    <p>Audit Trails<br>
                                                        <div style="font-size: 10px;">(Up to 30,000 records can be stored)</div>
                                                    </p>
                                                </li>--%>
                                                <li>
                                                    <img src="img/icon/oprational/icon_3.jpg" alt="">
                                                    <p>Non-handed Operation</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">USER ACCESS RIGHTS:</h4>
                                            <ul class="clear">
                                                <%--<li>
                                                    <img src="img/icon/userAccess/icon_1.jpg" alt="">
                                                    <p>Super Admin </p>
                                                </li>--%>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_3.jpg" alt="">
                                                    <p>User</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_4.jpg" alt="">
                                                    <p>Admin</p>
                                                </li>
                                                
                                                <%--<li>
                                                    <img src="img/icon/userAccess/icon_2.jpg" alt="">
                                                    <p>Guest</p>
                                                </li>--%>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">ALARMS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/alarms/icon_1.jpg" alt="">
                                                    <p>Low Battery </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_2.jpg" alt="">
                                                    <p>Break-in</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_3.jpg" alt="">
                                                    <p>High Temperature</p>
                                                </li>
                                                    <%--<li>
                                                        <img src="img/icon/alarms/icon_4.jpg" alt="">
                                                        <p>Door not Locked</p>
                                                    </li>--%>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Battery:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_3.jpg" alt="">
                                                    <p>AA (4 batteries) </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/battery/icon_2.jpg" alt="">
                                                    <p>Jump start (Power Bank 5V/Battery 9V)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">DOOR THICKNESS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_1.jpg" alt="">
                                                    <p>30-50mm</p>
                                                </li>
                                                <%--<li>
                                                    <img src="img/icon/doorThickness/icon_2.jpg" alt="">
                                                    <p>65-90mm, 90-100mm*</p>
                                                </li>--%>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Finish:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_1.jpg" alt="">
                                                    <p>Black & Chrome </p>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>


                    </div>
                                </div>
						    </div><!--col-md-12-->	
						  </div>
						  <div id="re-tro" class="tab-pane fade">
						    <div class="col-md-12 no-padding">
						    	<div class="ro clear">
                                    <div class="iconwrpSec">
                        <div class="col-lg-12 row">
                            <div class="subHeading">Mnemonic Guide: <span>RE-tro</span></div>
                        </div>
                            
                            <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation">
                                            <h4 class="icon_head">ACCESS MODES:</h4>
                                            <ul>
                                               <li>
                                                    <img src="img/icon/access/icon_2.jpg" alt="">
                                                    <p>Key-pad or Password (upto 3 unique accesses)</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/access/icon_3.jpg" alt="">
                                                    <p>RFID (upto 100 unique accesses)</p>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">SMART TECHNOLOGIES: </h4>
                                            <%--<div class="col-lg-6 col-sm-6 PR_specifcation">--%>
                                                <ul class="clear ">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_1.jpg" alt="">
                                                        <p>Smart Password </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_3.jpg" alt="">
                                                        <p>Smart Etiquettes </p>
                                                    </li>
                                                     <li>
                                                        <img src="img/icon/smartTech/icon_2.jpg" alt="">
                                                        <p>Smart Voice</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_5.jpg" alt="">
                                                        <p>Smart Freeze </p>
                                                    </li>

                                                </ul>
                                            <%--</div>--%>
                                           <%-- <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_4.jpg" alt="">
                                                        <p>Smart Security</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_6.jpg" alt="">
                                                        <p>Smart Night Vision</p>
                                                    </li>
                                                </ul>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">LOCKING MODES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/locking/icon_3.jpg" alt="">
                                                    <p>Auto Locking </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_1.jpg" alt="">
                                                    <p>Privacy Locking</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_2.jpg" alt="">
                                                    <p>Manual Locking</p>
                                                </li>

                                                <li>
                                                    <img src="img/icon/locking/icon_5.jpg" alt="">
                                                    <p>Defense Locking</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">OPERATIONAL FEATURES:</h4>
                                            <ul class="clear">
                                                <%--<li>
                                                    <img src="img/icon/oprational/icon_1.jpg" alt="">
                                                    <p>Panic Exit </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/oprational/icon_2.jpg" alt="">
                                                    <p>Audit Trails<br>
                                                        <div style="font-size: 10px;">(Up to 30,000 records can be stored)</div>
                                                    </p>
                                                </li>--%>
                                                <li>
                                                    <img src="img/icon/oprational/icon_3.jpg" alt="">
                                                    <p>Non-handed Operation</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">USER ACCESS RIGHTS:</h4>
                                            <ul class="clear">
                                                <%--<li>
                                                    <img src="img/icon/userAccess/icon_1.jpg" alt="">
                                                    <p>Super Admin </p>
                                                </li>--%>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_3.jpg" alt="">
                                                    <p>User</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_4.jpg" alt="">
                                                    <p>Admin</p>
                                                </li>
                                                
                                                <%--<li>
                                                    <img src="img/icon/userAccess/icon_2.jpg" alt="">
                                                    <p>Guest</p>
                                                </li>--%>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">ALARMS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/alarms/icon_1.jpg" alt="">
                                                    <p>Low Battery </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_2.jpg" alt="">
                                                    <p>Break-in</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_3.jpg" alt="">
                                                    <p>High Temperature</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_4.jpg" alt="">
                                                    <p>Door not Locked</p>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Battery:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_3.jpg" alt="">
                                                    <p>AA (4 batteries) </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/battery/icon_4.jpg" alt="">
                                                    <p>Jump start (9V)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">DOOR THICKNESS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_1.jpg" alt="">
                                                    <p>10-12 mm</p>
                                                </li>
                                                <%--<li>
                                                    <img src="img/icon/doorThickness/icon_2.jpg" alt="">
                                                    <p>65-90mm, 90-100mm*</p>
                                                </li>--%>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Finish:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_1.jpg" alt="">
                                                    <p>Black & Chrome </p>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>


                    </div>
                                </div>
						    </div><!--col-md-12-->	
						  </div>
						  <div id="re-mote" class="tab-pane fade">
						    <div class="col-md-12 no-padding">
						    	<div class="ro clear">
                                    <div class="iconwrpSec">
                        <div class="col-lg-12 row">
                            <div class="subHeading">Mnemonic Guide: <span>RE-mote</span></div>
                        </div>

                        
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">ACCESS MODES:</h4>
                                            <ul class="clear">
                                              <li><img src="img/icon/access/icon_2.jpg" alt=""><p>Key-pad or Password </p></li>
                                    <li><img src="img/icon/access/icon_3.jpg" alt=""><p>RFID (upto 100 unique accesses)</p></li>
                                       <li><img src="img/icon/smartTech/icon_4.jpg" alt=""><p>Mechanical Key (upto 2 keys)</p></li>
                                       <li><img src="img/icon/smartTech/icon_6.jpg" alt=""><p>Bluetooth Enabled Key (Mobile App Access)</p></li>
                                    
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">SMART TECHNOLOGIES: </h4>
                                            <%--<div class="col-lg-6 col-sm-6 PR_specifcation">--%>
                                                <ul class="clear ">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_1.jpg" alt="">
                                                        <p>Smart Password </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_3.jpg" alt="">
                                                        <p>Smart Etiquettes </p>
                                                    </li>
                                                     <li>
                                                        <img src="img/icon/smartTech/icon_2.jpg" alt="">
                                                        <p>Smart Voice</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_5.jpg" alt="">
                                                        <p>Smart Freeze </p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_8.jpg" alt="">
                                                        <p>Smart Video Phone**</p>
                                                    </li>

                                                </ul>
                                            <%--</div>--%>
                                           <%-- <div class="col-lg-6 col-sm-6 PR_specifcation">
                                                <ul class="clear row">
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_4.jpg" alt="">
                                                        <p>Smart Security</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/icon/smartTech/icon_6.jpg" alt="">
                                                        <p>Smart Night Vision</p>
                                                    </li>
                                                </ul>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">LOCKING MODES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/locking/icon_3.jpg" alt="">
                                                    <p>Auto Locking </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_1.jpg" alt="">
                                                    <p>Privacy Locking</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/locking/icon_2.jpg" alt="">
                                                    <p>Manual Locking</p>
                                                </li>

                                                <li>
                                                    <img src="img/icon/locking/icon_5.jpg" alt="">
                                                    <p>Defense Locking</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">OPERATIONAL FEATURES:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/oprational/icon_1.jpg" alt="">
                                                    <p>Panic Exit </p>
                                                </li>
                                         
                                                <li>
                                                    <img src="img/icon/oprational/icon_3.jpg" alt="">
                                                    <p>Non-handed Operation</p>
                                                </li>
                                                       <li>
                                                    <img src="img/icon/oprational/icon_7.jpg" alt="">
                                                    <p>Audit TrailsOffline Lock 
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">USER ACCESS RIGHTS:</h4>
                                            <ul class="clear">
                                                <%--<li>
                                                    <img src="img/icon/userAccess/icon_1.jpg" alt="">
                                                    <p>Super Admin </p>
                                                </li>--%>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_3.jpg" alt="">
                                                    <p>User</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/userAccess/icon_4.jpg" alt="">
                                                    <p>Admin</p>
                                                </li>
                                                
                                                <li>
                                                    <img src="img/icon/userAccess/icon_2.jpg" alt="">
                                                    <p>Guest</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">ALARMS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/alarms/icon_1.jpg" alt="">
                                                    <p>Low Battery </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_2.jpg" alt="">
                                                    <p>Break-in</p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/alarms/icon_3.jpg" alt="">
                                                    <p>High Temperature</p>
                                                </li>
                                                    <li>
                                                        <img src="img/icon/alarms/icon_4.jpg" alt="">
                                                        <p>Door not Locked</p>
                                                    </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Battery:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_3.jpg" alt="">
                                                    <p>AA (8 batteries) </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/battery/icon_2.jpg" alt="">
                                                    <p>Jump start (9V)</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">DOOR THICKNESS:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/doorThickness/icon_1.jpg" alt="">
                                                    <p>40-90mm</p>
                                                </li>
                                                <%--<li>
                                                    <img src="img/icon/doorThickness/icon_2.jpg" alt="">
                                                    <p>65-90mm, 90-100mm*</p>
                                                </li>--%>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--<div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Finish:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/battery/icon_1.jpg" alt="">
                                                    <p>Black & Chrome </p>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>--%>
                        <div class="row">
                            <div class="RowWrp">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="iconWrp">
                                        <div class="PR_specifcation boxIN">
                                            <h4 class="icon_head">Types of password:</h4>
                                            <ul class="clear">
                                                <li>
                                                    <img src="img/icon/password/icon-1.png" alt="">
                                                    <p>OTP Password  </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/password/icon-2.png" alt="">
                                                    <p>Bluetooth Key Sharing </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/password/icon-3.png" alt="">
                                                    <p>Period Password </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/password/icon-4.png" alt="">
                                                    <p>Schedule Password </p>
                                                </li>
                                                <li>
                                                    <img src="img/icon/password/icon-5.png" alt="">
                                                    <p>Permanent Password </p>
                                                </li>

                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>


                    </div>
                                </div>
						    </div><!--col-md-12-->	
						  </div>
						
						
						</div>
					</div><!--video-tab-box-->
				</div>
			</div>
		</div>
	</div>
</div><!--video-container-->	


</asp:Content>

