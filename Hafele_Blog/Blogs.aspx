﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="Blogs.aspx.cs" Inherits="Blogs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/fav-icon.png" type="image/x-icon">
    <link rel="stylesheet" href="/Content/Blogs/css/bootstrap.min.css">
    <link rel="stylesheet" href="/Content/Blogs/css/mystyle.css">
    <link rel="stylesheet" type="text/css" href="/Content/Blogs/css/slick.css">
    <link rel="stylesheet" type="text/css" href="/Content/Blogs/css/aos.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <header></header>
    <div class="content_wrapper">
        <section class="mainContent">

            <div class="container mb-5">
                <div class="row">
                    <div class=" col-md-7" data-aos="fade-up">
                        <div class="bannerSlider">
                            <%--<div class="position-relative">
                	<div class="bannerPic"><img src="/Content/Blogs/images/banner/1.jpg" class="img-fluid"></div>
                    <div class="bannerText">
                    	<a href="#">
                        <h1 class="animated " data-animation-in="fadeInLeft">Lorem ipsum dummy Hafele Blog </h1>
                        <p class="animated " data-animation-in="fadeInLeft">Consectetur adipiscing elitsed deiusmo </p>
                        </a>
                    </div>
                </div>
                <div class="position-relative">
                	<div class="bannerPic"><img src="/Content/Blogs/images/banner/2.jpg" class="img-fluid"></div>
                    <div class="bannerText">
                    	<a href="#">
                        <h1 class="animated " data-animation-in="fadeInLeft">Lorem ipsum dummy Hafele Blog </h1>
                        <p class="animated " data-animation-in="fadeInLeft">Consectetur adipiscing elitsed deiusmo </p>
                        </a>
                    </div>
                </div>--%>
                            <asp:Literal ID="ltrFeatureBlogs" runat="server"></asp:Literal>
                        </div>

                    </div>

                    <div class=" col-md-5" data-aos="fade-up">
                        <%--<div class="blogBox">
            	<img src="/Content/Blogs/images/1.jpg" class="img-fluid mb-4">
                <div class="blogBrief">
                	<span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                </div>
                <a href="#">
                <h1>Lorem ipsum dummy Hafele Blog </h1>
                <p>Consectetur adipiscing elitseding</p>
                </a>
            </div>	--%>
                        <asp:Literal ID="ltrFeatureBlogsTop1" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>


       <%--     <div class="container mb-5 postwrap">
                <h1 class="title mb-4" data-aos="zoom-in">Latest <span>posts</span></h1>

                <div class="postSlider pb-4">

                    <asp:Literal ID="ltrLatest" runat="server"></asp:Literal>

                   
                </div>
            </div>--%>


            <div class="container postwrap">
<h1 class="title mb-4" data-aos="zoom-in">Latest <span>posts</span></h1>
	<div class="row ">
                	
            <div class="col-12 awardWrap">
            <div class="slider-nav mb-5">
               <%-- <div class="blogCateogry">Appliance Care</div>
                <div class="blogCateogry">Culinary Corner </div>
                <div class="blogCateogry">Kitchen Trends</div>
                <div class="blogCateogry">Tips and Tricks</div>
                <div class="blogCateogry">Ultimate Kitchen Guide </div>--%>

                <asp:Literal ID="ltrLatestCategory" runat="server"></asp:Literal>
                
            </div>
        
            <div class="slider-for mb-5 ">

                 <asp:Literal ID="ltrLatest" runat="server"></asp:Literal>
         <%--       
                <!-- tab 1 Bangaluru -->
            
                <div>
                    <div class="postSlider pb-4">
                        <div class="pr-5">
                            <div class="blogBox">
                                <a href="#">
                                <div class="blogPic mb-4"><img src="images/post/1.jpg"></div>
                                <div class="blogBrief">
                                    <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                </div>
                                <h1>Lorem ipsum dummy Hafele Blog </h1>
                                <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </div>
                        </div>
                        
                        <div class="pr-5">
                            <div class="blogBox">
                                <a href="#">
                                <div class="blogPic mb-4"><img src="images/post/2.jpg"></div>
                                <div class="blogBrief">
                                    <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                </div>
                                <h1>Lorem ipsum dummy Hafele Blog </h1>
                                <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </div>
                        </div>
                        
                        <div class="pr-5">
                            <div class="blogBox">
                                <a href="#">
                                <div class="blogPic mb-4"><img src="images/post/3.jpg"></div>
                                <div class="blogBrief">
                                    <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                </div>
                                <h1>Lorem ipsum dummy Hafele Blog </h1>
                                <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </div>
                        </div>
                        
                        <div class="pr-5">
                            <div class="blogBox">
                                <a href="#">
                                <div class="blogPic mb-4"><img src="images/post/4.jpg"></div>
                                <div class="blogBrief">
                                    <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                </div>
                                <h1>Lorem ipsum dummy Hafele Blog </h1>
                                <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- tab 2 Kolhapur -->
            
                <div>
                    <div class="postSlider pb-4">
                        
                        <div class="pr-5">
                            <div class="blogBox">
                                <a href="#">
                                <div class="blogPic mb-4"><img src="images/post/2.jpg"></div>
                                <div class="blogBrief">
                                    <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                </div>
                                <h1>Lorem ipsum dummy Hafele Blog </h1>
                                <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <!-- tab 3 Lucknow -->
            
                <div>
                    <div class="postSlider pb-4">
                       
                        <div class="pr-5">
                            <div class="blogBox">
                                <a href="#">
                                <div class="blogPic mb-4"><img src="images/post/3.jpg"></div>
                                <div class="blogBrief">
                                    <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                </div>
                                <h1>Lorem ipsum dummy Hafele Blog </h1>
                                <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </div>
                        </div>
                        
                        <div class="pr-5">
                            <div class="blogBox">
                                <a href="#">
                                <div class="blogPic mb-4"><img src="images/post/4.jpg"></div>
                                <div class="blogBrief">
                                    <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                </div>
                                <h1>Lorem ipsum dummy Hafele Blog </h1>
                                <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- tab 4 Vadodara -->
            
                <div>
                    <div class="postSlider pb-4">
                        <div class="pr-5">
                            <div class="blogBox">
                                <a href="#">
                                <div class="blogPic mb-4"><img src="images/post/1.jpg"></div>
                                <div class="blogBrief">
                                    <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                </div>
                                <h1>Lorem ipsum dummy Hafele Blog </h1>
                                <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </div>
                        </div>
                        
                        <div class="pr-5">
                            <div class="blogBox">
                                <a href="#">
                                <div class="blogPic mb-4"><img src="images/post/2.jpg"></div>
                                <div class="blogBrief">
                                    <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                </div>
                                <h1>Lorem ipsum dummy Hafele Blog </h1>
                                <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </div>
                        </div>
                        
                        <div class="pr-5">
                            <div class="blogBox">
                                <a href="#">
                                <div class="blogPic mb-4"><img src="images/post/3.jpg"></div>
                                <div class="blogBrief">
                                    <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                </div>
                                <h1>Lorem ipsum dummy Hafele Blog </h1>
                                <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                
                <!-- tab 4 Vadodara -->
            
                <div>
                    <div class="postSlider pb-4">
                        <div class="pr-5">
                            <div class="blogBox">
                                <a href="#">
                                <div class="blogPic mb-4"><img src="images/post/1.jpg"></div>
                                <div class="blogBrief">
                                    <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                </div>
                                <h1>Lorem ipsum dummy Hafele Blog </h1>
                                <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </div>
                        </div>
                        
                        <div class="pr-5">
                            <div class="blogBox">
                                <a href="#">
                                <div class="blogPic mb-4"><img src="images/post/2.jpg"></div>
                                <div class="blogBrief">
                                    <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                </div>
                                <h1>Lorem ipsum dummy Hafele Blog </h1>
                                <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
               
                --%>
                
            </div>
            
        </div>
        
        </div>
</div>

            <div class="container mb-5 dashedBorder px-lg-0 ">
                <div class="row  pb-5">
                 <%--   <div class="col-md-8">
                        <h1 class="title mb-4" data-aos="zoom-in">Populer <span>posts</span></h1>

                        <div class="row">--%>
                            <%--<div class="col-md-6" data-aos="fade-up" data-aos-delay="200">
            		<div class="blogBox">
                        <a href="#">
                        <div class="blogPic mb-4"><img src="/Content/Blogs/images/post/3.jpg" class=""></div>
                       
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
                        <p>Consectetur adipiscing elitseding</p>
                        </a>
                    </div>
        		</div>
        		
                <div class="col-md-6" data-aos="fade-up" data-aos-delay="200">
            		<div class="blogBox">
                        <a href="#">
                        <div class="blogPic mb-4"><img src="/Content/Blogs/images/post/3.jpg" class=""></div>
                       
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
                        <p>Consectetur adipiscing elitseding</p>
                        </a>
                    </div>
        		</div>--%>
                            <asp:Literal ID="ltrTopTwoPopular" runat="server"></asp:Literal>
                    <%--    </div>
                    </div>

                    <div class="col-md-4">
                        <h1 class="title mb-4" data-aos="zoom-in">Post <span>list</span></h1>

                        <ul class="postList">
                            <li data-aos="fade-up" data-aos-delay="200">
                                <a href="#">
                                    <div class="blogBrief">
                                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                    </div>
                                    <h1>Lorem ipsum dummy Hafele Blog </h1>
                                    <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </li>

                            <li data-aos="fade-up" data-aos-delay="300">
                                <a href="#">
                                    <div class="blogBrief">
                                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                    </div>
                                    <h1>Lorem ipsum dummy Hafele Blog </h1>
                                    <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </li>

                            <li data-aos="fade-up" data-aos-delay="400">
                                <a href="#">
                                    <div class="blogBrief">
                                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                                    </div>
                                    <h1>Lorem ipsum dummy Hafele Blog </h1>
                                    <p>Consectetur adipiscing elitseding</p>
                                </a>
                            </li>
                        </ul>
                    </div>--%>
                </div>
            </div>


            <div class="container mb-5 postwrap dashedBorder px-lg-0">
                <h1 class="title mb-4 text-center" data-aos="zoom-in">Must Read <span>articles</span></h1>

                <div class="articleSlider mb-5 pb-4">
                    <%--  <div>
            <a href="#"  class="articleBox d-flex h-100">
                <div class="align-self-center text-center w-100">  	
                    <div class="blogBrief">
                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                    </div>
                    <h1>Lorem ipsum dummy Hafele Blog  </h1>
                </div>
            </a>
        </div>
        
        <div>
            <a href="#"  class="articleBox d-flex h-100">
                <div class="align-self-center text-center w-100">  	
                    <div class="blogBrief">
                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                    </div>
                    <h1>Lorem ipsum dummy Hafele Blog  </h1>
                </div>
            </a>
        </div>
        
        <div>
            <a href="#"  class="articleBox d-flex h-100">
                <div class="align-self-center text-center w-100">  	
                    <div class="blogBrief">
                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                    </div>
                    <h1>Lorem ipsum dummy Hafele Blog  </h1>
                </div>
            </a>
        </div>
        
        <div>
            <a href="#"  class="articleBox d-flex h-100">
                <div class="align-self-center text-center w-100">  	
                    <div class="blogBrief">
                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                    </div>
                    <h1>Lorem ipsum dummy Hafele Blog  </h1>
                </div>
            </a>
        </div>
                    --%>
                    <asp:Literal ID="ltrMustRead" runat="server"></asp:Literal>
                </div>
            </div>

            <div class="container mb-5 px-lg-0">
                <div class="row">
                    <div class="col-lg-4 mb-4 mb-md-5 order-1 order-lg-0" data-aos="fade-up" data-aos-delay="200">
                        <div class="newletterBox">
                            <h2>Subscribe for Newsletter </h2>
                            <p>Phasellus porttitor sapien a purus venenatis</p>

              <%--               <form class="form-group fadeAnim">
                	<input type="email" class="form-control rounded-0" placeholder="Enter Your Email">
                    <input type="submit" value="Subscribe Now">
                </form>--%>
                            <div class="form-group fadeAnim">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control rounded-0" placeholder="Enter Your Email" autocomplete="off" ></asp:TextBox>
                              <span id="ErrorEmail1" class="ErrorValidation">Email is required.</span>
                               <span id="ErrorEmail2" class="ErrorValidation">Invalid email address.</span>
                                <asp:Label ID="lblStatus"  runat="server" Style="color:#dc3545;"></asp:Label>  
                                <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" OnClientClick="javascript:return validation();" type="submit" Text="Subscribe Now" value="Subscribe Now" runat="server" />
                            </div>
                        </div>
                    </div>

                    <%--   <div class="col-md-6 col-lg-4  mb-4 mb-md-5 " data-aos="fade-up" data-aos-delay="400">
        	<div class="position-relative bottomBlogBox">
                <div class="bottomBlogPic"><img src="/Content/Blogs/images/banner/1.jpg" class="img-fluid"></div>
                <div class="bottomBlogText">
                    <a href="#">
                    <h1 class="animated " data-animation-in="fadeInLeft">Hafele Blog </h1>
                    <p class="animated " data-animation-in="fadeInLeft">Consectetur adipiscing elitsed deiusmo </p>
                    </a>
                </div>
            </div>
        </div>
        
        <div class="col-md-6 col-lg-4 mb-4 mb-md-5" data-aos="fade-up" data-aos-delay="600">
        	<div class="position-relative bottomBlogBox">
                <div class="bottomBlogPic"><img src="/Content/Blogs/images/banner/1.jpg" class="img-fluid"></div>
                <div class="bottomBlogText">
                    <a href="#">
                    <h1 class="animated " data-animation-in="fadeInLeft">Hafele Blog </h1>
                    <p class="animated " data-animation-in="fadeInLeft">Consectetur adipiscing elitsed deiusmo </p>
                    </a>
                </div>
            </div>
        </div>--%>

                    <asp:Literal ID="ltrBottomBlogs" runat="server"></asp:Literal>



                </div>
            </div>



        </section>
    </div>

    <style>
        .ErrorValidation {

        display: none;
        color: #dc3545!important;
       margin-bottom: 10px;
      
    }
    </style>

    <script src="/Content/Blogs/js/jquery-3.5.1.min.js"></script>
    <script src="/Content/Blogs/js/bootstrap.js"></script>
    <script src="/Content/Blogs/js/aos.js"></script>
    <script src="/Content/Blogs/js/common.js"></script>
    <script src="/Content/Blogs/js/slick.min.js"></script>



    <script>

        // banner slider //
        $('.bannerSlider').slick({ autoplay: true, speed: 1000, autoplaySpeed: 3000, pauseOnHover: false, lazyLoad: 'progressive', arrows: true, dots: false, fade: true, }).slickAnimation();

        // post slider //
        $(".postSlider").slick({ slidesToShow: 3, slidesToScroll: 1, autoplay: !1, arrows: !1, dots: !0, infinite: !0, autoplaySpeed: 2e3, responsive: [{ breakpoint: 991, settings: { slidesToShow: 2, arrows: !1, dots: !0, slidesToScroll: 1 } }, { breakpoint: 600, settings: { slidesToShow: 1, arrows: !1, dots: !0, slidesToScroll: 1, adaptiveHeight: !0 } }] });

        // article slider //
        $(".articleSlider").slick({ slidesToShow: 3, slidesToScroll: 1, autoplay: !1, arrows: !1, dots: !0, infinite: !0, autoplaySpeed: 2e3, responsive: [{ breakpoint: 991, settings: { slidesToShow: 2, arrows: !1, dots: !0, slidesToScroll: 1 } }, { breakpoint: 600, settings: { slidesToShow: 1, arrows: !1, dots: !0, slidesToScroll: 1, adaptiveHeight: !0 } }] });

 $('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  //asNavFor: '.slider-nav',
  asNavFor: '.postSlider',
  swipe: false 
 
 });

$('.slider-nav').slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: false,
  centerMode: false,
  focusOnSelect: true,
  
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 7,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
		dots: false,
		arrows:true,
      }
    },
    {
      breakpoint: 550,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
		dots: false,
		arrows:true,
		
      }
    }
  ]
  
});

                function validation() {
            var Email = $('#<%=txtEmail.ClientID%>').val();
            var blank = false;


            if (Email == '') {
                $('#<%=lblStatus.ClientID%>').css('display', 'none');
                $('#ErrorEmail2').css('display', 'none');
                $('#ErrorEmail1').css('display', 'block');
                blank = true;
            }
            else {
                var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText.test(Email)) {
                    $('#<%=lblStatus.ClientID%>').css('display', 'none');
                    $('#ErrorEmail1').css('display', 'none');
                    $('#ErrorEmail2').css('display', 'block');
                    blank = true;
                }
                else {
                    $('#ErrorEmail2').css('display', 'none');


                }
                $('#ErrorEmail1').css('display', 'none');
              $('#<%=lblStatus.ClientID%>').css('display', 'none');

            }
     
         if (blank) {
                return false;
            }
            else {
                return true;
            }
                }
   
        </script>


    <%--<script type="text/javascript">  

          function UserOrEmailAvailability() { //This function call on text change.             
            $.ajax({  
                type: "POST",  
                url: "Blogs.aspx/CheckEmail", // this for calling the web method function in cs code.  
                data: '{useroremail: "' + $("#<%=txtEmail.ClientID%>")[0].value + '" }',// user name or email value  
                contentType: "application/json; charset=utf-8",  
                dataType: "json",  
                success: OnSuccess,  
                failure: function (response) {  
                    alert(response);  
                }  
            });  
        }  
  
        // function OnSuccess  
        function OnSuccess(response) {   
            switch (response.d) {  
                case "true":

                    $('#ErrorEmail3').css('display', 'block');
                        $('#ErrorEmail2').css('display', 'none');
                        $('#ErrorEmail1').css('display', 'none');
                 

                    if (blank) {
                        return false;
                    }
                    else {
                        return true;
                    }
                    break;  

            }  
        }  
  
    </script> --%>


</asp:Content>

