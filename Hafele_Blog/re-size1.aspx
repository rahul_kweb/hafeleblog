﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="re-size.aspx.cs" Inherits="re_size" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Hafele</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="img/nmims.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/style.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- content sec -->
		<div class="content_wrapper">
			
			<!-- banner sec -->
			<section class="bannerForm_Sec">
				<div class="">
					<div class="mobileLogo">
						<a href="index.aspx"><img src="img/logo.png" alt=""></a>
					</div>					
					<div class="bannerIn">									
						<div id="bannerBig" class="owl-carousel owl-theme">          
							<div class="item">											
								<img src="img/banner_size.jpg" alt="">
							</div>						
						</div>
						<div class="banner_logo"><img src="img/logo_1.png" alt=""></div>
					</div>						
				</div>
				
			</section>
			<!-- // banner sec -->
			<!-- Pr Detail sec -->
			<section class="PrDetail_sec">
				<div class="container">
				
					<div class="row clear">
						<div class="col-lg-5 col-sm-5">
							<div class="prImg">
								<img src="img/prImg/re-size.png" alt="">
							</div>
						</div>
						<div class="col-lg-7 col-sm-7">
							<div class="PrDeas">
								<h2>RE-Size</h2>
								<h5>Because your fashionable door needs its ‘right’ match…</h5>
								<figcaption>For the Art-aspirants who use the inﬂuences of their right brain to look for the ideal balance in aesthetics and functionality…</figcaption>
								<p><span>‘Peace’ for the mind; ‘Win’ for the eye:</span> Häfele’s RE-Size Digital Lock uniquely combines incomparable technology with an aesthetic design, bringing the much desired ornamentation to your door while also securing your home with planned access. Owing to its singular design, RE-Size allows you to combine its existence with any preferred lever handle as per your choice. This brings in a whiff of customization to your approach while designing the security needs for your door- you may not otherwise ﬁnd such ﬂexibility in the market. The exclusivity of Hafele’s RE-Size lies in its potential to jump-start operations through an integrated power bank which can be easily charged through a USB Cable. With 4 different access modes – Finger Print, Key Pad, RFID and Mechanical Key – Häfele’s RE-Size Digital Lock brings you optimum security.</p>
							</div>
						</div>
					</div>
					
					<div class="ro clear">
						<div class="iconwrpSec">
							<div class="col-lg-12 row">
								<div class="subHeading">Mnemonic Guide: <span>RE-Size</span></div>
							</div>
							
							<div class="row">
								<div class="RowWrp">								
									<div class="col-lg-6 col-sm-6">							
										<div class="iconWrp">
											<div class="PR_specifcation">
												<h4 class="icon_head">ACCESS MODES</h4>
												<ul>
													<li><img src="img/icon/access/icon_1.jpg" alt=""> <p>Capacitive Fingerprint (upto 43 unique accesses)</p></li>													
													<li><img src="img/icon/access/icon_2.jpg" alt=""> <p>Key-pad or Password (upto 7 unique accesses)</p></li>
													<li><img src="img/icon/access/icon_3.jpg" alt=""> <p>RFID (upto 43 unique accesses)</p></li>
													<li><img src="img/icon/access/icon_4.jpg" alt=""> <p>Mechanical Key (upto 2 keys)</p></li>													
												</ul>
											</div>
										</div>
									</div>									
									<div class="col-lg-6 col-sm-6">	
										<div class="iconWrp">
											<div class="PR_specifcation">
												<h4 class="icon_head">SMART TECHNOLOGIES: </h4>
												<div class="col-lg-6 col-sm-6 PR_specifcation">
													<ul class="clear row">
														<li><img src="img/icon/smartTech/icon_1.jpg" alt=""> <p>Smart Password </p></li>																											
														<li><img src="img/icon/smartTech/icon_2.jpg" alt=""> <p>Smart Voice</p></li>
														<li><img src="img/icon/smartTech/icon_3.jpg" alt=""> <p>Smart Etiquettes </p></li>		
														<li><img src="img/icon/smartTech/icon_4.jpg" alt=""> <p>Smart Security</p></li>	
													</ul>
												</div>
												<div class="col-lg-6 col-sm-6 PR_specifcation">
													<ul class="clear row">																				
														<li><img src="img/icon/smartTech/icon_5.jpg" alt=""> <p>Smart Freeze </p></li>														
													</ul>
												</div>												
											</div>
										</div>
									</div>									
								</div>
							</div>
							<div class="row">
								<div class="RowWrp">
									<div class="col-lg-6 col-sm-6">	
										<div class="iconWrp">
											<div class="PR_specifcation boxIN">
												<h4 class="icon_head">LOCKING MODES:</h4>
												<ul class="clear">
													<li><img src="img/icon/locking/icon_3.jpg" alt=""> <p>Auto Locking </p></li>
													<li><img src="img/icon/locking/icon_2.jpg" alt=""> <p>Manual Locking</p></li>
													<li><img src="img/icon/locking/icon_1.jpg" alt=""> <p>Privacy Locking </p></li>
													<li><img src="img/icon/locking/icon_5.jpg" alt=""> <p>Defense Locking</p></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-sm-6">	
										<div class="iconWrp">
											<div class="PR_specifcation">
												<h4 class="icon_head">OPERATIONAL FEATURES:</h4>
												<ul class="clear">
													<li><img src="img/icon/oprational/icon_1.jpg" alt=""> <p>Panic Exit </p></li>													
													<li><img src="img/icon/oprational/icon_3.jpg" alt=""> <p>Non-handed Operation</p></li>										
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="RowWrp">
									<div class="col-lg-6 col-sm-6">
										<div class="iconWrp">
											<div class="PR_specifcation boxIN">
												<h4 class="icon_head">USER ACCESS RIGHTS:</h4>
												<ul class="clear">
													<li><img src="img/icon/userAccess/icon_2.jpg" alt=""> <p>Guest</p></li>
													<li><img src="img/icon/userAccess/icon_3.jpg" alt=""> <p>User</p></li>
													<li><img src="img/icon/userAccess/icon_4.jpg" alt=""> <p>Admin</p></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-sm-6">
										<div class="iconWrp">
											<div class="PR_specifcation">
												<h4 class="icon_head">ALARMS:</h4>
												<ul class="clear">
													<li><img src="img/icon/alarms/icon_1.jpg" alt=""> <p>Low Battery </p></li>
													<li><img src="img/icon/alarms/icon_4.jpg" alt=""> <p>Door not Locked</p></li>																						
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="RowWrp">
									<div class="col-lg-6 col-sm-6">
										<div class="iconWrp">
											<div class="PR_specifcation boxIN">
												<h4 class="icon_head">Battery:</h4>
												<ul class="clear">
													<li><img src="img/icon/battery/icon_1.jpg" alt=""> <p>AA (3,000 cycles - 4 batteries) </p></li>
													<li><img src="img/icon/battery/icon_2.jpg" alt=""> <p>Jump start (power bank)</p></li>										
												</ul>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-sm-6">
										<div class="iconWrp">
											<div class="PR_specifcation boxIN">
												<h4 class="icon_head">DOOR THICKNESS:</h4>
												<ul class="clear">
													<li><img src="img/icon/doorThickness/icon_1.jpg" alt=""> <p>40-60mm</p></li>
													<li><img src="img/icon/doorThickness/icon_2.jpg" alt=""> <p>60-90mm, 90-120mm*</p></li>																			
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="RowWrp">
									<div class="col-lg-8 col-sm-8">
										<div class="iconWrp">
											<div class="PR_specifcation boxIN">
												<h4 class="icon_head">Finish:</h4>
												<ul class="clear">
													<li><img src="img/icon/battery/icon_1.jpg" alt=""> <p>Matt Black, Polish Chrome + Matt Black</p></li>													
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>								
						</div>
						<div class="row">
							<div class="col-lg-12 clear">
								<ul class="lockingguide">
									<li><p class="lockingBTN">Lever handle selection guide <span>(to be ordered separately)</span></p></li>								
								</ul>
								<ul class="lockingguide bticon">								
									<li>
										<div class="turnLeve">
											<img src="img/img_2.jpg" alt="">
										</div>
									</li>
									<li>
										<div class="turnLeve">
											<img src="img/img_3.jpg" alt="">
										</div>
									</li>
									<li>
										<div class="turnLeve">
											<img src="img/img_4.jpg" alt="">
										</div>
									</li>
									<li>
										<div class="turnLeve">
											<img src="img/img_5.jpg" alt="">
										</div>
									</li>
								</ul>
							</div>
						</div>
				</div>
			</section>
			<!-- // Pr Detail sec -->	
            
             <!-- Pr PrVideo_sec -->
			<section class="PrVideo_sec">
				<div class="container-fluid">
				    <div class="row">
                        <div class="col-lg-12">
                           <h2>RE-Size <span>Videos</span></h2> 
                            <div class="video_frames yt_videos">
                                <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/3n1pb5Q9SgE?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p>User guide</p>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/PtuYp5zMB7c?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p>Installation video</p>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/fca0DS8YJXw?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p>Quick Installation video</p>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="videoWrp">
                                        <iframe class="video_groups" width="100%" height="315" src="https://www.youtube.com/embed/FmoscI7XwOs?enablejsapi=1&version=3&wmode=transparent" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p>Manual</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</section>
			<!-- // Pr PrVideo_sec -->	


			<!-- explore range -->			
			<section class="exploreSec">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Explore <span>range</span></h2>
                        <div id="slider3D" class="owl-carousel owl-theme">
                           <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-size.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Size</h5>
                                        <p>Because your fashionable door needs its ‘right’ match…</p>
                                    </div>
                                    <img src="img/prImgsl/re-size.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-place.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Place</h5>
                                        <p>Because your mechanical lock needs an upgrade…</p>
                                    </div>
                                    <img src="img/prImgsl/re-place.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-bell.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Bell</h5>
                                        <p>Because you need a <span class="strikethrough">99.99</span> “100” percent…</p>
                                    </div>
                                    <img src="img/prImgsl/re-bell.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-vel.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-Veal</h5>
                                        <p>Because you need the highest form of security…</p>
                                    </div>
                                    <img src="img/prImgsl/pr_1.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider3Dbox">
                                    <a href="re-ai.aspx" class="dtPage"></a>
                                    <div class="prDeas">
                                        <h5>RE-AL</h5>
                                        <p>Because you need a better security solution…</p>
                                    </div>
                                    <img src="img/prImgsl/re-ai.png" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
			<!-- // explore range -->
		</div>
		<!-- // content sec -->
</asp:Content>

