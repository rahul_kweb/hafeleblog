﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/AdminMaster.master" AutoEventWireup="true" CodeFile="BlogsComments.aspx.cs" Inherits="admin_Comments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div class="right_col" role="main">
        <div class="">
      <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Blogs Comments</h2>
                            <div class="clearfix"></div>
                        </div>
                        </div>
                    </div>
                </div>

           <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
                                OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging" DataKeyNames="CommentID"
                                class="table table-striped table-bordered" PageSize="10" AllowPaging="true">

                                <Columns>
                                    <asp:BoundField DataField="CommentID" HeaderText="CommentID" />
                                    <asp:BoundField DataField="BlogId" HeaderText="BlogId" />
                                    <asp:BoundField DataField="Title" HeaderText="Blog Title" />
                                    <asp:BoundField DataField="Name" HeaderText="Name" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" />
                                    <asp:BoundField DataField="Comment" HeaderText="Comment" />
                                    <asp:BoundField DataField="Approved" HeaderText="Approved" />
                          
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                                                CommandName="Select">
                            <i class="icon-edit"></i> Approve
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton3" CssClass="btn" runat="server" CausesValidation="False"
                                                CommandName="Select">
                            <i class="icon-edit"></i> Reject
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                                                CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination"></PagerStyle>
                            </asp:GridView>
                        </div>

                    </div>
                </div>
            </div>
              </div>
            </div>
</asp:Content>

