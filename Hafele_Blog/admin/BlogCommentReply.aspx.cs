﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_BlogCommentReply : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Admin"] != null)
        {
            if (!IsPostBack)
            {
                BindBlogSCommentsReply();
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    public void BindBlogSCommentsReply()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CommentReply 'GetCommentReply'");
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.Columns[1].Visible = false;
            gdView.DataSource = dt;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }

    }
    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {

            DataTable dt = new DataTable();
            int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);

            dt = utility.Display("Exec Proc_CommentReply 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                string status = dt.Rows[0]["Approved"].ToString();

                if (status == "False")
                {
                    using (SqlCommand cmd = new SqlCommand("Proc_CommentReply"))
                    {

                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@para", "updateApproved");
                        cmd.Parameters.AddWithValue("@ReplyId", Id);

                        if (utility.Execute(cmd))
                        {

                            BindBlogSCommentsReply();
                        }

                    }
                }

                else
                {
                    using (SqlCommand cmd = new SqlCommand("Proc_CommentReply"))
                    {

                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@para", "updateReject");
                        cmd.Parameters.AddWithValue("@ReplyId", Id);

                        if (utility.Execute(cmd))
                        {

                            BindBlogSCommentsReply();
                        }

                    }
                }


            }

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int Id = (int)gdView.DataKeys[e.RowIndex].Value;
        using (SqlCommand cmd = new SqlCommand("Proc_CommentReply"))
        {

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "delete");
            cmd.Parameters.AddWithValue("@ReplyId", Id);

            if (utility.Execute(cmd))
            {

                BindBlogSCommentsReply();
            }

        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindBlogSCommentsReply();
    }
}