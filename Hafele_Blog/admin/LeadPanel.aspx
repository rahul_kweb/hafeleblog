﻿<%@ Page Language="C#" MasterPageFile="~/admin/AdminMaster.master"  AutoEventWireup="true" CodeFile="LeadPanel.aspx.cs" Inherits="admin_LeadPanel" EnableEventValidation="false" %>

<%--<!DOCTYPE html>--%>

<%--<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lead Panel</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <link rel="shortcut icon" href="images/fab-icon.jpg" type="image/x-icon" />
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function (e) {
            $("#txtFrom").datepicker();
            $("#txtTo").datepicker();
            $("#txtFrom,#txtTo").datepicker("option", "dateFormat", "yy/mm/dd");
        });
        function validate() {
            if (document.getElementById("txtFrom").value == "" || document.getElementById("txtFrom").value == null) {
                alert("Please select the From Date");
                return false;
            }
            if (document.getElementById("txtTo").value == "" || document.getElementById("txtTo").value == null) {
                alert("Please select the To Date");
                return false;
            }
        }

    </script>
</head>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
        <div class="container">
            <div class="row ">
                <asp:DropDownList ID="ddlPage" runat="server" OnSelectedIndexChanged="ddlPage_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Value="LandingPage_PostLaunch">Hafele Post Launch Leads</asp:ListItem>
                    <asp:ListItem Value="LandingPage_PostLaunchDowloadB">Download Brochure Leads</asp:ListItem>

                </asp:DropDownList>
                <asp:Label ID="lblfrom" runat="server" Text="From Date:"></asp:Label>
                <asp:TextBox ID="txtFrom" ClientIDMode="Static" runat="server"></asp:TextBox>
                <asp:Label ID="lblto" runat="server" Text="To Date:"></asp:Label>
                <asp:TextBox ID="txtTo" ClientIDMode="Static" runat="server"></asp:TextBox>
                <asp:Button runat="server" ID="btnSubmit" Text="Submit" OnClick="btnSubmit_Click" OnClientClick="return validate()"></asp:Button>
                <asp:Button ID="btnExportWord" runat="server" Text="ExportToWord" OnClick="btnExportWord_Click" Style="display: none" Visible="false" />
                &nbsp;
        <asp:Button ID="btnExportExcel" runat="server" Text="ExportToExcel" OnClick="btnExportExcel_Click" />
                &nbsp;
        <asp:Button ID="btnExportPDF" runat="server" Text="ExportToPDF" OnClick="btnExportPDF_Click" Visible="false" />
                &nbsp;
         <asp:Button ID="Button1" runat="server" Text="ExportToCSV" OnClick="btnExportCSV_Click" Visible="false" />
                <asp:Button runat="server" ID="btnLogout" Text="Logout" OnClick="btnLogout_Click"></asp:Button><br />
                <br />
                <strong>Leads Count:
                    <asp:Literal ID="lblCount" runat="server"></asp:Literal></strong>
                <br />
                <asp:GridView ID="GridView1" runat="server" OnRowDataBound="GridView1_RowDataBound" CellPadding="4" ForeColor="#333333" class="table table-bordered table-striped" EmptyDataText="">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </div>
        </div>
  </div>
        </div>

     <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function (e) {
            $("#txtFrom").datepicker();
            $("#txtTo").datepicker();
            $("#txtFrom,#txtTo").datepicker("option", "dateFormat", "yy/mm/dd");
        });
        function validate() {
            if (document.getElementById("txtFrom").value == "" || document.getElementById("txtFrom").value == null) {
                alert("Please select the From Date");
                return false;
            }
            if (document.getElementById("txtTo").value == "" || document.getElementById("txtTo").value == null) {
                alert("Please select the To Date");
                return false;
            }
        }

    </script>
</asp:Content>
