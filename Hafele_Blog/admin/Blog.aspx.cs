﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Blog : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Admin"] != null)
        {
            if (!IsPostBack)
            {
                BindDropdown();
                BindGrid();
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }

    }

    private void BindDropdown()
    {
        try
        {
            
            DataTable dt = new DataTable();
            dt = utility.Display("Execute Proc_Blogs 'bindCategory'");

            if (dt.Rows.Count > 0)
            {
                listCategory.DataSource = dt;
                listCategory.DataBind();
                listCategory.DataTextField = "Category";
                listCategory.DataValueField = "Category";
                listCategory.DataBind();


            }
            else
            {
                listCategory.DataSource = null;
                listCategory.DataBind();


            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }

    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = utility.Display("Execute Proc_Blogs 'get'");

            if (dt.Rows.Count > 0)
            {
                gdView.Columns[0].Visible = false;
                gdView.DataSource = dt;
                gdView.DataBind();


            }
            else
            {
                gdView.DataSource = null;
                gdView.DataBind();


            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }


    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/Blogs/";



        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                if (fileUploadImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Image-", ext, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }



                using (SqlCommand cmd = new SqlCommand("Proc_Blogs"))
                {
                    string postDate = DateTime.ParseExact(txtDate.Text, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");


                    string Category = "";
                    foreach (ListItem item in listCategory.Items)
                    {
                        if (item.Selected)
                        {
                            Category += item.Text + ",";
                        }
                    }

                    if (Category.Length > 0)
                    {
                        Category = Category.Substring(0, Category.Length - 1);
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "add");
                    cmd.Parameters.AddWithValue("@Image", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Title", txtTitle.Text.Trim());
                    cmd.Parameters.AddWithValue("@Category", Category);
                    cmd.Parameters.AddWithValue("@Tags", txtTags.Text.Trim());
                    //cmd.Parameters.AddWithValue("@Tags", ddlTags.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@BlogType", txtBlogType.Text.Trim());
                    cmd.Parameters.AddWithValue("@PostDate", postDate);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    //cmd.Parameters.AddWithValue("@Slug", Generateslg(txtHeding.Text.Trim()));


                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        BindDropdown();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }

        else
        {
            if (fileUploadImage.HasFile)
            {
                ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                if (!utility.IsValidImageFileExtension(ext))
                {
                    MyMessageBox1.ShowError("Only Image file allowed.");
                    return; // STOP FURTHER PROCESSING
                }

                MainImage = utility.GetUniqueName(VirtualPart, "Blogs-", ext, this, false);
                fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
            }


            using (SqlCommand cmd = new SqlCommand("Proc_Blogs"))
            {
                string postDate = DateTime.ParseExact(txtDate.Text, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                cmd.Parameters.AddWithValue("@Image", MainImage + ext);
                cmd.Parameters.AddWithValue("@Title", txtTitle.Text.Trim());

                string Category = "";
                foreach (ListItem item in listCategory.Items)
                {
                    if (item.Selected)
                    {
                        Category += item.Text +",";
                    }
                }

                if (Category.Length > 0)
                {
                    Category = Category.Substring(0, Category.Length - 1);
                }

                cmd.Parameters.AddWithValue("@Category", Category);
                cmd.Parameters.AddWithValue("@Tags", txtTags.Text.Trim());
                //cmd.Parameters.AddWithValue("@Tags", ddlTags.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@BlogType", txtBlogType.Text.Trim());
                cmd.Parameters.AddWithValue("@PostDate", postDate);
                cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                //cmd.Parameters.AddWithValue("@Slug", Generateslg(txtHeding.Text.Trim()));


                if (utility.Execute(cmd))
                {
                    Reset();
                    BindGrid();
                    BindDropdown();
                    MyMessageBox1.ShowSuccess("Successfully updated");

                }
                else
                {
                    MyMessageBox1.ShowWarning("Unable to updated");
                }
            }
        }
    }

    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        //if (!fileUploadImage.HasFile)
        //{
        //    message = "Image, ";
        //    isOK = false;
        //}

        if (txtTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Title, ";
        }


        if (listCategory.SelectedValue == "0")
        {
            isOK = false;
            message += "Category, ";
        }

        //if (txtTags.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Tags, ";
        //}


        if (txtBlogType.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Blog Type, ";
        }


        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (txtDate.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Date, ";
        }
 
        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;


        if (txtTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Title, ";
        }


        if (listCategory.SelectedValue == "0")
        {
            isOK = false;
            message += "Category, ";
        }

        //if (txtTags.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Tags, ";
        //}


        if (txtBlogType.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Blog Type, ";
        }


        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (txtDate.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Date, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }


    private void Reset()
    {

        hdnId.Value = string.Empty;
        txtTitle.Text = string.Empty;
        //listCategory.SelectedValue = "0";
        txtTags.Text = string.Empty;
        txtBlogType.Text = string.Empty;
        txtDate.Text = string.Empty;
        txtDescription.Text = string.Empty;
        btnsumbit.Text = "Save";
        ImagePreview.Visible = false;


    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);
            dt = utility.Display("Exec Proc_Blogs 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                hdnId.Value = dt.Rows[0]["Id"].ToString();
                txtTitle.Text = dt.Rows[0]["Title"].ToString();

                var Category= dt.Rows[0]["Category"].ToString();
  
                    foreach (ListItem item in listCategory.Items)
                    {
                        if (Category.Contains(item.Text))
                            item.Selected = true;
                    }



                //var Tags = dt.Rows[0]["Tags"].ToString();

                //foreach (string tech in Tags.Split(','))
                //{
                //    if (tech != "")
                //    {
                //        ListItem items = new ListItem();

                //        items.Selected = true;
                //    }


                //}


                //ddlTags.SelectedItem.Text = dt.Rows[0]["Tags"].ToString();

                txtTags.Text = dt.Rows[0]["Tags"].ToString();
                txtBlogType.Text = dt.Rows[0]["BlogType"].ToString();
                txtDate.Text = dt.Rows[0]["PostDate"].ToString();
                txtDescription.Text = dt.Rows[0]["Description"].ToString();
                ImagePreview.Visible = true;
                ImagePreview.ImageUrl = string.Format("~/Content/uploads/Blogs/" + dt.Rows[0]["Image"].ToString());

                btnsumbit.Text = "Update";

            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Blogs"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "delete");
            cmd.Parameters.AddWithValue("@Id", Id);
            if (utility.Execute(cmd))
            {
                Reset();
                BindGrid();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }

    public string Generateslg(string strtext)
    {
        string str = strtext.ToString().ToLower();
        str = Regex.Replace(str, @"[^0-9a-zA-Z]+", "-");
        return str;
    }
}