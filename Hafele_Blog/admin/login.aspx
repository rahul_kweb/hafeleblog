﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="admin_login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hafele</title>
    <style>
        .lblClass {
            float: left;
            text-align: right;
            margin-right: 26px;
            width: 104px;
            display: block;
            color: #913092;
            font-size: 23px;
            padding-top: 5px;
        }

        .lblWrongClass {
            float: left;
            text-align: right;
            margin-right: 26px;
            width: 250px;
            display: block;
            color: #ff0000;
            font-size: 14px;
            padding-top: 1px;
        }

        .txtClass {
            width: 150px;
            background: #cccdce;
            height: 35px;
            border: none;
            border-radius: 20px;
            float: right;
            outline: 0 !important;
            padding: 0 0 0 15px;
        }

        .btnClass {
            /*background: url(images/Submit.png) no-repeat 0 0;*/
            width: 75px;
            height: 37px;
            border-radius: 15px;
            border: 0;
            float: right;
            margin-top: 15px;
            outline: 0 !important;
            cursor: pointer;
            margin-top: -35px;
            color: #913092;
            font-weight: bold;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="margin: 0px auto; width: 300px;">
                <div style="height: 120px;"></div>
                <div style="width: 100px; height: 20px; padding: 5px; background: #FFA500; border-top-left-radius: 5px; border-top-right-radius: 5px; font-size: 20px; font-weight: bold; text-align: center; color: white;">Login</div>
                <div style="width: 295px; height: 150px; padding: 25px; border: 3px solid #FFA500; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
                    <div>
                        <asp:Label ID="Label1" CssClass="lblClass" runat="server" Text="Name :"></asp:Label>
                        <asp:TextBox ID="txtLoginName" CssClass="txtClass" runat="server"></asp:TextBox>
                        <br />
                        <br />
                        <br />
                        <asp:Label ID="Label2" CssClass="lblClass" runat="server" Text="Password :"></asp:Label>
                        <asp:TextBox ID="txtLoginPassword" CssClass="txtClass" TextMode="Password" runat="server"></asp:TextBox>
                    </div>
                    <div style="clear: both;"></div>
                    <div style="position: relative; float: right;">
                        <asp:Label ID="lblWrong" CssClass="lblWrongClass" runat="server" Visible="false" Text="*Sorry, Wrong credencials."></asp:Label>
                    </div>
                    <br />
                    <br />
                    <br />
                    <asp:Button ID="btnLogin" CssClass="btnClass" runat="server" Text="SUBMIT" OnClick="btnLogin_Click" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
