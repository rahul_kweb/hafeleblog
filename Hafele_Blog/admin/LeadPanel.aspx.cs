﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;

public partial class admin_LeadPanel : System.Web.UI.Page
{
    string Source;
    string utm_source;
    string utm_medium;
    string utm_content;
    string utm_campaign;
    string utm_keyword;

    private object con;
    string query;
    SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["HafeleIconicCMS"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Admin"] != null)
        {
            if (!IsPostBack)
            {
                //if (DateTime.Now >= Convert.ToDateTime("8/15/2016 00:00:00 PM"))
                //ddlContest.Items.FindByText("Contest4").Selected = true;
                //else
                //    ddlContest.Items.FindByText("Contest2").Selected = true;

                // string strQuery = "SELECT [Name],[Mobile],[Email],[City],case [WinnerStatus] when 1 then 'All Answers are Correct' else 'Incorrect Answer' end[Winner Status] ,[Answer1] ,[Answer2] ,[Answer3],[Source],[utm_source] ,[utm_medium],[utm_content],[utm_campaign],[utm_keyword],[CreatedOn] ,[Contest] FROM [dbo].[Holi_Landing] where [Contest]='" + ddlContest.SelectedItem.Text + "'  ORDER BY CreatedOn DESC";
                //string strQuery = "SELECT [FullName],[EmailID],[MobileNo],[City],[SelectedOffer] FROM [dbo].[Holi_Landing] where [SelectedOffer]='" + ddlContest.SelectedItem.Text + "'  ORDER BY CreatedOn DESC";

                loadData();
                //query = "Select * from [dbo].[GetInTouch] where CONVERT(VARCHAR(10),CreatedOn,101) >= '" + DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + "' and CONVERT(VARCHAR(10),CreatedOn,101) <= '" + DateTime.Now.ToString("MM/dd/yyyy") + "'  ORDER BY CreatedOn DESC";
                //SqlCommand cmd = new SqlCommand(query);
                //DataTable dt = GetData(cmd);
                //GridView1.DataSource = dt;
                //GridView1.DataBind();
                //lblCount.Text = Convert.ToString(dt.Rows.Count);
            }
        }
        else
            Response.Redirect("Login.aspx");
    }

    private DataTable loadData()
    {
        string strQuery = string.Empty;

        //strQuery += "Select * from [dbo].[" + ddlPage.SelectedValue + "]";

        if (ddlPage.SelectedItem.Value == "LandingPage_PostLaunch")
        {
            strQuery += "WITH cte_LandingPage_PostLaunch AS(SELECT *, ROW_NUMBER() OVER (PARTITION BY MobileNo ORDER BY CreatedOn DESC) AS rownum FROM LandingPage_PostLaunch)";
                strQuery += "select Name ,LastName ,MobileNo ,Email ,Product ,FORMAT(CreatedOn,'dd/MM/yyyy hh:mm tt') CreatedDate,CreatedOn, source ,utm_source ,utm_medium ,utm_content ,utm_campaign ,utm_keyword ,Device ,utm_adgroup ,utm_ad ,PageSource ,keyword ,City from cte_LandingPage_PostLaunch";
        }
        else
        {
            strQuery += "WITH CTELandingPage_PostLaunchDowloadB AS(SELECT *, ROW_NUMBER() OVER (PARTITION BY Email ORDER BY CreatedOn DESC) AS rownum FROM LandingPage_PostLaunchDowloadB)";
            strQuery += "select Email ,FORMAT(CreatedOn,'dd/MM/yyyy hh:mm tt') CreatedDate,CreatedOn, source ,utm_source ,utm_medium ,utm_content ,utm_campaign ,utm_keyword ,Device ,utm_adgroup ,utm_ad ,PageSource ,keyword from CTELandingPage_PostLaunchDowloadB";
        }


        if (!(string.IsNullOrEmpty(txtFrom.Text.Trim()) && string.IsNullOrEmpty(txtTo.Text.Trim())))
        {
            strQuery += " where CONVERT(VARCHAR(10),CreatedOn,111) >= '" + txtFrom.Text + "' and CONVERT(VARCHAR(10),CreatedOn,111) <= '" + txtTo.Text + "'";

        }
        else {
            strQuery += " where CONVERT(VARCHAR(10),CreatedOn,111) >= '" + DateTime.Now.AddDays(-1).ToString("yyyy/mm/dd") + "' and CONVERT(VARCHAR(10),CreatedOn,111) <= '" + DateTime.Now.ToString("yyyy/mm/dd") + "'";
        }
       
        if (ddlPage.SelectedItem.Value == "LandingPage_PostLaunch")
        {
            strQuery += " and rownum=1";
        }
        else
        {
            strQuery += " and rownum=1";
        }
        
        strQuery += " ORDER BY CreatedOn DESC";
        SqlCommand cmd = new SqlCommand(strQuery);
        DataTable dt = GetData(cmd);
        dt.Columns.Remove("CreatedOn");
        GridView1.DataSource = dt;
        GridView1.DataBind();
        lblCount.Text = Convert.ToString(dt.Rows.Count);
        return dt;
    }

    protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadData();
    }

    private DataTable GetData(SqlCommand cmd)
    {
        DataTable dt = new DataTable();
        SqlDataAdapter sda = new SqlDataAdapter();
        cmd.CommandType = CommandType.Text;
        cmd.Connection = con1;
        try
        {
            con1.Open();
            sda.SelectCommand = cmd;
            sda.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            con1.Close();
            sda.Dispose();
            con1.Dispose();
        }
    }

    protected void btnLogout_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Redirect("Login.aspx");
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void OnPaging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }

    protected void btnExportWord_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.doc");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-word ";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        GridView1.AllowPaging = false;
        GridView1.DataBind();
        GridView1.RenderControl(hw);
        Response.Output.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }

    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string FileName = "Data" + DateTime.Now + ".xls";
        StringWriter strwritter = new StringWriter();
        HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
        GridView1.GridLines = GridLines.Both;
        GridView1.HeaderStyle.Font.Bold = true;
        GridView1.RenderControl(htmltextwrtter);
        Response.Write(strwritter.ToString());
        Response.End();
    }

    protected void btnExportPDF_Click(object sender, EventArgs e)
    {
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        GridView1.AllowPaging = false;
        GridView1.DataBind();
        GridView1.RenderControl(hw);
        StringReader sr = new StringReader(sw.ToString());


        Response.End();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {

            //query = "SELECT * FROM[dbo].[GetInTouch] where CONVERT(VARCHAR(10),CreatedOn,101) >= '" + txtFrom.Text + "' and CONVERT(VARCHAR(10),CreatedOn,101) <= '" + txtTo.Text + "'  ORDER BY CreatedOn DESC";
            //con1.Open();

            //SqlCommand cmd = new SqlCommand(query, con1);
            //SqlDataAdapter ad = new SqlDataAdapter(cmd);
            //DataTable dt = new DataTable();
            //ad.Fill(dt);
            //GridView1.DataSource = dt;
            //GridView1.DataBind();
            //lblCount.Text = Convert.ToString(dt.Rows.Count);
            Session["data"] = loadData();
            //con1.Close();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnExportCSV_Click(object sender, EventArgs e)
    {
        //if (Session["data"] != null)
        //{
        //    GridView1.DataSource = Session["data"];
        //    GridView1.DataBind();
        //}

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.csv");
        Response.Charset = "";
        Response.ContentType = "application/text";

        //GridView1.AllowPaging = false;
        //GridView1.DataBind();

        StringBuilder sb = new StringBuilder();
        for (int k = 0; k < GridView1.HeaderRow.Cells.Count; k++)
        {
            //add separator
            sb.Append(GridView1.HeaderRow.Cells[k].Text + ';');
        }
        //append new line
        sb.Append("\r\n");
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            for (int k = 0; k < GridView1.HeaderRow.Cells.Count; k++)
            {
                //add separator
                sb.Append(GridView1.Rows[i].Cells[k].Text.Replace("&nbsp;", "") + ';');
            }
            //append new line
            sb.Append("\r\n");
        }
        Response.Output.Write(sb.ToString());
        Response.Flush();
        Response.End();

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        // e.Row.Cells[0].Visible = false; // hides the first column

    }
}