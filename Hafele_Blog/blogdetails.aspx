﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="blogdetails.aspx.cs" EnableEventValidation="false" Inherits="blogdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta charset="utf-8">
    <title>Hafele - Blogs</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/fav-icon.png" type="image/x-icon">
    <link rel="stylesheet" href="/Content/Blogs/css/bootstrap.min.css">
    <link rel="stylesheet" href="/Content/Blogs/css/mystyle.css">
    <link rel="stylesheet" type="text/css" href="/Content/Blogs/css/slick.css">
    <link rel="stylesheet" type="text/css" href="/Content/Blogs/css/aos.css">
    <link rel="stylesheet" href="/Content/Blogs/css/font-awesome.min.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <div class="content_wrapper">
    <section class="mainContent">
<div class="container mb-5  px-lg-0 ">
    <div class="row  pb-5">
        <div class="col-lg-8" >
            <div class="blogDetailsWrap">
                <asp:HiddenField ID="hdnBlogId" runat="server" />
                <asp:HiddenField ID="hdnBlogCommentsCount" runat="server" />
            <%--	<div class="blogPic mb-5" data-aos="fade-up"><img src="/Content/Blogs/images/blogs/1.jpg" class="d-block w-100"></div>
                
                <div class="blogDate">21 Jun 2021</div>
                
                <h1 class="title mb-3">Lorem ipsum dummy Hafele Blog </h1>
                
                
                <div class="blogTags mb-4">
                	<img src="/Content/Blogs/images/tag.png">
                    <a href="#">Lorem ipsum</a>,
                    <a href="#">Tempor incididunt</a>,
                    <a href="#">Sorem ipsum</a>,
                    <a href="#">Mempor incididunt</a>
                    
                    <span>03 Comments</span>
                    <div class="clearfix"></div>
                </div>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</p>

				<div class="row">
                	<div class="col-md-5 mb-4">
                    	<img src="/Content/Blogs/images/blogs/1.jpg" class="d-block w-100">
                    </div>
                    
                    <div class="col-md-7">
                    	<ul class="commmon">
                        	<li>Lorem ipsum dolor sit amet, consectetur adipisicing eli</li>
                            <li>Tempor incididunt ut labore et dolore magna aliqua</li>
                            <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco</li>
                            <li>Laboris nisi ut aliquip ex ea commodo consequat</li>
                            <li>Duis aute irure dolor in reprehenderit in voluptate velit </li>
                            <li>Tempor incididunt ut labore et dolore magna aliqua</li>
                            <li>Ut enim ad minim veniam, quis nostrud exercitation </li>
                            <li>Laboris nisi ut aliquip ex ea commodo consequat</li>
                        </ul>
                    </div>
                </div>


<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</p>--%>

<asp:Literal ID="ltrdetails" runat="server"></asp:Literal>
			<%--<div class="row">
            	<div class="col-md-8">
                	<div class="row row-cols-2 row-cols-sm-4 row-cols-lg-4">
                        <div class="col"><a href="#" class="tags">Category 01</a></div>
                        <div class="col"><a href="#" class="tags">Category 02</a></div>
                        <div class="col"><a href="#" class="tags">Category 03</a></div>
                    </div>
                </div>
                
                <div class="col-md-4">
                	<div class="d-inline-block mr-3">Share Now</div>
                    <div class="d-inline-block shareMedia">
                    	<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>--%>
            
            
    <div class="commentSection my-5" data-aos="fade-up">
        <asp:Literal ID="ltrCommentCount" runat="server"></asp:Literal> 
            	<%--<div class="commentCount">03 Comments</div>--%>
                
        <ul>
                   <asp:Literal ID="ltrCommentReply" runat="server"></asp:Literal> 
    <%--    <li class="row justify-content-center position-relative">
            <div class="col-5 col-sm-2">
                <div class="commentPic"><img src="/Content/Blogs/images/commentPic.jpg" class="w-100 d-block"></div>
            </div>
            <div class="col-sm-10">
                <div class="comment">
                    <div class="commentBy ">Sourav Maheshwari Says <span>:</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                    <div class="commentDate">MAY 29, 2018 AT 9:09 AM</div>
                </div>
                
                <div class="comment  pl-4">
                    <div class="commentBy redColor">Sourav Maheshwari Says <span>:</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                    <div class="commentDate">MAY 29, 2018 AT 9:09 AM</div>
                </div>
                
            </div>
            
            <button  class="replBtn  d-inline-block" onclick="myFunction()" type="button">Reply</button>
            
        <div class=" col-12 commentReply"  >
        <div class="replybox">
            <div class="row form-group mb-0">
            <div class="col-12"><textarea class="form-control rounded-0 mb-3" placeholder="Your Reply"></textarea></div>
            <div class="col-sm-5"><input type="text" class="form-control rounded-0 mb-3" placeholder="Name"></div>
            <div class="col-sm-5"><input type="email" class="form-control rounded-0 mb-3" placeholder="Email"></div>
            <div class="col-sm-2"><button  class="form-control rounded-0"><i class="fa fa-paper-plane" aria-hidden="true"></i></button></div>
            </div>
             </div>
        </div>
        </li>
                    
        <li class="row justify-content-center position-relative">
            <div class="col-5 col-sm-2">
                <div class="commentPic"><img src="/Content/Blogs/images/commentPic.jpg" class="w-100 d-block"></div>
            </div>
            <div class="col-sm-10">
                <div class="comment">
                    <div class="commentBy ">Sourav Maheshwari Says <span>:</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                    <div class="commentDate">MAY 29, 2018 AT 9:09 AM</div>
                </div>
                
                <div class="comment  pl-4">
                    <div class="commentBy redColor">Sourav Maheshwari Says <span>:</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                    <div class="commentDate">MAY 29, 2018 AT 9:09 AM</div>
                </div>
                
            </div>
            
            <button  class="replBtn  d-inline-block" onclick="myFunction()">Reply</button>
            
        <div class=" col-12 commentReply"  >
         <div class="replybox">
            <div class="row form-group mb-0">
            <div class="col-12"><textarea class="form-control rounded-0 mb-3" placeholder="Your Reply"></textarea></div>
            <div class="col-sm-5"><input type="text" class="form-control rounded-0 mb-3" placeholder="Name"></div>
            <div class="col-sm-5"><input type="email" class="form-control rounded-0 mb-3" placeholder="Email"></div>
            <div class="col-sm-2"><button  class="form-control rounded-0"><i class="fa fa-paper-plane" aria-hidden="true"></i></button></div>
            </div>
        </div>
        </div>
        </li>
        
        <li class="row justify-content-center position-relative">
            <div class="col-5 col-sm-2">
                <div class="commentPic"><img src="/Content/Blogs/images/commentPic.jpg" class="w-100 d-block"></div>
            </div>
            <div class="col-sm-10">
                <div class="comment">
                    <div class="commentBy ">Sourav Maheshwari Says <span>:</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                    <div class="commentDate">MAY 29, 2018 AT 9:09 AM</div>
                </div>
                
                <div class="comment  pl-4">
                    <div class="commentBy redColor">Sourav Maheshwari Says <span>:</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                    <div class="commentDate">MAY 29, 2018 AT 9:09 AM</div>
                </div>
                
            </div>
            
            <button  class="replBtn  d-inline-block" onclick="myFunction()">Reply</button>
            
        <div class=" col-12 commentReply"  >
         <div class="replybox">
            <div class="row form-group mb-0">
            <div class="col-12"><textarea class="form-control rounded-0 mb-3" placeholder="Your Reply"></textarea></div>
            <div class="col-sm-5"><input type="text" class="form-control rounded-0 mb-3" placeholder="Name"></div>
            <div class="col-sm-5"><input type="email" class="form-control rounded-0 mb-3" placeholder="Email"></div>
            <div class="col-sm-2"><button  class="form-control rounded-0"><i class="fa fa-paper-plane" aria-hidden="true"></i></button></div>
            </div>
           </div>
        </div>
        </li>
        
        <li class="row justify-content-center position-relative">
            <div class="col-5 col-sm-2">
                <div class="commentPic"><img src="/Content/Blogs/images/commentPic.jpg" class="w-100 d-block"></div>
            </div>
            <div class="col-sm-10">
                <div class="comment">
                    <div class="commentBy ">Sourav Maheshwari Says <span>:</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                    <div class="commentDate">MAY 29, 2018 AT 9:09 AM</div>
                </div>
                
                <div class="comment  pl-4">
                    <div class="commentBy redColor">Sourav Maheshwari Says <span>:</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                    <div class="commentDate">MAY 29, 2018 AT 9:09 AM</div>
                </div>
                
            </div>
            
            <button  class="replBtn  d-inline-block" onclick="myFunction()">Reply</button>
            
        <div class=" col-12 commentReply"  >
         <div class="replybox">
            <div class="row form-group mb-0">
            <div class="col-12"><textarea class="form-control rounded-0 mb-3" placeholder="Your Reply"></textarea></div>
            <div class="col-sm-5"><input type="text" class="form-control rounded-0 mb-3" placeholder="Name"></div>
            <div class="col-sm-5"><input type="email" class="form-control rounded-0 mb-3" placeholder="Email"></div>
            <div class="col-sm-2"><button  class="form-control rounded-0"><i class="fa fa-paper-plane" aria-hidden="true"></i></button></div>
            </div>
           </div>
        </div>
        </li>
        
        <li class="row justify-content-center position-relative">
            <div class="col-5 col-sm-2">
                <div class="commentPic"><img src="/Content/Blogs/images/commentPic.jpg" class="w-100 d-block"></div>
            </div>
            <div class="col-sm-10">
                <div class="comment">
                    <div class="commentBy ">Sourav Maheshwari Says <span>:</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                    <div class="commentDate">MAY 29, 2018 AT 9:09 AM</div>
                </div>
                
                <div class="comment  pl-4">
                    <div class="commentBy redColor">Sourav Maheshwari Says <span>:</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                    <div class="commentDate">MAY 29, 2018 AT 9:09 AM</div>
                </div>
                
            </div>
            
            <button  class="replBtn  d-inline-block" onclick="myFunction()">Reply</button>
            
        <div class=" col-12 commentReply"  >
         <div class="replybox">
            <div class="row form-group mb-0">
            <div class="col-12"><textarea class="form-control rounded-0 mb-3" placeholder="Your Reply"></textarea></div>
            <div class="col-sm-5"><input type="text" class="form-control rounded-0 mb-3" placeholder="Name"></div>
            <div class="col-sm-5"><input type="email" class="form-control rounded-0 mb-3" placeholder="Email"></div>
            <div class="col-sm-2"><button  class="form-control rounded-0"><i class="fa fa-paper-plane" aria-hidden="true"></i></button></div>
            </div>
         </div>
        </div>
        </li>--%>         
                  
        </ul>
                
            </div>


            
            <div class="blogReplyBox p-4 mb-5" data-aos="fade-up">
            	<h1 class="title mb-3">Leave a Reply</h1>
                <p class="otherHead mb-4">Your email address will not be published.</p>
                
             <%--   <form class="row form-group">
                	<div class="col-12"><textarea class="form-control rounded-0 mb-3" placeholder="Your Comment"></textarea></div>
                    <div class="col-sm-6"><input type="text" class="form-control rounded-0 mb-3" placeholder="Name"></div>
                    <div class="col-sm-6"><input type="email" class="form-control rounded-0 mb-3" placeholder="Email"></div>
                    <div class="col-sm-3"><input type="submit" class="form-control rounded-0" value="Post Comments"></div>
                </div>--%>

                   <div class="row form-group">
                	<div class="col-12">
                        <asp:TextBox ID="txtComment" TextMode="MultiLine" runat="server" class="form-control rounded-0 mb-3" placeholder="Your Comment"></asp:TextBox>
                                   <span id="ErrorComment" class="ErrorValidation">Comment is required.</span>
                	</div>
                    <div class="col-sm-6">
                        <asp:TextBox Id="txtName" class="form-control rounded-0 mb-3" placeholder="Name" runat="server"></asp:TextBox>
                            <span id="ErrorName" class="ErrorValidation">Name is required.</span>
                    </div>
                    
                       <div class="col-sm-6">
                        <asp:TextBox ID="txtEmail" runat="server" class="form-control rounded-0 mb-3" placeholder="Email"></asp:TextBox>
                                          <span id="ErrorEmail1" class="ErrorValidation">Email is required.</span>
                                            <span id="ErrorEmail2" class="ErrorValidation">Invalid email address.</span>
                       </div>
                    
                       <div class="col-sm-3"><asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" type="submit" class="form-control rounded-0"  OnClientClick="javascript:return validation();" Text="Post Comments" value="Post Comments" runat="server" /></div>
                </div>
         
            </div>
            
            <div class="articleSlider mb-5 pb-4">
                <asp:Literal ID="ltrBlogDetailBottomBlogs" runat="server"></asp:Literal>
        <%--<div>
            <a href="#"  class="articleBox d-flex h-100">
                <div class="align-self-center text-center w-100">  	
                    <div class="blogBrief">
                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                    </div>
                    <h1>Lorem ipsum dummy Hafele Blog </h1>
                </div>
            </a>
        </div>
        
        <div>
            <a href="#"  class="articleBox d-flex h-100">
                <div class="align-self-center text-center w-100">  	
                    <div class="blogBrief">
                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                    </div>
                    <h1>Lorem ipsum dummy Hafele Blog  </h1>
                </div>
            </a>
        </div>
        
        <div>
            <a href="#"  class="articleBox d-flex h-100">
                <div class="align-self-center text-center w-100">  	
                    <div class="blogBrief">
                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                    </div>
                    <h1>Lorem ipsum dummy Hafele Blog  </h1>
                </div>
            </a>
        </div>
        
        <div>
            <a href="#"  class="articleBox d-flex h-100">
                <div class="align-self-center text-center w-100">  	
                    <div class="blogBrief">
                        <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                    </div>
                    <h1>Lorem ipsum dummy Hafele Blog  </h1>
                </div>
            </a>
        </div>--%>
        
      
    </div>
            
            </div>
        </div>
        
        <div class="col-lg-4">
        	<h1 class="title mb-4" data-aos="zoom-in">Related <span>post</span></h1>
            
            <ul class="postList mb-5">
                <asp:Literal ID="ltrDetailsRelatedBlog" runat="server"></asp:Literal>
            <%--	<li data-aos="fade-up" data-aos-delay="200">
                	<a href="#">
                    	<div class="blogBrief">
                            <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                        </div>
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
		                <p>Consectetur adipiscing elitseding</p>
                    </a>
                </li>
                
                <li data-aos="fade-up" data-aos-delay="300">
                	<a href="#">
                    	<div class="blogBrief">
                            <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                        </div>
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
		                <p>Consectetur adipiscing elitseding</p>
                    </a>
                </li>
                
                <li data-aos="fade-up" data-aos-delay="400">
                	<a href="#">
                    	<div class="blogBrief">
                            <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                        </div>
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
		                <p>Consectetur adipiscing elitseding</p>
                    </a>
                </li>
                <li data-aos="fade-up" data-aos-delay="400">
                	<a href="#">
                    	<div class="blogBrief">
                            <span class="categoryTitle">Categoty Title</span>   |  <span class="blogDate">21 Jun 2021</span>
                        </div>
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
		                <p>Consectetur adipiscing elitseding</p>
                    </a>
                </li>--%>
         
            </ul>
            
            <h1 class="title mb-4" data-aos="zoom-in">Populer <span>post</span></h1>
            
            <div class="row mb-3">
                <asp:Literal ID="ltrPopularBlogDetails" runat="server"></asp:Literal>
    <%--        	<div class="col-md-6 col-lg-12 mb-4 " data-aos="fade-up" >
            		<div class="blogBox dashedBorder ">
                        <a href="#">
                        <div class="blogPic mb-4"><img src="/Content/Blogs/images/post/1.jpg" class=""></div>
                       	
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
                        <p>Consectetur adipiscing elitseding</p>
                        </a>
                    </div>
        		</div>
                
                <div class="col-md-6 col-lg-12 mb-4 " data-aos="fade-up" >
            		<div class="blogBox dashedBorder ">
                        <a href="#">
                        <div class="blogPic mb-4"><img src="/Content/Blogs/images/post/2.jpg" class=""></div>
                       	
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
                        <p>Consectetur adipiscing elitseding</p>
                        </a>
                    </div>
        		</div>
                <div class="col-md-6 col-lg-12 mb-4 " data-aos="fade-up" >
            		<div class="blogBox dashedBorder ">
                        <a href="#">
                        <div class="blogPic mb-4"><img src="/Content/Blogs/images/post/3.jpg" class=""></div>
                       	
                        <h1>Lorem ipsum dummy Hafele Blog </h1>
                        <p>Consectetur adipiscing elitseding</p>
                        </a>
                    </div>
        		</div>--%>
            </div>
            
            
            <h1 class="title mb-4" data-aos="zoom-in">#Tags <span>posts</span></h1>
            <div class="row row-cols-2 row-cols-sm-4 row-cols-lg-3">
            	<%--<div class="col"><a href="#" class="tags">Category 01</a></div>
                <div class="col"><a href="#" class="tags">Category 02</a></div>
                <div class="col"><a href="#" class="tags">Category 03</a></div>
                <div class="col"><a href="#" class="tags">Category 04</a></div>
                <div class="col"><a href="#" class="tags">Category 05</a></div>
                <div class="col"><a href="#" class="tags">Category 06</a></div>
                <div class="col"><a href="#" class="tags">Category 07</a></div>
                <div class="col"><a href="#" class="tags">Category 08</a></div>--%>
                <asp:Literal ID="ltrTagsByTitle" runat="server"></asp:Literal>
            </div>
            
        </div>
    </div>
</div>
</section>
          </div>

    <style>
             .ErrorValidation {

        display: none;
        color: #dc3545!important;
       margin-bottom: 10px;
      
    }
    </style>

        <script src="/Content/Blogs/js/jquery-3.5.1.min.js"></script>
    <script src="/Content/Blogs/js/bootstrap.js"></script>
    <script src="/Content/Blogs/js/aos.js"></script>
    <script src="/Content/Blogs/js/common.js"></script>
    <script src="/Content/Blogs/js/slick.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sharer.js@latest/sharer.min.js"></script>

    <script>

        // banner slider //
        $('.bannerSlider').slick({ autoplay: true, speed: 1000, autoplaySpeed: 3000, pauseOnHover: false, lazyLoad: 'progressive', arrows: true, dots: false, fade: true, }).slickAnimation();

        // post slider //
        $(".postSlider").slick({ slidesToShow: 3, slidesToScroll: 1, autoplay: !1, arrows: !0, dots: !1, infinite: !0, autoplaySpeed: 2e3, responsive: [{ breakpoint: 991, settings: { slidesToShow: 2, arrows: !0, dots: !1, slidesToScroll: 1 } }, { breakpoint: 600, settings: { slidesToShow: 1, arrows: !0, dots: !1, slidesToScroll: 1, adaptiveHeight: !0 } }] });

        // article slider //
        $(".articleSlider").slick({ slidesToShow: 2, slidesToScroll: 1, autoplay: !1, arrows: !0, dots: !1, infinite: !0, autoplaySpeed: 2e3, responsive: [{ breakpoint: 991, settings: { slidesToShow: 2, arrows: !1, dots: !0, slidesToScroll: 1 } }, { breakpoint: 600, settings: { slidesToShow: 1, arrows: !1, dots: !0, slidesToScroll: 1, adaptiveHeight: !0 } }] });


//$(".nextForm").click(function() {
//	var activeTab = $(this).attr("data-title");
//	var openForm = $("#"+activeTab);	
//	openForm.children("form").slideToggle();
//	$(this).delay(500).fadeOut();
//	$('html,body').delay(700).animate({scrollTop:openForm.offset().top-160},1000);
//});

//$(".nextForm").click(function(){
//  $(".commentReply").slideToggle();
//});


        /// Comment Validation
          function validation() {
            var Name = $('#<%=txtName.ClientID%>').val();
            var Email = $('#<%=txtEmail.ClientID%>').val();
            var Comment = $('#<%=txtComment.ClientID%>').val();
            var blank = false;

            if (Name == '') {
                $('#ErrorName').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorName').css('display', 'none');

            }


            if (Email == '') {
                $('#ErrorEmail2').css('display', 'none');
                $('#ErrorEmail1').css('display', 'block');
                blank = true;
            }
            else {
                var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText.test(Email)) {
                    $('#ErrorEmail1').css('display', 'none');
                    $('#ErrorEmail2').css('display', 'block');
                    blank = true;
                }
                else {
                    $('#ErrorEmail2').css('display', 'none');


                }
                $('#ErrorEmail1').css('display', 'none');


            }
        

            if (Comment == '') {
                $('#ErrorComment').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorComment').css('display', 'none');

            }
            
       
         if (blank) {
                return false;
            }
            else {
                return true;
            }
          }



      
</script>

<script>
    var hdnCommentval;
    var Reply;
    var Name;
    var Email;

    $('.replBtn').click(function () {
        $(this).siblings('.commentReply').children(".replybox").show(200);

        $('.Show').hide(0);
        $('.Hide').show(0);

        hdnCommentval = $(this).siblings('.commentReply').children(".replybox").find('input').attr('value');


    });



      // Reply validation

         function validationReply() {
             var Name = $("#txtName" + hdnCommentval + "").val();
            var Email = $("#txtEmail" + hdnCommentval + "").val();
            var Comment = $("#txtReply" + hdnCommentval + "").val();
            var blank = false;

            if (Name == '') {
                //$('#ErrorNameReply').css('display', 'block');
                $("#txtName" + hdnCommentval + "").css('border-color', 'red');
                blank = true;
            }
            else {
                $("#txtName" + hdnCommentval + "").css('border-color', '');
                //$('#ErrorNameReply').css('display', 'none');

            }

            if (Email == '') {
                $("#txtEmail" + hdnCommentval + "").css('border-color', 'red');
                blank = true;
            }
            else {
                var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText.test(Email)) {
                    $("#txtEmail" + hdnCommentval + "").css('border-color', 'red');
                    blank = true;
                }
                else {
                    $("#txtEmail" + hdnCommentval + "").css('border-color', '');


                }

            }
        

            if (Comment == '') {
                $("#txtReply" + hdnCommentval + "").css('border-color', 'red');
                blank = true;
            }
            else {
                $("#txtReply" + hdnCommentval + "").css('border-color', '');

            }
            
       
         if (blank) {
                return false;
            }
            else {
             savevalue();
            }
        }


    function savevalue()
    {

        var Replydata = {};

        Replydata.CommentId = hdnCommentval;
        Replydata.Reply = $("#txtReply" + hdnCommentval + "").val();
        Replydata.Name = $("#txtName" + hdnCommentval + "").val();
        Replydata.Email = $("#txtEmail" + hdnCommentval + "").val();


        $.ajax({
            url: '/blogdetails.aspx/AddReply',
            method: 'post',
            data: '{obj: ' + JSON.stringify(Replydata) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: OnSuccess,
            error: function (err) {
                console.log(err);
            }
        });
    }

    function OnSuccess(response) {
        alert(response.d);
        location.reload(true);
    }


    </script>


</asp:Content>

