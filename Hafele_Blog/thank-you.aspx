﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeFile="thank-you.aspx.cs" Inherits="thank_you" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="en">
<head>
          <!-- Google Tag Manager -->
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
                new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-NFZSDST');</script>
<!-- End Google Tag Manager -->
    <title>Hafele Digital Locks | Smart Lock by Hafele | Digital Security</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="http://hafeleiconic.id8lab.net/images/fab-icon.png">
    <meta name="description" content="Check out the amazing digital locks by Hafele with amazing features like smart password, smart voice, smart security and smart visitor image capture">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/newstyle.css">

   <meta http-equiv="refresh" content="10; url=http://digital-locks.hafeleindia.co.in">
   <style>
        .modal-body {
            overflow: hidden;
        }

        @media only screen and (max-width: 480px) {
            .popup {
                width: auto;
            }

                .popup .col-sm-4 {
                    margin-bottom: 10px;
                }
        }

        a.fancyLink {position: relative;display: block;}
        img.img-responsive.playButton{ position:absolute; left:50%; top:50%; margin-left:-25px; margin-top:-13px;}
    </style>
</head>
<body>
      <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NFZSDST"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <!-- main site wrapper -->
    <div class="site_wrapper">


        <!-- sticky form -->
        <section class="form_sec thankYou">
            <div class="Fopen">
                Enquire Now
            </div>
            <div class="thankYouIn">
                <div class="thankYoubox">
                    Thank You for sharing your details. We will get back to you shortly.
                </div>
            </div>
        </section>
        <!-- // sticky form -->
        <div class="customer-care">
                <a href="https://www.websitealive7.com/4471/operator/guest/gDefault_v2.asp?cframe=login&chattype=normal&groupid=4471&websiteid=823&departmentid=9626&sessionid_=&iniframe=&ppc_id=&autostart=&proactiveid=&req_router_type=&text2chat_info=&loginname=Guest&loginnamelast=&loginemail=&loginphone=&infocapture_ids=&infocapture_values=&dl=https%3A%2F%2Fwww%2Ehafeleindia%2Ecom%2Fen%2F&loginquestion=" target="_blank">
                    <img src="img/icon/customer-care.png" alt="" border="0" /></a>
            </div>

        <!-- content sec -->
    <div class="content_wrapper">

        <!-- banner sec -->
            <section class="bannerForm_Sec">
                <div class="">
                    <%-- <div class="mobileLogo">
                    <img loading="lazy" src="img/logo.png" alt="">
                    </div>--%>
                    <div class="bannerIn">
                        <div id="bannerBig" class="owl-carousel owl-theme homepage-owlcarsouel">
                            <div class="item">
                                <picture>
                                    <source loading="lazy" srcset="img/product-banner.webp" type="image/webp" />
                                    <source loading="lazy" srcset="img/product-banner.jpeg" type="image/jpeg" />
                                    <img loading="lazy" src="img/product-banner.jpeg" alt="Hafele Digital Lock">
                                </picture>
                            </div>
                            <div class="item">
                                <picture>
                                    <source loading="lazy" srcset="img/banner_revel.webp" type="image/webp" />
                                    <source loading="lazy" srcset="img/banner_revel.jpg" type="image/jpeg" />
                                    <img loading="lazy" src="img/banner_revel.jpg" alt="RE-VEAL Biometric Keypad Door Lock">
                                </picture>
                            </div>
                            <div class="item">
                                <picture>
                                    <source loading="lazy" srcset="img/banner-retro.webp" type="image/webp" />
                                    <source loading="lazy" srcset="img/banner-retro.jpg" type="image/jpeg" />
                                    <img loading="lazy" src="img/banner-retro.jpg" alt="RE-TRO Indoor Keypad Door Lock">
                                </picture>
                            </div>
                            <div class="item">
                                <picture>
                                    <source loading="lazy" srcset="img/banner_redesign.webp" type="image/webp" />
                                    <source loading="lazy" srcset="img/banner_redesign.jpg" type="image/jpeg" />
                                    <img loading="lazy" src="img/banner_redesign.jpg" alt="RE-DESIGN Best Keypad Door Lock">
                                </picture>
                            </div>
                            <div class="item">
                                <picture>
                                    <source loading="lazy" srcset="img/banner-remote.webp" type="image/webp" />
                                    <source loading="lazy" srcset="img/banner-remote.jpg" type="image/jpeg" />
                                    <img loading="lazy" src="img/banner-remote.jpg" alt="RE-MOTE Hafele Digital Door Lock">
                                </picture>
                            </div>
                        </div>
                        <div class="banner_logo">
                            <picture>
                                <img loading="lazy" src="img/logo_1.png" alt="">
                            </picture>
                        </div>
                    </div>
            </section>
            <!-- // banner sec -->
        <!-- feature sec -->
       
        <!--Start of the auto slider banner-->
        <div class="container mt-30">
		<div class="row">
			<div class="col-md-12">
				<div class="home-security d-flex">
					<div class="home-security-banner border-shadow">
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->
						 <%-- <ol class="carousel-indicators">
						    <li data-target="#myCarousel" data-slide-to="0" class="active" onclick="$('#myCarouseltext').carousel(0)"></li>
						    <li data-target="#myCarousel" data-slide-to="1" onclick="$('#myCarouseltext').carousel(1)"></li>
						    <li data-target="#myCarousel" data-slide-to="2" onclick="$('#myCarouseltext').carousel(2)"></li>
						  </ol>--%>

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner">
						    <div class="item active">
						      <img src="img/banner-1.jpg" alt="" class="img-responsive">
						    </div>

						    <%--<div class="item">
						      <img src="img/banner-1.jpg" alt="" class="img-responsive">
						    </div>

						    <div class="item">
						      <img src="img/banner-1.jpg" alt="" class="img-responsive">
						    </div>--%>
						  </div>
						</div>
					</div><!--home-security-banner-->
					<div class="home-security-content">
						<div id="myCarouseltext" class="carousel slide" data-ride="carousel">
						  <!-- Wrapper for slides -->
						  <div class="carousel-inner">
						    <div class="item active">
						      <div class="home-security-card">
										<p class="info">
										Häfele introduces a new perspective to home security with its integrated range of Digital Home Security Solutions; allowing you to control multi-dimensional aspects of home access through a single locking device, as per your specific lifestyle and at the leisure of your convenience.
										</p>
										<h4 class="highlight-text">So it’s time to move to a smarter way of life -it’s time to “REinventAccess”!</h4>
									</div><!--home-security-card-->
						    </div>

<%--						    <div class="item">
						      <div class="home-security-card">
										<p class="info">
										Häfele introduces a new perspective to home security with its integrated range of Digital Home Security Solutions; allowing you to control multi-dimensional aspects of home access through a single locking device, as per your specific lifestyle and at the leisure of your convenience.
										</p>
										<h4 class="highlight-text">So it’s time to move to a smarter way of life -it’s time to “REinventAccess”!</h4>
									</div>
						    </div>

						    <div class="item">
						      <div class="home-security-card">
										<p class="info">
										Häfele introduces a new perspective to home security with its integrated range of Digital Home Security Solutions; allowing you to control multi-dimensional aspects of home access through a single locking device, as per your specific lifestyle and at the leisure of your convenience.
										</p>
										<h4 class="highlight-text">So it’s time to move to a smarter way of life -it’s time to “REinventAccess”!</h4>
									</div>
						    </div>--%>
						  </div>
						</div>
					</div><!--home-security-content-->		
				</div>
			</div>
		</div>
	</div>
        <!--End of the auto slider banner-->


        <!--Start of the explore range-->
            <div class="explorerange-wrap">
                <div class="explorerange-innerwrap redbottom-border">
                    <h3>Explore <span>range</span></h3>
                    <div class="explorerange-wrap">
                        <div class="explorerange-innergrid xborder-shadow hidden-sm hidden-xs">
                            <div class="list-inline explorerange-gridlisting">
                                <div class="individual-explorerange reveal-wrap">
                                    <div class="individualexplore-innerlist">
                                        <a href="re-veal.aspx" data-toggle="tooltip" title="<img src='img/productimg/reveal-hover.jpg' />" data-placement="right">
                                            <div class="exploreimg-div">
                                                <img loading="lazy" src="img/productimg/exploreimg-1.png" alt="" class="img-responsive" />
                                                <div class="exploreimg-hiddenarrow">
                                                    <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                </div>
                                                <!--End of the explore img hidden arrow-->
                                            </div>
                                            <!--End f the explore img-div-->
                                        </a>
                                        <div class="explorerange-content">
                                            <h4>RE-veal</h4>
                                            <p>Silk id Fingerprint</p>
                                        </div>
                                        <!--End of the explore range content-->
                                    </div>
                                    <!--End if the individual explore inner list-->
                                </div>
                                <!--End  of the individual explore range-->
                                <div class="individual-explorerange real-wrap">
                                    <div class="individualexplore-innerlist ">
                                        <a href="re-al.aspx" data-toggle="tooltip" title="<img src='img/productimg/real-hover.jpg' />" data-placement="right">
                                            <div class="exploreimg-div">
                                                <img loading="lazy" src="img/productimg/exploreimg-2.png" alt="" class="img-responsive" />
                                                <div class="exploreimg-hiddenarrow">
                                                    <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                </div>
                                                <!--End of the explore img hidden arrow-->
                                            </div>
                                            <!--End f the explore img-div-->
                                        </a>
                                        <div class="explorerange-content">
                                            <h4>RE-al</h4>
                                            <p>Smart Video Phone</p>
                                        </div>
                                        <!--End of the explore range content-->
                                    </div>
                                    <!--End if the individual explore inner list-->
                                </div>
                                <!--End  of the individual explore range-->
                                <div class="individual-explorerange retro-wrap">
                                    <div class="individualexplore-innerlist ">
                                        <a href="re-tro.aspx" data-toggle="tooltip" title="<img src='img/productimg/retro-hover.jpg' />" data-placement="left">
                                            <div class="exploreimg-div">
                                                <img loading="lazy" src="img/productimg/exploreimg-6.png" alt="" class="img-responsive" />
                                                <div class="exploreimg-hiddenarrow">
                                                    <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                </div>
                                                <!--End of the explore img hidden arrow-->
                                            </div>
                                            <!--End f the explore img-div-->
                                        </a>
                                        <div class="explorerange-content">
                                            <h4>RE-tro</h4>
                                            <p>Auto Locking</p>
                                        </div>
                                        <!--End of the explore range content-->
                                    </div>
                                    <!--End if the individual explore inner list-->
                                </div>
                                <!--End  of the individual explore range-->
                                <div class="individual-explorerange redesign-wrap">
                                    <div class="individualexplore-innerlist ">
                                        <a href="re-design.aspx" data-toggle="tooltip" title="<img src='img/productimg/redesign-hover.jpg' />" data-placement="right">
                                            <div class="exploreimg-div">
                                                <img loading="lazy" src="img/productimg/exploreimg-7.png" alt="" class="img-responsive" />
                                                <div class="exploreimg-hiddenarrow">
                                                    <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                </div>
                                                <!--End of the explore img hidden arrow-->
                                            </div>
                                            <!--End f the explore img-div-->
                                        </a>
                                        <div class="explorerange-content">
                                            <h4>RE-design</h4>
                                            <p>Auto Locking</p>
                                        </div>
                                        <!--End of the explore range content-->
                                    </div>
                                    <!--End if the individual explore inner list-->
                                </div>
                                <!--End  of the individual explore range-->
                                <div class="individual-explorerange remote-wrap">
                                    <div class="individualexplore-innerlist ">
                                        <a href="re-mote.aspx" data-toggle="tooltip" title="<img src='img/productimg/remote-hover.jpg' />" data-placement="right">
                                            <div class="exploreimg-div">
                                                <img loading="lazy" src="img/productimg/exploreimg-8.png" alt="" class="img-responsive" />
                                                <div class="exploreimg-hiddenarrow">
                                                    <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                </div>
                                                <!--End of the explore img hidden arrow-->
                                            </div>
                                            <!--End f the explore img-div-->
                                        </a>
                                        <div class="explorerange-content">
                                            <h4>RE-mote</h4>
                                            <p>Smart Etiquettes</p>
                                        </div>
                                        <!--End of the explore range content-->
                                    </div>
                                    <!--End if the individual explore inner list-->
                                </div>
                                <!--End  of the individual explore range-->
                            </div>
                        </div>
                        <!--End of the explore range inner grid-->
                        <div class="mobexplore-rangewrapper hidden-md hidden-lg">
                            <div class="mobexplore-innerrange">
                                <div class="owl-carousel owl-theme mobexplore-rangecarsouel">
                                    <div class="item row">
                                        <div class="individual-explorerange reveal-wrap col-xs-12">
                                            <div class="individualexplore-innerlist">
                                                <a href="re-veal.aspx">
                                                    <div class="exploreimg-div">
                                                        <img loading="lazy" src="img/productimg/exploreimg-1.png" alt="" class="img-responsive" />
                                                        <div class="exploreimg-hiddenarrow">
                                                            <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                        </div>
                                                        <!--End of the explore img hidden arrow-->
                                                    </div>
                                                    <!--End f the explore img-div-->
                                                </a>
                                                <div class="explorerange-content">
                                                    <h4>RE-veal</h4>
                                                    <p>Silk id Fingerprint</p>
                                                </div>
                                                <!--End of the explore range content-->
                                            </div>
                                            <!--End if the individual explore inner list-->
                                        </div>
                                        <!--End  of the individual explore range-->
                                    </div>
                                    <!--End of the item-->

                                    <div class="item row">
                                        <div class="individual-explorerange real-wrap col-xs-12">
                                            <div class="individualexplore-innerlist ">
                                                <a href="re-al.aspx">
                                                    <div class="exploreimg-div">
                                                        <img loading="lazy" src="img/productimg/exploreimg-2.png" alt="" class="img-responsive" />
                                                        <div class="exploreimg-hiddenarrow">
                                                            <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                        </div>
                                                        <!--End of the explore img hidden arrow-->
                                                    </div>
                                                    <!--End f the explore img-div-->
                                                </a>
                                                <div class="explorerange-content">
                                                    <h4>RE-al</h4>
                                                    <p>Smart Video Phone</p>
                                                </div>
                                                <!--End of the explore range content-->
                                            </div>
                                            <!--End if the individual explore inner list-->
                                        </div>
                                        <!--End  of the individual explore range-->
                                    </div>
                                    <!--End of the item-->

                                    <div class="row item">
                                        <div class="individual-explorerange retro-wrap col-xs-12">
                                            <div class="individualexplore-innerlist ">
                                                <a href="re-tro.aspx">
                                                    <div class="exploreimg-div">
                                                        <img loading="lazy" src="img/productimg/exploreimg-6.png" alt="" class="img-responsive" />
                                                        <div class="exploreimg-hiddenarrow">
                                                            <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                        </div>
                                                        <!--End of the explore img hidden arrow-->
                                                    </div>
                                                    <!--End f the explore img-div-->
                                                </a>
                                                <div class="explorerange-content">
                                                    <h4>RE-tro</h4>
                                                    <p>Auto Locking</p>
                                                </div>
                                                <!--End of the explore range content-->
                                            </div>
                                            <!--End if the individual explore inner list-->
                                        </div>
                                        <!--End  of the individual explore range-->
                                    </div>
                                    <!--End of the item-->

                                    <div class="row item">
                                        <div class="individual-explorerange redesign-wrap col-xs-12">
                                            <div class="individualexplore-innerlist ">
                                                <a href="re-design.aspx">
                                                    <div class="exploreimg-div">
                                                        <img loading="lazy" src="img/productimg/exploreimg-7.png" alt="" class="img-responsive" />
                                                        <div class="exploreimg-hiddenarrow">
                                                            <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                        </div>
                                                        <!--End of the explore img hidden arrow-->
                                                    </div>
                                                    <!--End f the explore img-div-->
                                                </a>
                                                <div class="explorerange-content">
                                                    <h4>RE-design</h4>
                                                    <p>Auto Locking</p>
                                                </div>
                                                <!--End of the explore range content-->
                                            </div>
                                            <!--End if the individual explore inner list-->
                                        </div>
                                        <!--End  of the individual explore range-->
                                    </div>
                                    <!--End of the item-->

                                    <div class="row item">
                                        <div class="individual-explorerange remote-wrap col-xs-12">
                                            <div class="individualexplore-innerlist ">
                                                <a href="re-mote.aspx">
                                                    <div class="exploreimg-div">
                                                        <img loading="lazy" src="img/productimg/exploreimg-8.png" alt="" class="img-responsive" />
                                                        <div class="exploreimg-hiddenarrow">
                                                            <img loading="lazy" src="img/white-arrow.png" alt="" class="img-responsive" />
                                                        </div>
                                                        <!--End of the explore img hidden arrow-->
                                                    </div>
                                                    <!--End f the explore img-div-->
                                                </a>
                                                <div class="explorerange-content">
                                                    <h4>RE-mote</h4>
                                                    <p>Smart Etiquettes</p>
                                                </div>
                                                <!--End of the explore range content-->
                                            </div>
                                            <!--End if the individual explore inner list-->
                                        </div>
                                        <!--End  of the individual explore range-->
                                    </div>
                                    <!--End of the item-->
                                    
                                </div>
                                <!--End of he mob owl explore carsouel-->
                            </div>
                            <!--End if the mobexplore inner range-->
                        </div>
                        <!--End of the mob explore range wrapper-->
                    </div>
                    <!--End of the explore range grid wrap-->
                </div>
                <!--End of the explore range inner wrap-->
            </div>
            <!--End of the explore range wrap-->
            <!--End of the explore range-->


       
           <!-- amazing feature sec -->
            <section class="AmazingFeatureSec">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class=" redbottom-border">
                                <h3>Amazing <span>features</span></h3>
                            </div>
                            <div id="navigation" class="owl-carousel  amazingfeature-wrap">
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartPassword.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartPassword1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartVoice.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartVoice1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartEtiquettes.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartEtiquettes1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartSecurity.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartSecurity1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartVisitorImageCapture.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartVisitorImageCapture1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartFreeze.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartFreeze1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartIntruderCapture.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartIntruderCapture1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartNightVision.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartNightVision1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartVideoPhone.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartVideoPhone1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartStorage.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartStorage1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="iconBoxWRP">
                                        <div class="iconBox">
                                            <img loading="lazy" src="img/icon/SmartMotionSensor.png" class="default">
                                            <img loading="lazy" src="img/icon/SmartMotionSensor1.png" class="deHover" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="slider" class="owl-carousel">
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Password: </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>The SMART PASSWORD technology allows you to hide your numeric password between random numbers. Let’s assume that your password is 12345678; the Smart Password function allows you to hide this password before, after or in between other random digits, for example: 9876123456784901. The password can be set up to 8 or 12 digits while the random cushioning numbers can be included up to 30 digits. The Smart Password function is helpful when you don’t want to reveal your password to a person standing next to you while you are accessing your home.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Voice: </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>Every Digital Lock from Häfele talks to you during the instances at which you engage with the lock. These interactive engagements could include step-by-step voice guidance by the lock while adding a user, setting a password or enabling a function/mode; or simple voice notiﬁcations from the lock about various operative modes or incorrect usage of the lock.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Etiquettes: </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>Since every digital lock from Häfele talks to you, it is pertinent to have the right etiquettes to adjust the volume of the lock voice. This technology allows you to seamlessly adjust the voice volume or put it on mute, especially when you are entering your home in the wee hours of morning ensuring that your neighbours are not jolted out of their sleep by your talking lock.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Security: </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>With the different access modes available in the Häfele Digital Locks, you can enable additional security by combining two access authentications; for example, in case of a new house-help who is yet to earn your trust, it could be unsafe to just give her a numeric password access as you could be in danger of her sharing this password with an ally. In such situations adding a face-recognition access as the second authentication can ensure that the person entering your home is not an unwanted stranger.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Visitor Image Capture: </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>Hafele’s RE-Bell Video Doorbell allows you to know who had visited your home even when you were not around to receive them. Every visitor’s image and video call can be viewed live if you are around to receive it and is also recorded for you to see in your mobile App later.  In fact, even if you are in a no network zone or your phone is on ﬂight mode and during that time if a guest visits you, the bell clicks and saves the image of the guest on the server which can then be viewed in the app later. You can now be rest assured that no guest coming to your house goes unnoticed.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Freeze: </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>The Häfele Digital Locks have a narrow tolerance for incorrect entries of access by way of a wrong password or unfamiliar ﬁngerprint – 5 to 10 wrong entries result in a lock-freeze for up to 5 minutes. This feature reduces the possibilities of unwanted break-ins.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Intruder Capture:</h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>RE-Veal, the face recognition lock by Häfele, not only enables the highest level of security for your home but also tells you if someone tried to disrupt this security. Its Smart Intruder Capture technology records the face of any unregistered user who has tried to break into your home. The lock can pull out up to 10 such images in case you want to do an instant check.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Night Vision:</h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>A feature unique to Hafele’s RE-Veal Face Recognition Lock and Hafele’s RE-Bell Video door Bell, Smart Night Vision uses infrared technology to recognize a visitor’s face even in pitch darkness. This feature comes most handy when you or your guests are trying to access your home in the dense hours of the night when the overall lights in your building corridors are dimmed out or completely turned off.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Video Phone:  </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>Hafele’s RE-AL and RE-Place digital locks can be paired to an existing Video Door Phone using a simple radio frequency unit. Once connected, your Video Door Phone and Digital Lock work seamlessly together as one gadget allowing you the beneﬁts of interacting with a visitor at your doorstep while also enabling you to unlock the door to him through the key button on your video door phone; another feather of convenience to manage your door security system from a remote location. And in case you don’t have a Video Door Phone, you could consider Hafele’s RE-Bell video doorbell which acts individually as a video door phone and cohesively as a complete security solution when clubbed with any of our digital locks!</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Storage:  </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>Häfele’s RE-Bell Video Doorbell comes with the added advantage of recoding video footage and that to for 24 hours continuously! This Smart Storage feature allows you to keep a historic perspective on who entered your home; not just through time logs but through actual videos. The only requirement is that your App-enabled phone (which is paired to the lock) should have an SD card as the videos recorded automatically get saved in the storage memory of an SD card.</p>
                                    </div>
                                </div>
                                <div class="projectitem">
                                    <div class="tbaDeasIN">
                                        <div class="project-title-wrap">
                                            <h4>Smart Motion Sensor:  </h4>
                                            <a href="productfeature.aspx" class="project-btn">View All</a>
                                        </div>
                                        <p>Häfele’s RE-Bell Video Doorbell comes with an intuitive smart motion sensor feature which when activated senses the presence of a person or any other moving object within 3 meters of the camera lens. Such a realization is then sent to you as a notiﬁcation on your App–enabled phone alerting you that someone or something was hovering around your main door; an ideal option to enable increased security for your home.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- // amazing feature sec -->
            <!-- video sec -->
            <section class="AmazingFeatureSec clear videosfeature-wrap">
                <div class="yt_videos container">
                    <div class=" redbottom-border">
                        <h3>Watch  <span>Videos</span></h3>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-6">
                            <a data-fancybox="" href="https://www.youtube.com/watch?v=FIU8PD3RQCI" class="fancyLink">
                                <img loading="lazy" class="img-responsive" src="https://img.youtube.com/vi/FIU8PD3RQCI/hqdefault.jpg" />
                                <img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
                            </a>
                        <h4>No reason to smile today?</h4>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <a data-fancybox="" href="https://www.youtube.com/watch?v=d0nEYPyDy18" class="fancyLink">
                                <img loading="lazy" class="img-responsive" src="https://img.youtube.com/vi/d0nEYPyDy18/hqdefault.jpg" />
                                <img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
                            </a>
                        <h4>Invited for a Theme Party?</h4>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <a data-fancybox="" href="https://www.youtube.com/watch?v=xphOo-NS54E" class="fancyLink">
                                <img loading="lazy" class="img-responsive" src="https://img.youtube.com/vi/xphOo-NS54E/hqdefault.jpg" />
                                <img loading="lazy" class="img-responsive playButton" src="img/play_button.png" />
                            </a>
                        <h4>Cant find your keys in the dark?</h4>
                        </div>
                        <div class="col-sm-12 all-vdo-up">
                        <a href="video.aspx" class="all-vdo">View All Videos</a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- //video sec -->

    </div>
    <!-- // content sec -->
        <!-- footer search sec -->

        <!-- // footer search sec -->
        <!-- footer sec -->
        <footer class="footer_sec">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="row">
                            <li class="col-lg-4 col-sm-4">
                                <p class="copy_right">© 2018 by Häfele</p>
                            </li>
                            <li class="col-lg-4 col-sm-4">
                                <ul class="ft_social">
                                    <li><a target='_blank' href="https://www.instagram.com/hafele_india/">
                                        <img src="img/icon_1.png" alt=""></a></li>
                                    <li><a target='_blank' href="https://twitter.com/hafeleindia/">
                                        <img src="img/icon_2.png" alt=""></a></li>
                                    <li><a target='_blank' href="https://www.facebook.com/hafeleindia/">
                                        <img src="img/icon_3.png" alt=""></a></li>
                                    <li><a target='_blank' href="https://www.youtube.com/user/HaefeleIN1/">
                                        <img src="img/icon_4.png" alt=""></a></li>
                                </ul>
                            </li>
                            <li class="col-lg-4 col-sm-4">
                                <div class="ftLogo">
                                    <a href="#">
                                        <img src="img/ftLogo.png" alt=""></a>
                                    <%--<a href="#" class="termsCondition" data-toggle="modal" data-target="#exampleModal">Terms & Conditions</a>--%>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- // footer sec -->
    </div>
    <!-- // site wrapper -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Terms & Conditions</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="">
                        <h4>Game Rules:</h4>
                        <ul>
                            <li>1] Like Hafele India page on Facebook | Follow Hafele India on Instagram and Twitter</li>
                            <li>2] Share your #lockedoutstories on http://hafeleindia.co.in/lockedout-stories/</li>
                            <li>3] Share http://hafeleindia.co.in/lockedout-stories/ page on Facebook | Instagram| Twitter. Use #lockedoutstories and Tag your 5 friends and ask them to participate.</li>
                            <li>4] Unique stories will stand a chance to win Exciting prizes and amazon vouchers.</li>
                        </ul>
                        <h4>Terms and conditions:</h4>
                        <p>#lockedoutstories Contest: By participating in this contest, the Participant fully and unconditionally agrees to and accepts the Terms and Conditions. This contest is not valid for Hafele India Pvt Ltd, its affiliates, subsidiaries, distributors, dealers as well as its advertising, creative, media, digital, design and other creative agencies and their immediate family members.</p>
                        <p>By participating in this contest, participants agree to be bound by the official rules and regulations of the contest and agree that decisions made by Hafele India Pvt. Ltd are final and conclusive. No queries whatsoever will be entertained.</p>
                        <h5>Terms & Conditions: Rules to follow before participating in the contest:</h5>
                        <ul>
                            <li>• The #lockedoutstories Contest is brought to you by Hafele India Pvt Ltd. (‘Organizer’).</li>
                            <li>• The contest is open for all the residents of India who are 18 years of age or above.</li>
                            <li>• The contest is valid from 27th October 2018 to 15th November 2018 on Hafele India Pvt Ltd’s Facebook, Twitter and Instagram page, in accordance with these terms and conditions. The organizer may in its absolute discretion curtail or extend the Contest Period or any part of the promotion, as it deems necessary without any liability whatsoever, and no communication in this regard will be entertained.</li>
                            <li>• Hafele India Pvt Ltd reserves the right to cancel, terminate or suspend the contest without any prior notice. For the avoidance of doubt, any cancellation, termination or suspension by Hafele India Pvt Ltd of the contest shall not entitle the participant to any claim or compensation against Hafele India Pvt Ltd for all losses or damages which may be suffered or incurred by the participant as a direct or an indirect result of the act of cancellation, termination or suspension.</li>
                            <li>• Hafele India Pvt Ltd is entitled to use, publish or feature the caption, winners and/or their names/photographs/audio clips/video clips for publicity and advertising purposes. Hafele India Pvt Ltd reserves the right to delay the timing of the publication of the winners of the contest. All information provided will be kept strictly private and confidential.</li>
                            <li>• Hafele India Pvt Ltd shall not be liable for any disruption to the contest, whether due to technical problems or otherwise, which is beyond its reasonable control. In the event of disruption to the contest, reasonable effort shall be used to remedy the disruption and resume the contest on a fair and equitable basis to the participants.</li>
                            <li>• By entering this contest, participants unconditionally and irrevocably agree to be bound by these terms and conditions and decision of the Hafele India Pvt Ltd. Any breach of the terms and conditions may, at Hafele India Pvt Ltd absolute discretion, result in forfeiture of any prize.</li>
                            <li>• Hafele India Pvt Ltd reserves the right at its absolute discretion to vary, delete or add to any of these terms and conditions and its decision without any prior notice. These terms and conditions will prevail over any inconsistent terms, conditions, provisions or representations contained in any other promotional materials advertising the contest. </li>
                            <li>• Prizes cannot be exchanged in full, or partially for cash or other equivalents.</li>
                            <li>• Hafele India Pvt Ltd reserves the right to replace the prizes with items of similar value in the event of unforeseen circumstances.</li>
                            <li>• Participants represent and warrant that they have read and understood the terms and conditions of this contest and agree to be bound by them. </li>
                            <li>• The Terms & Conditions are subject to Indian Law and the exclusive jurisdiction of the Courts in Mumbai only.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="VideoModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Watch product video</h4>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="415" src="https://www.youtube.com/embed/dy9eE1p00y8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

        </div>
    </div>

        <!--<script src="js/jquery-1.12.0.min.js"></script>-->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/owl.carousel.js"></script>
      <link href="https://fonts.googleapis.com/css?family=Parisienne|Roboto:100,300,400,500,700,900" rel="stylesheet">
      <script src='https://www.youtube.com/iframe_api' async></script>
      <script src="js/common.js"></script>
      <script src="js/jquery.scrolldepth.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous"></script>
    <script type="text/javascript">		
        $(document).ready(function () {

            $('#bannerBig').owlCarousel({
                //loop:true,
                margin: 0,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                        //margin:20
                    },
                    600: {
                        items: 1,
                        nav: true
                        //	margin:20
                    }
                }
            });

            $('.owl-item.active.center').prev().addClass('wp').siblings().removeClass('wp');
            $('.owl-item.active.center').next().addClass('wpp').siblings().removeClass('wpp');
            $('.wp').prev().addClass('wppp').siblings().removeClass('wppp');
            $('.wpp').next().addClass('wpppp').siblings().removeClass('wpppp');

            $('.Fopen').click(function () {
                $('body').toggleClass('OpenF');
            });
        });

        $(document).ready(function () {

            var sync1 = $("#slider");
            var sync2 = $("#navigation");
            var slidesPerPage = 6;
            var syncedSecondary = true;

            sync1.owlCarousel({
                items: 1,
                slideSpeed: 200000,
                nav: true,
                autoplay: false,
                dots: true,
                loop: true,
                responsiveRefreshRate: 200,
                navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
            }).on('changed.owl.carousel', syncPosition);

            sync2
                .on('initialized.owl.carousel', function () {
                    sync2.find(".owl-item").eq(0).addClass("current");
                })
                .owlCarousel({
                    items: slidesPerPage,
                    dots: true,
                    margin: 0,
                    nav: true,
                    loop: false,
                    // smartSpeed: 1000,
                    //slideSpeed : 500,
                    slideBy: slidesPerPage,
                    responsiveRefreshRate: 100,
                    responsive: {
                        0: {
                            items: 1
                        },
                        400: {
                            items: 2
                        },
                        600: {
                            items: 3
                        },
                        768: {
                            items: 4
                        },
                        1000: {
                            items: 5
                        }
                        ,
                        1200: {
                            items: 6
                        }
                    }
                }).on('changed.owl.carousel', syncPosition2);

            function syncPosition(el) {
                var count = el.item.count - 1;
                var current = Math.round(el.item.index - (el.item.count / 2) - .5);

                if (current < 0) {
                    current = count;
                }
                if (current > count) {
                    current = 0;
                }

                sync2
                    .find(".owl-item")
                    .removeClass("current")
                    .eq(current)
                    .addClass("current");
                var onscreen = sync2.find('.owl-item.active').length - 1;
                var start = sync2.find('.owl-item.active').first().index();
                var end = sync2.find('.owl-item.active').last().index();

                if (current > end) {
                    sync2.data('owl.carousel').to(current, 100, true);
                }
                if (current < start) {
                    sync2.data('owl.carousel').to(current - onscreen, 100, true);
                }
            }

            function syncPosition2(el) {
                if (syncedSecondary) {
                    var number = el.item.index;
                    sync1.data('owl.carousel').to(number, 100, true);
                }
            }

            sync2.on("click", ".owl-item", function (e) {
                e.preventDefault();
                var number = $(this).index();
                sync1.data('owl.carousel').to(number, 300, true);
            });
        });







        var $owl = $('#slider3D');
        $owl.children().each(function (index) {
            $(this).attr('data-position', index);
        });
        $owl.owlCarousel({
            autoplay: false,
            center: true,
            loop: true,
            nav: true,
            responsive: {
                0: {
                    items: 1,
                    margin: 20
                },
                639: {
                    items: 3,
                    margin: 0,
                    touchDrag: false,
                    mouseDrag: false
                },
                768: {
                    items: 3,
                    margin: 0,
                    touchDrag: false,
                    mouseDrag: false
                },
                1200: {
                    items: 5,
                    margin: 30,
                    touchDrag: false,
                    mouseDrag: false
                }
            },
        });
        $(document).on('click', '.owl-item>div', function () {
            $owl.trigger('to.owl.carousel', $(this).data('position'));
            $('.owl-item.active.center').prev().addClass('wp').siblings().removeClass('wp');
            $('.owl-item.active.center').next().addClass('wpp').siblings().removeClass('wpp');
            $('.wp').prev().addClass('wppp').siblings().removeClass('wppp');
            $('.wpp').next().addClass('wpppp').siblings().removeClass('wpppp');
        });
        $('.owl-nav div').on('click', function () {
            $('.owl-item.active.center').prev().addClass('wp').siblings().removeClass('wp');
            $('.owl-item.active.center').next().addClass('wpp').siblings().removeClass('wpp');
            $('.wp').prev().addClass('wppp').siblings().removeClass('wppp');
            $('.wpp').next().addClass('wpppp').siblings().removeClass('wpppp');
        });

    </script>

</body>
</html>
